# CATALYST INFORMATION BROKER

## Installation instructions 

To install, Docker and Docker-compose support is assumed. Before building the container, you should tell the Information Broker where the Market Session Manager is deployed. To do this, edit the .env file and set the MSM_URL variable, by setting the IP address where the Market Session Manager listens to (the default port is 8001), and the value of ITLB_URL variable with the IP address where the ITLB listens to.
After successful docker installation and IB configuration, just run the commands:

```bash
$ cd information_broker/config/docker
$ bash launch.sh
```

If everything went well, you should be able to reach informatIon broker services at ```http://<YOUR_IP>:5000/```

### How to install Docker under Ubuntu

To install docker / docker-compose in an ubuntu 16.04 system, run the following as root:

```bash
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get -y install docker-ce
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose 
sudo chmod +x /usr/local/bin/docker-compose
```

### Docker-compose Installation

```bash
sudo curl -L https://github.com/docker/compose/releases/download/1.18.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

### Information Broker Swagger
`http://{information_broker_ip}/swagger/`