import os

APPEND_SLASH=False

SECRET_KEY = 'o_zm@6e!y_6yozwj$*$%v(c8n%+e8hjyk$@l@83ygtpuhkaj_7'

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))

STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'


MEDIA_URL = '/media/' # '"/{}{}".format(PREFIX, '/media/')
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')


#TODO: Check if this is needed
# ACCESS_TOKEN = ''

#TODO: Check if these are needed
# CLIENT_ID = 'baU8LWi0SABAz27dAMl9GuFNP3MZSTnYB7CDHtJk'
# CLIENT_SECRET = 'P6Pbo6Tnm9X48kXGAhx1kMJyZzE8rFE0yheXv80Mek6DDVUd8zppYqUjOLB8j6ZtJuIILO2ojpfWXLn7QO3Do5etRs3wZBlVmu3wMeyOpflXphSJoKFTZoBS0xEGN4EG'

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Native Apps
    'ib',
    'rest_framework',
    'rest_framework.authtoken',
    # 3rd party apps
    'corsheaders',
    'cachalot',
    'drf_yasg',
]

#TODO: Check if this is needed
USER_TOKEN_KEY_PREFIX = 'token_user_'
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'oauth2_provider.middleware.OAuth2TokenMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'ib.middleware.KeycloakMiddleware'
]

ROOT_URLCONF = 'information_broker.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'information_broker.wsgi.application'


AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# ==================================
#   CACHE SETTINGS
# ==================================
ORM_CACHE_TIMEOUT = 2592000
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
        'TIMEOUT': ORM_CACHE_TIMEOUT,
    }
}

# ==================================
#   CACHALOT SETTINGS
# ==================================
CACHALOT_ENABLED = True
CACHALOT_CACHE = 'default'
CACHALOT_CACHE_RANDOM = True
CACHALOT_INVALIDATE_RAW = True
CACHALOT_TIMEOUT = ORM_CACHE_TIMEOUT

USE_SESSION_AUTH = True

# ==================================
#   REST FRAMEWORK SETTINGS
# ==================================
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        #'rest_framework.authentication.TokenAuthentication',  # <-- And here
    ],
    'DEFAULT_PERMISSION_CLASSES': (
        # 'rest_framework.permissions.IsAuthenticated',
    )
}

# ==================================
#   SWAGGER SETTINGS
# ==================================
SWAGGER_SETTINGS = {
    'SUPPORTED_SUBMIT_METHODS': [
        'get',
        'post',
        'put',
        'patch',
        'delete',
    ],
    'SECURITY_DEFINITIONS': {
        'Bearer': {
            'description': "",
            'type': 'apiKey',
            'name': 'Authorization',
            'in': 'header'
        },
    },
    'USE_SESSION_AUTH': False,
    'JSON_EDITOR': True,
    'JSON_REQUEST': True,
}



# ==================================
#   LOGGING SETTINGS
# ==================================

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'detailed': {
            'class': 'logging.Formatter',
            'format': "[%(asctime)s] - [%(name)s:%(lineno)s] - [%(levelname)s] %(message)s",
        }
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'INFO',
            'formatter': 'detailed',
        },
        'file': {
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': "{}/ib.log".format(BASE_DIR),
            'mode': 'w',
            'formatter': 'detailed',
            'level': 'INFO',
            'maxBytes': 2024 * 2024,
            'backupCount': 5,
        },
    },
    'loggers': {
        'ib': {
            'level': 'INFO',
            'handlers': ['console']
        },
    }
}

# ==================================
#   MSM SETTINGS
# ==================================

# TODO: Are the first two really needed? MSM does not even exist any more
MSM_URL = os.getenv('MSM_URL')
MCM_URL = os.getenv('MCM_URL')
ITLB_URL = os.getenv('ITLB_URL')


#TODO: Check if this is needed
TOKEN_URL = 'http://localhost'