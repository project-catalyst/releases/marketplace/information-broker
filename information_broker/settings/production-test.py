
# TODO: I tried to split the settings as they should be.
# the rest of the settings files should be accordingly refactored
from information_broker.settings.base import *
import os


def get_static_url():
    """
    Gets the static url based on the relative one
    """
    _rel_url = os.getenv('RELATIVE_URL', '')
    if len(_rel_url) == 0:
        return '/static/'

    if _rel_url.endswith("/api/"):
        _rel_url = _rel_url.replace("/api/", "/")

    return ("/" + _rel_url + "/static/").replace("//", "/")


DEBUG = True

RELATIVE_URL = os.getenv('RELATIVE_URL', '')
STATIC_URL = get_static_url()

CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_HEADERS = (
    'accept',
    'accept-encoding',
    'authorization',
    'content-type',
    'dnt',
    'origin',
    'user-agent',
    'x-csrftoken',
    'x-requested-with',
)

ALLOWED_HOSTS = ['*', ]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'information_broker',
        'USER': 'information_broker',
        'PASSWORD': 'xyXdSM2DW5HPkJG8vV9CezXLmhtpyuLc',
        'HOST': 'localhost',
        'PORT': '5435',
    }
}

# TODO: complete with specific KeyCloak details
KEYCLOAK = {
    "url": os.getenv("KEYCLOAK_URL", "http://83.235.169.221:51008/auth/"),
    "client_id": "catalyst-maas-flexibility",
    "client_secret": "b3c2c4a6-2142-4d70-bba1-d73f056b5aa8",
    "realm_name": "test-test-test",
    "config": os.path.join(BASE_DIR, 'keycloak-config.json'),
    "admin_user": "admin",
    "admin_pwd": "nQc5NqEwPcJk3wG7amnbd9X0P4Kx6G7O"
}