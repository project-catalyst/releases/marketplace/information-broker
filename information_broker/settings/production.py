#TODO: I tried to split the settings as they should be.
#the rest of the settings files should be accordingly refactored
from information_broker.settings.base import *
import os

def get_static_url ():
    """
    Gets the static url based on the relative one
    """
    _rel_url = os.getenv('RELATIVE_URL', '')
    if len(_rel_url) == 0:
        return '/static/'
    
    if _rel_url.endswith("/api/"):
        _rel_url = _rel_url.replace("/api/","/")
    
    return ("/" + _rel_url + "/static/").replace("//","/")


DEBUG = True

RELATIVE_URL = os.getenv('RELATIVE_URL', '')
STATIC_URL = get_static_url()

CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_HEADERS = (
    'accept',
    'accept-encoding',
    'authorization',
    'content-type',
    'dnt',
    'origin',
    'user-agent',
    'x-csrftoken',
    'x-requested-with',
)

ALLOWED_HOSTS = ['*', ]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.getenv('POSTGRES_DATABASE'),
        'USER': os.getenv('POSTGRES_USER'),
        'PASSWORD': os.getenv('POSTGRES_PASSWORD'),
        'HOST': os.getenv('POSTGRES_HOST'),
        'PORT': 5432,
    }
}

KEYCLOAK = {
    "url": os.getenv("KEYCLOAK_URL", "http://maas-keycloak-service.default:8080/auth/"),
    "client_id": os.getenv("CLIENT_ID", "catalyst-maas"),
    "client_secret": os.getenv("CLIENT_SECRET"),
    "realm_name": os.getenv("KEYCLOACK_REALM_NAME"),
    "config": os.path.join(BASE_DIR, 'keycloak-config.json'),
    "admin_user": os.getenv("REALM_ADMIN_USERNAME"),
    "admin_pwd": os.getenv("REALM_ADMIN_PASSWORD")
}
