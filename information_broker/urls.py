
from django.conf.urls import url, include
from django.contrib import admin

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('ib.urls', namespace="app")),
]

if settings.RELATIVE_URL and len(settings.RELATIVE_URL) > 0:
    urlpatterns = [url(r'^{}'.format(settings.RELATIVE_URL), include(urlpatterns)), ]
