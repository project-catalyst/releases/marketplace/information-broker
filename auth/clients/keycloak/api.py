from django.conf import settings

from keycloak import KeycloakOpenID
from keycloak import KeycloakAdmin

import json

from keycloak import URL_ADMIN_CLIENT_SCOPES, raise_error_from_response, KeycloakGetError, \
    URL_ADMIN_USERS, URL_ADMIN_USER_CLIENT_ROLES, URL_ADMIN_CLIENTS, URL_ADMIN_CLIENT_ROLES, URL_ADMIN_REALM_ROLES, \
    URL_ADMIN_CLIENT_SECRETS, URL_ADMIN_CLIENT_AUTHZ_SETTINGS, URL_ADMIN_USER_REALM_ROLES


import logging.config
from django.conf import settings

logging.config.dictConfig(settings.LOGGING)
log = logging.getLogger("ib")


realm_specific_roles = {
    'generic participant': 'market-participant',
    'memo': 'memo',
    'market operator': 'operator',
}

client_specific_roles = {
    'dso': 'dso',
    'mbm': 'mbm',
    'mcm': 'mcm',
    'aggregator': 'aggregator'
}


class Client(object):
    client: KeycloakOpenID = None

    admin: KeycloakAdmin = None

    realm: str = None

    def __init__(self):
        self.realm = settings.KEYCLOAK['realm_name']
        self.server_url = settings.KEYCLOAK['url']

        self.client = KeycloakOpenID(server_url=self.server_url,
                                     client_id=settings.KEYCLOAK['client_id'],
                                     client_secret_key=settings.KEYCLOAK['client_secret'],
                                     realm_name=settings.KEYCLOAK['realm_name'],
                                     verify=True)

        self.admin = KeycloakAdmin(server_url=self.server_url,
                                   username=settings.KEYCLOAK['admin_user'],
                                   password=settings.KEYCLOAK['admin_pwd'],
                                   realm_name=settings.KEYCLOAK['realm_name'],
                                   verify=False)

    def get_client(self):
        return self.client

    def get_admin(self):
        return self.admin

    def create_user(self, username, password, first_name, last_name, user_type):
        """
        Creates a user in the realm.
        :param user_type:
        :param username:
        :param password:
        :param first_name:
        :param last_name:
        :return:
        """

        user_id = self.get_user_id_in_realm(username=username, realm_name=self.realm)

        if user_id is not None:
            log.warning("A keycloak user with username: {} already exists in realm: {}"
                        .format(username, self.realm))
            return None

        if user_type in realm_specific_roles:
            realm_roles = [realm_specific_roles[user_type]]
        else:
            realm_roles = []

        payload = {
            "attributes": {},
            "credentials": [
                {
                    "type": "password",
                    "value": password
                }
            ],
            "enabled": True,
            "firstName": first_name,
            "lastName": last_name,
            "realmRoles": [
                "user_default"
            ],
            "emailVerified": True,
            "username": username
        }
        ret = self.admin.create_user(payload=payload)

        if len(realm_roles) > 0:
            self.assign_realm_roles_to_user_in_realm(username, realm_roles, self.realm)

        return ret

    # realm roles apply realm wide, to every registered keycloak client
    def assign_realm_roles_to_user_in_realm(self, username, roles, realm_name):
        """
        Assigns realm roles to a user
        :param username: The username of the user
        :param roles: List[str] The roles of interest
        :param realm_name: The realm name
        :return: Nothing
        """
        realm_name = realm_name if realm_name else self.realm
        user_id = self.get_user_id_in_realm(username=username, realm_name=realm_name)
        all_roles = self.get_realm_roles_in_realm(realm_name=realm_name)
        roles_repr = [x for x in all_roles if x['name'] in roles]
        payload = roles_repr
        params_path = {"realm-name": realm_name, "id": user_id}
        ret = self.post_to_keycloak(url=URL_ADMIN_USER_REALM_ROLES.format(**params_path),
                                    payload=payload)
        return ret

    def assign_client_roles_to_market_actor(self, username, user_type):
        client_name = settings.KEYCLOAK['client_id']

        if user_type in client_specific_roles:
            client_roles = [client_specific_roles[user_type]]
        else:
            client_roles = []

        if len(client_roles) > 0:
            log.warning("Client: {} - Assigning client roles: {} to keycloak user :{}"
                        .format(client_name, client_roles, username))

            ret = self.assign_client_roles_to_user(username, client_name, client_roles)
        else:
            ret = 0
        return ret

    # assign client specific roles to user. client specific roles are specific per market variant
    # ex (heat, it load, flexibility), since 1 keycloak client is created per market variant deployment
    def assign_client_roles_to_user(self, username, client_name, roles):
        """
        Assigns roles to a user
        :param username: The username of interest
        :param client_name: The app client name
        :param roles: The list of roles to add
        :return: Nothing
        """

        return self.assign_client_roles_to_user_in_realm(username=username,
                                                         client_name=client_name,
                                                         roles=roles,
                                                         realm_name=self.realm)


    def assign_client_roles_to_user_in_realm(self, username, client_name, roles, realm_name):
        """
        Assigns client roles to a user in a realm
        :param username: The username of the user
        :param client_name: The client name
        :param roles: List[str] The list of roles to assign
        :param realm_name: The realm name
        :return:
        """
        realm_name = realm_name if realm_name else self.realm
        user_id = self.get_user_id_in_realm(username=username, realm_name=realm_name)
        client_id = self.get_client_id_in_realm(client_name=client_name,
                                                realm_name=realm_name)
        all_roles = self.get_client_roles_in_realm(client_name=client_name,
                                                   realm_name=realm_name)
        roles_repr = [x for x in all_roles if x['name'] in roles]
        payload = roles_repr
        params_path = {"realm-name": realm_name, "id": user_id, "client-id": client_id}
        ret = self.post_to_keycloak(URL_ADMIN_USER_CLIENT_ROLES.format(**params_path),
                                    payload=payload)
        return ret

    def get_user_id_in_realm(self, username, realm_name):
        """
        Gets the user id in a realm
        :param username: The username of the user
        :param realm_name: The realm name
        :return: The user id
        """
        params_path = {"realm-name": realm_name if realm_name else self.realm}
        users = self.fetch_all(URL_ADMIN_USERS.format(**params_path), {"search": username})
        return next((user["id"] for user in users if user["username"] == username), None)

    def get_client_id_in_realm(self, client_name, realm_name):
        """
        Gets the id of a client in a realm
        :param client_name: The client name
        :param realm_name: The realm name
        :return: The client id
        """
        params_path = {"realm-name": realm_name if realm_name else self.realm}
        clients = self.get_from_keycloak(URL_ADMIN_CLIENTS.format(**params_path))
        for client in clients:
            if client_name == client.get('name') or client_name == client.get('clientId'):
                return client["id"]
        return None


    def get_realm_roles_in_realm(self, realm_name):
        """
        Gets the roles in a realm
        :param realm_name: The realm name
        :return: List[RoleRepresentation] The list of realm roles
        """
        realm_name = realm_name if realm_name else self.realm
        params_path = {"realm-name": realm_name}
        ret = self.get_from_keycloak(URL_ADMIN_REALM_ROLES.format(**params_path))
        return ret

    def get_client_roles_in_realm(self, client_name, realm_name):
        """
        Gets the client roles in a realm
        :param client_name: The client name
        :param realm_name: The realm name
        :return: List[RoleRepresentation] The list of client roles
        """
        client_id = self.get_client_id_in_realm(client_name, realm_name)
        params_path = {"realm-name": realm_name if realm_name else self.realm, "id": client_id}
        data_raw = self.get_from_keycloak(URL_ADMIN_CLIENT_ROLES.format(**params_path))
        return data_raw

    def fetch_all(self, url, query=None):
        '''Wrapper function to paginate GET requests

        :param url: The url on which the query is executed
        :param query: Existing query parameters (optional)

        :return: Combined results of paginated queries
        '''
        results = []

        # initalize query if it was called with None
        if not query:
            query = {}
        page = 0
        query['max'] = 100

        # fetch until we can
        while True:
            query['first'] = page * 100
            partial_results = raise_error_from_response(
                self.admin.raw_get(url, **query),
                KeycloakGetError)
            if not partial_results:
                break
            results.extend(partial_results)
            page += 1
        return results

    def get_from_keycloak(self, url):
        data_raw = self.admin.raw_get(url)
        return raise_error_from_response(data_raw, KeycloakGetError)

    def post_to_keycloak(self, url, payload):
        data_raw = self.admin.raw_post(url,
                                        data=json.dumps(payload))
        if data_raw.status_code not in [200, 201, 204]:
            log.error("Error while POSTing to {}".format(url))
            log.error("Status code: {}".format(data_raw.status_code))
            log.error("Content:     {}".format(data_raw.content.decode("utf-8")))
            raise Exception("Post to Keycloak failed")
        if 'location' in data_raw.headers:
            _last_slash_idx = data_raw.headers['Location'].rindex('/')
            return data_raw.headers['Location'][_last_slash_idx + 1:]
        else:
            return data_raw.content.decode("utf-8")