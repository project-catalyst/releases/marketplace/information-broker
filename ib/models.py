import ast

from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class ActionStatus(models.Model):
    type_choices = (('unchecked', 'Unchecked'), ('valid', 'Valid'), ('invalid', 'Invalid'), ('active', 'Active'),
                    ('inactive', 'Inactive'), ('withdrawn', 'Withdrawn'), ('accepted', 'Accepted'),
                    ('partially_accepted', 'Partially_accepted'), ('rejected', 'Rejected'), ('selected', 'Selected'),
                    ('delivered', 'Delivered'), ('not_delivered', 'Not_delivered'), ('billed', 'Billed'))
    status = models.CharField(choices=type_choices, max_length=45,
                              help_text='status of an action (unchecked, valid, eligible, etc.)')

    def __str__(self):
        return "Action Status id {}, status {}".format(self.id, self.status)

    class Meta:
        db_table = 'ActionStatus'
        ordering = ('id',)


class ActionType(models.Model):
    type_choices = (('offer', 'Offer'), ('bid', 'Bid'))
    type = models.CharField(choices=type_choices, max_length=45, help_text='type of action (bid, offer)')

    def __str__(self):
        return "Action Type {} {}".format(self.id, self.type)

    class Meta:
        ordering = ('id',)
        db_table = 'ActionType'


class Form(models.Model):
    form_choices = (('electric_energy', 'Electric_energy'), ('thermal_energy', 'Thermal energy'),
                    ('congestion_management', 'Congestion Management'),
                    ('reactive_power_compensation', 'Reactive Power Compensation'),
                    ('spinning_reserve', 'Spinning Reserve'), ('scheduling', 'Scheduling'), ('it_load', 'IT Load'))
    form = models.CharField(choices=form_choices, max_length=45, help_text='Form type')

    def __str__(self):
        return "Form {}".format(self.form)

    class Meta:
        ordering = ('id',)
        db_table = 'Form'


class MarketActorType(models.Model):
    type_choices = (('aggregator', 'Aggregator'), ('dso', 'Dso'), ('generic participant', 'Generic participant'),
                    ('market operator', 'Market operator'), ('msm', 'Market Session Manager'))
    type = models.CharField(choices=type_choices, default='dso', max_length=100,
                            help_text='type of market actor ( aggregator, DSO, generic participant)')

    def __str__(self):
        return "Actor: {} {}".format(self.type, self.id)

    class Meta:
        ordering = ('id',)
        db_table = 'MarketActorType'


class MarketActor(models.Model):
    companyName = models.CharField(max_length=100, help_text="Company name")
    email = models.EmailField(help_text="email")
    phone = models.CharField(max_length=30, help_text="Phone number")
    representativeName = models.CharField(max_length=100, help_text="Name of company representative")
    vat = models.CharField(max_length=30, help_text="Value Added Tax number")
    marketActorTypeid = models.ForeignKey(MarketActorType, help_text="market actor type identity number",
                                          db_column='MarketActorType_id')

    def __str__(self):
        return "Actor {}: type: {}, name: {}".format(self.id, self.marketActorTypeid.type, self.representativeName)

    class Meta:
        ordering = ('id',)
        db_table = 'MarketActor'


class TimeFrame(models.Model):
    type_choices = (('intra_day', 'Intra_day'), ('day_ahead', 'Day_ahead'), ('real_time', 'Real_time'),)
    type = models.CharField(choices=type_choices, default='intra_day', max_length=100,
                            help_text='time frame type (day-ahead, intra-day, real-time)')

    def __str__(self):
        return "Timeframe {}".format(self.type)

    class Meta:
        ordering = ('id',)
        db_table = 'TimeFrame'


class MarketServiceType(models.Model):
    type_choices = (('energy', 'Energy'), ('ancillary-services', 'Ancillary-services'), ('thermal_energy', 'Heat'),
                    ('it_load', 'IT Load'))
    type = models.CharField(choices=type_choices, default='energy', max_length=100,
                            help_text="type of market (energy, flexibility, heat, IT load)")
    timeFrameid = models.ForeignKey(TimeFrame, db_column='TimeFrame_id')

    def __str__(self):
        return "Service type {} - {}".format(self.type, self.timeFrameid)

    class Meta:
        ordering = ('id',)
        db_table = 'MarketServiceType'


class Marketplace(models.Model):
    marketOperatorid = models.ForeignKey(MarketActor, related_name="marketplace_operator",
                                         help_text="market Operator identity number", db_column='MarketOperator_id')
    marketServiceTypeid = models.ForeignKey(MarketServiceType, help_text="description of the location",
                                            db_column='MarketServiceType_id')
    dSOid = models.ForeignKey(MarketActor, related_name="marketplace_dso", help_text="DSO identity number",
                              db_column='DSO_id')

    def __str__(self):
        return "Marketplace {} ({})".format(self.id, self.marketServiceTypeid.type)

    class Meta:
        ordering = ('id',)
        db_table = 'Marketplace'


class SessionStatus(models.Model):
    type_choices = (('active', 'Active'), ('inactive', 'Inactive'), ('closed', 'Closed'), ('cleared', 'Cleared'),
                    ('completed', 'Completed'))
    status = models.CharField(choices=type_choices, max_length=45)

    def __str__(self):
        return "Status id {}, status {}".format(self.id, self.status)

    class Meta:
        ordering = ('id',)
        db_table = 'SessionStatus'


class MarketSession(models.Model):
    sessionStartTime = models.DateTimeField(help_text="session Start time")
    sessionEndTime = models.DateTimeField(help_text="session end time")
    deliveryStartTime = models.DateTimeField(help_text="delivery start time")
    deliveryEndTime = models.DateTimeField(help_text="delivery end time")
    marketplaceid = models.ForeignKey(Marketplace, help_text="MarketPlace identity number", db_column='Marketplace_id')
    sessionStatusid = models.ForeignKey(SessionStatus, help_text="session status identity number",
                                        db_column='SessionStatus_id')
    formid = models.ForeignKey(Form, help_text="energy form identity number", db_column='Form_id')
    clearingPrice = models.IntegerField(help_text="equilibribrium price resulting from the market clearing process",
                                        null=True, blank=True)

    def __str__(self):
        return "Session {} {} {}".format(self.id, self.sessionStatusid, self.formid)

    class Meta:
        ordering = ('id',)
        db_table = 'MarketSession'


class Load(models.Model):
    date = models.DateTimeField(help_text="date of the posted action")

    def __str__(self):
        return "Load id: {}, date: {}".format(self.id, self.date)

    class Meta:
        ordering = ('id',)
        db_table = 'Load'


class LoadValue(models.Model):
    loadid = models.ForeignKey(Load, help_text="Load identity number", db_column="Load_id")
    parameter = models.CharField(max_length=45, help_text="name of the load value (CPU, RAM, DISK)")
    uom = models.CharField(max_length=45,
                           help_text="name of the measurement unit, considering the type of parameter ( CPU -> number of cores, RAM -> MBs, DISK -> GBs)")
    value = models.DecimalField(max_digits=10, decimal_places=3, default=0.0,
                                help_text="the value representing the number of uom's declared")

    def __str__(self):
        return "LoadValue id:{} -> load id : {}, parameter: {}, uom: {}, value: {}".format(self.id, self.loadid,
                                                                                           self.parameter, self.uom,
                                                                                           self.value)

    class Meta:
        ordering = ('id',)
        db_table = 'LoadValue'


class MarketAction(models.Model):
    date = models.DateTimeField(help_text="date of invoicing")
    actionStartTime = models.DateTimeField(help_text="action start time")
    actionEndTime = models.DateTimeField(help_text="action end time")
    value = models.DecimalField(max_digits=10, decimal_places=3, help_text="market session identity number")
    uom = models.CharField(max_length=45, help_text="unit of measurement")
    price = models.DecimalField(max_digits=10, decimal_places=3, help_text="commodity price")

    deliveryPoint = models.CharField(max_length=45, blank=True, null=True, help_text="delivery point")
    marketSessionid = models.ForeignKey(MarketSession, help_text="market session identity number",
                                        db_column='MarketSession_id')
    marketActorid = models.ForeignKey(MarketActor, help_text="market actor identity number",
                                      db_column='MarketActor_id')
    formid = models.ForeignKey(Form, help_text="energy form identity number", db_column='Form_id')
    actionTypeid = models.ForeignKey(ActionType, help_text="action type identity number", db_column='ActionType_id')
    statusid = models.ForeignKey(ActionStatus, help_text="action status identity number",
                                 db_column='ActionStatus_id')
    loadid = models.ForeignKey(Load, blank=True, null=True, help_text="load identity number",
                               db_column="Load_id")


    def __str__(self):
        return "Market Action id: {}, actionTypeId: {} formId: {}  marketSessionId: {} statusId: {}, loadId: {}".format(
            self.id, self.actionTypeid, self.formid, self.marketSessionid,
            self.statusid, self.loadid)

    class Meta:
        ordering = ('id',)
        db_table = 'MarketAction'


class MarketActionCounterOffer(models.Model):
    marketAction_Bid_id = models.ForeignKey(MarketAction, related_name='bids',
                                            help_text="counter bid related to the market action",
                                            db_column='MarketAction_Bid_id')
    marketAction_Offer_id = models.ForeignKey(MarketAction, related_name='offers',
                                              help_text="counter offer related to the market action",
                                              db_column='MarketAction_Offer_id')
    exchangedValue = models.DecimalField(max_digits=10, decimal_places=3, help_text="Exchange value")

    def __str__(self):
        return "Counter Offer: {} -> {}".format(self.marketAction_Bid_id, self.marketAction_Offer_id)

    class Meta:
        ordering = ('id',)
        db_table = 'MarketActionCounterOffer'


class Transaction(models.Model):
    dateTime = models.DateTimeField(help_text="transaction date")
    marketActionCounterOfferid = models.ForeignKey(MarketActionCounterOffer,
                                                   help_text="identity number of the related row in MarketActionCounterOffer",
                                                   db_column='MarketActionCounterOffer_id')

    def __str__(self):
        return "Transaction {} {}".format(self.id, self.marketActionCounterOfferid)

    class Meta:
        ordering = ('id',)
        db_table = 'Transaction'


class Invoice(models.Model):
    date = models.DateTimeField(help_text="date of invoicing")
    penalty = models.DecimalField(max_digits=10, decimal_places=3, default=0.0,
                                  help_text="a penalty will be applied equal to the fixed fee", null=True)
    amount = models.DecimalField(max_digits=10, decimal_places=3, default=0.0,
                                 help_text="price paid for the energy/service delivery", null=True)
    fixedFee = models.DecimalField(max_digits=10, decimal_places=3, default=0.0, help_text="fixed fee amount",
                                   null=True)
    transactionid = models.ForeignKey(Transaction, help_text="identity number of the related transaction",
                                      db_column='Transaction_id')

    def __str__(self):
        return "Invoice {} -> {}".format(self.id, self.transactionid)

    class Meta:
        ordering = ('id',)
        db_table = 'Invoice'


class Marketplace_has_MarketActor(models.Model):
    marketplace = models.ForeignKey(Marketplace, help_text="MarketPlace identity number", db_column='Marketplace_id')
    marketActor = models.ForeignKey(MarketActor, help_text="MarketActor identity number", db_column='MarketActor_id')

    def __str__(self):
        return "{} -> {}".format(self.marketActor, self.marketplace)

    class Meta:
        ordering = ('id',)
        db_table = 'Marketplace_has_MarketActor'


class ElectricityPrice(models.Model):
    price = models.DecimalField(max_digits=10, decimal_places=3, help_text="Electricity Price")

    class Meta:
        ordering = ('price',)
        db_table = 'ElectricityPrice'


class ReferencePrice(models.Model):
    price = models.DecimalField(max_digits=10, decimal_places=3, help_text="commodity reference price")
    validityStartTime = models.DateTimeField(help_text="validity start time for the reference price")
    validityEndTime = models.DateTimeField(help_text="validity end time for the reference price")
    formId = models.ForeignKey(Form, help_text="energy form identity number", db_column='Form_id')
    marketplaceId = models.ForeignKey(Marketplace,
                                      help_text="marketplace identity number where to apply the reference price",
                                      db_column='Marketplace_id')

    def __str__(self):
        return "Reference price {} -> price {}, form id {}, marketplace id {} ".format(self.id, self.price, self.formId,
                                                                                       self.marketplaceId)

    class Meta:
        ordering = ('id',)
        db_table = 'ReferencePrice'


class Rule(models.Model):
    title = models.CharField(max_length=10000, help_text="Catalyst title")
    description = models.CharField(max_length=10000, help_text="description")
    timestamp = models.DateTimeField(help_text="timestamp")

    class Meta:
        ordering = ('id',)
        db_table = 'Rule'


class UserActorRel(models.Model):
    from django.contrib.auth.models import User
    STATUS_CHOICES = (
        ("unknown", "Unknown"),
        ("valid", "Valid"),
        ("invalid", "Invalid")
    )
    user = models.ForeignKey(User, primary_key=True, unique=True)
    # user = models.OneToOneField(User, on_delete=models.CASCADE)
    actor = models.ForeignKey(MarketActor)
    status = models.CharField(max_length=1000, choices=STATUS_CHOICES, default="unknown")

    def __str__(self):
        return "user: {} {} -> actor id: {}, name: {}, status: {}, type: {}".format(self.user.id, self.user.username,
                                                                                    self.actor.id,
                                                                                    self.actor.representativeName,
                                                                                    self.status,
                                                                                    self.actor.marketActorTypeid)

    class Meta:
        db_table = 'UserActorRel'


class ListField(models.TextField):
    # __metaclass__ = models.SubfieldBase
    description = "Stores a python list"

    def __init__(self, *args, **kwargs):
        super(ListField, self).__init__(*args, **kwargs)

    def from_db_value(self, value, expression, connection, context):
        return self.to_python(value)

    def to_python(self, value):
        if not value:
            value = []

        if isinstance(value, list):
            return value

        return ast.literal_eval(value)

    def get_prep_value(self, value):
        if value is None:
            return value
        return str(value)

        # return unicode(value)

    def value_to_string(self, obj):
        value = self._get_val_from_obj(obj)
        return self.get_db_prep_value(value)


class SG_Data(models.Model):
    Customer_Number = models.CharField(max_length=30)
    Customer_Name = models.CharField(max_length=64)
    Date = models.CharField(max_length=10)
    Measuring_Period_Duration = models.CharField(max_length=5)
    Value_Identifier = models.CharField(max_length=10)
    Unique_cut_No = models.CharField(max_length=5)
    Unique_CHA_No = models.CharField(max_length=64)
    Device_ID = models.CharField(max_length=10)
    Measurements = ListField()
    Port_Number = models.CharField(max_length=5)
    Unit = models.CharField(max_length=10)
    Channel_Name = models.CharField(max_length=10)
    Timestamp = models.CharField(max_length=8)

    class Meta:
        abstract = True
        ordering = ('Date',)


class SG_Consumption(SG_Data):
    Transformer_Ratio = models.CharField(max_length=5)

    class Meta:
        db_table = "sg_consumption"
        ordering = ('Date',)


class SG_Generation(SG_Data):
    Transformer_Ratio = models.CharField(max_length=5)

    class Meta:
        db_table = "sg_generation"
        ordering = ('Date',)

