from django.conf import settings
from pytz import timezone
import requests, json
from rest_framework.response import Response
from django.core.serializers.json import DjangoJSONEncoder
import logging

logging.config.dictConfig(settings.LOGGING)
log = logging.getLogger('ib')

def setUTCFormatter(date_time):
    format = "%Y-%m-%d %H:%M %z"
    datetime_obj_utc = date_time.replace(tzinfo=timezone('UTC'))
    return datetime_obj_utc.strftime(format)


def postActions(actions, url):
    if not actions:
        return Response(status=400, data={"details": "Invalid input. No actions provided."})

    log.info("ACTIONS to be posted to {}: {}".format(url, json.dumps(actions, cls=DjangoJSONEncoder)))
    headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}
    try:
        response = requests.post(url, data=json.dumps(actions, cls=DjangoJSONEncoder), headers=headers)
    except Exception as e:
        log.error("Error invoking MSM", e)
        return Response(status=404, data={"details": "Error invoking MSM service"})
    return Response(status=response.status_code, data=response.content)

def postActionsWithTimeout(actions, url, timeout):
    if not actions:
        return Response(status=400, data={"details": "Invalid input. No actions provided."})

    log.info("ACTIONS to be posted to {}: {}".format(url, json.dumps(actions, cls=DjangoJSONEncoder)))
    headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}
    try:
        response = requests.post(url, data=json.dumps(actions, cls=DjangoJSONEncoder), headers=headers, timeout=timeout)
    except Exception as e:
        log.error("Error invoking MSM", e)
        return Response(status=404, data={"details": "Error invoking MSM service"})
    return Response(status=response.status_code, data=response.json())