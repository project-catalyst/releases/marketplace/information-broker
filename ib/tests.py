from datetime import timedelta, datetime

from django.conf import settings
from django.utils import timezone
from oauth2_provider.admin import Application, AccessToken
from oauth2_provider.backends import UserModel
from rest_framework.test import APIClient, APITestCase

from ib.models import Marketplace, MarketActor, MarketActorType, MarketServiceType, TimeFrame, Form, SessionStatus, \
    MarketSession, ActionType, ActionStatus, MarketAction, ElectricityPrice, Marketplace_has_MarketActor, \
    MarketActionCounterOffer, Transaction, Invoice, ReferencePrice, Rule


class test_IB(APITestCase):
    """Test module for /marketplace/ endpoint"""

    def setUp(self):
        el_price = ElectricityPrice.objects.create(price=56.1223)
        print(el_price)
        form = Form.objects.create(Form="energy form")
        form1 = Form.objects.create(Form="regulation")
        ses_status = SessionStatus.objects.create(status='active')
        ses_status1 = SessionStatus.objects.create(status='inactive')
        ses_status2 = SessionStatus.objects.create(status='closed')
        ses_status3 = SessionStatus.objects.create(status='cleared')
        ses_status4 = SessionStatus.objects.create(status='completed')
        act_type1 = ActionType.objects.create(type='offer')
        act_type2 = ActionType.objects.create(type='bid')
        act_status1 = ActionStatus.objects.create(status='unchecked')
        act_status2 = ActionStatus.objects.create(status='valid')
        act_status3 = ActionStatus.objects.create(status='invalid')
        act_status4 = ActionStatus.objects.create(status='active')
        act_status5 = ActionStatus.objects.create(status='inactive')
        act_status6 = ActionStatus.objects.create(status='withdrawn')
        act_status7 = ActionStatus.objects.create(status='accepted')
        act_status8 = ActionStatus.objects.create(status='partially accepted')
        act_status9 = ActionStatus.objects.create(status='rejected')
        act_status10 = ActionStatus.objects.create(status='selected')
        act_status11 = ActionStatus.objects.create(status='delivered')
        act_status12 = ActionStatus.objects.create(status='not delivered')
        act_status13 = ActionStatus.objects.create(status='billed')
        time = TimeFrame.objects.create(type="intra_day")
        actor_type1 = MarketActorType.objects.create(type='dso')
        typeOp = MarketActorType.objects.create(type='market operator')
        actor_type2 = MarketActorType.objects.create(type='aggregator')
        actor_type3 = MarketActorType.objects.create(type='generic participant')
        dso1 = MarketActor.objects.create(companyName='SIlo', email="actor@silo.com", phone="30267788901",
                                          representativeName="dso", VAT="8901", MarketActorType_id=actor_type1)
        operator1 = MarketActor.objects.create(companyName="DEH", email="op@deh.gr", phone="245788111",
                                               representativeName="deh 1", VAT="3456", MarketActorType_id=typeOp)
        service_type = MarketServiceType.objects.create(type="energy", TimeFrame_id=time)
        dso2 = MarketActor.objects.create(companyName='AUEB', email="actor@aueb.com", phone="30267788901",
                                          representativeName="aueb dso", VAT="8901",
                                          MarketActorType_id=actor_type1)
        operator2 = MarketActor.objects.create(companyName="ceid", email="op@ceid.gr", phone="245788111",
                                               representativeName="ceid 1", VAT="3456", MarketActorType_id=typeOp)
        operator3 = MarketActor.objects.create(companyName="ekpa", email="op@ekpa.gr", phone="245788111",
                                               representativeName="ekpa 1", VAT="3456", MarketActorType_id=typeOp)

        m = Marketplace.objects.create(DSO_id=dso1, MarketOperator_id=operator1, MarketServiceType_id=service_type)
        m1 = Marketplace.objects.create(DSO_id=dso2, MarketOperator_id=operator2, MarketServiceType_id=service_type)
        session = MarketSession.objects.create(Marketplace_id=m, SessionStatus_id=ses_status, Form_id=form,
                                               sessionStartTime="2012-03-31T11:06:05.182371Z",
                                               sessionEndTime="2012-03-31T11:06:05.182371Z",
                                               deliveryStartTime="2012-03-31T11:06:05.182371Z",
                                               deliveryEndTime="2012-03-31T11:06:05.182371Z",
                                               clearingPrice=10)
        session1 = MarketSession.objects.create(Marketplace_id=m1, SessionStatus_id=ses_status, Form_id=form,
                                                sessionStartTime="2012-03-31T11:06:05.182371Z",
                                                sessionEndTime="2012-03-31T11:06:05.182371Z",
                                                deliveryStartTime="2012-03-31T11:06:05.182371Z",
                                                deliveryEndTime="2012-03-31T11:06:05.182371Z",
                                                clearingPrice=10)
        print("session  ID {}".format(session.id))
        print("session1 ID {}".format(session1.id))
        session_closed = MarketSession.objects.create(Marketplace_id=m1, SessionStatus_id=ses_status2, Form_id=form,
                                                      sessionStartTime="2012-03-31T11:06:05.182371Z",
                                                      sessionEndTime="2012-03-31T11:06:05.182371Z",
                                                      deliveryStartTime="2012-03-31T11:06:05.182371Z",
                                                      deliveryEndTime="2012-03-31T11:06:05.182371Z",
                                                      clearingPrice=10)
        session_cleared = MarketSession.objects.create(Marketplace_id=m1, SessionStatus_id=ses_status3, Form_id=form,
                                                       sessionStartTime=datetime.today(),
                                                       sessionEndTime=datetime.today(),
                                                       deliveryStartTime=datetime.today(),
                                                       deliveryEndTime=datetime.today(),
                                                       clearingPrice=18)
        action_billed = MarketAction.objects.create(date=datetime.today(), actionStartTime=datetime.today(),
                                                    actionEndTime=datetime.today(), value=34.789, uom="kwh",
                                                    price=56.789,
                                                    deliveryPoint='Athens', MarketSession_id=session_cleared,
                                                    MarketActor_id=operator3, Form_id=form, ActionType_id=act_type1,
                                                    ActionStatus_id=act_status13)
        offer_notbilled = MarketAction.objects.create(date=datetime.today(), actionStartTime=datetime.today(),
                                                      actionEndTime=datetime.today(), value=34.789, uom="kwh",
                                                      price=56.789,
                                                      deliveryPoint='Athens', MarketSession_id=session_cleared,
                                                      MarketActor_id=operator3, Form_id=form, ActionType_id=act_type1,
                                                      ActionStatus_id=act_status12)
        action1 = MarketAction.objects.create(date=datetime.today(), actionStartTime=datetime.today(),
                                              actionEndTime=datetime.today(), value=34.789, uom="kwh", price=56.789,
                                              deliveryPoint='Athens', MarketSession_id=session,
                                              MarketActor_id=operator3, Form_id=form, ActionType_id=act_type1,
                                              ActionStatus_id=act_status4)
        action2 = MarketAction.objects.create(date=datetime.today(), actionStartTime=datetime.today(),
                                              actionEndTime=datetime.today(), value=14.789, uom="kwh", price=26.889,
                                              deliveryPoint='Patras', MarketSession_id=session1,
                                              MarketActor_id=operator2, Form_id=form, ActionType_id=act_type2,
                                              ActionStatus_id=act_status4)
        action_active = MarketAction.objects.create(date=datetime.today(), actionStartTime=datetime.today(),
                                                    actionEndTime=datetime.today(), value=34.789, uom="kwh",
                                                    price=16.889,
                                                    deliveryPoint='Heraklion', MarketSession_id=session1,
                                                    MarketActor_id=operator2, Form_id=form, ActionType_id=act_type1,
                                                    ActionStatus_id=act_status4)
        bid_active = MarketAction.objects.create(date=datetime.today(), actionStartTime=datetime.today(),
                                                 actionEndTime=datetime.today(), value=34.789, uom="kwh",
                                                 price=16.889,
                                                 deliveryPoint='Heraklion', MarketSession_id=session1,
                                                 MarketActor_id=operator2, Form_id=form, ActionType_id=act_type2,
                                                 ActionStatus_id=act_status4)
        delivered_action = MarketAction.objects.create(date=datetime.today(), actionStartTime=datetime.today(),
                                                       actionEndTime=datetime.today(), value=674.789, uom="kwh",
                                                       price=96.889,
                                                       deliveryPoint='Kalamata', MarketSession_id=session1,
                                                       MarketActor_id=operator2, Form_id=form, ActionType_id=act_type2,
                                                       ActionStatus_id=act_status11)
        participant1 = MarketActor.objects.create(companyName='SIlo', email="actor@silo.com", phone="30267788901",
                                                  representativeName="participant1", VAT="8901",
                                                  MarketActorType_id=actor_type3)
        participant2 = MarketActor.objects.create(companyName='faskomhlia', email="actor@gmail.com",
                                                  phone="30267788901",
                                                  representativeName="asteri ths faskomhlias", VAT="8901",
                                                  MarketActorType_id=actor_type3)
        counterOffer = MarketActionCounterOffer.objects.create(MarketAction_Bid_id=action2,
                                                               MarketAction_Offer_id=action_active,
                                                               exchangedValue=45.6789)
        rule1 = Rule.objects.create(title='Market Variant',
                                    description='The GEYSER Marketplace realizes two variants:\n1) GEYSER Energy Marketplace (GEM)\n2) GEYSER Ancillary Services Marketplace (GAM)\n',
                                    timestamp='2016-06-30T14:08:58Z', value=0)
        transaction = Transaction.objects.create(dateTime=datetime.today(), MarketActionCounterOffer_id=counterOffer)
        tmp = Marketplace_has_MarketActor.objects.create(Marketplace_id=m, MarketActor_id=participant1)
        tmp1 = Marketplace_has_MarketActor.objects.create(Marketplace_id=m1, MarketActor_id=participant2)
        rp1 = ReferencePrice.objects.create(price=45.67, Marketplace_id=m1, Form_id=form,
                                            validityStartTime="2016-10-01 00:00:00",
                                            validityEndTime="2018-10-01 23:59:59")
        rp2 = ReferencePrice.objects.create(price=65.67, Marketplace_id=m, Form_id=form,
                                            validityStartTime="2016-10-01 00:00:00",
                                            validityEndTime="2018-10-01 23:59:59")
        rp3 = ReferencePrice.objects.create(price=25.67, Marketplace_id=m1, Form_id=form1,
                                            validityStartTime="2016-10-02 00:00:00",
                                            validityEndTime="2018-10-02 23:59:59")
        self.test_user = UserModel.objects.create_user("test_user", "test@user.com", "123456")
        self.test_user1 = UserModel.objects.create_user("test_user1", "test@user1.com", "123457")
        self.test_user2 = UserModel.objects.create_user("test_user2", "test@user2.com", "123458")
        self.application1 = Application(
            name="Test Application1",
            redirect_uris="http://localhost",
            user=self.test_user1,
            client_type=Application.CLIENT_CONFIDENTIAL,
            authorization_grant_type=Application.GRANT_AUTHORIZATION_CODE,
        )
        self.application1.save()
        self.application2 = Application(
            name="Test Application2",
            redirect_uris="http://localhost",
            user=self.test_user2,
            client_type=Application.CLIENT_CONFIDENTIAL,
            authorization_grant_type=Application.GRANT_AUTHORIZATION_CODE,
        )
        self.application2.save()
        self.tok2 = AccessToken.objects.create(
            user=self.test_user2, token='19999999999',
            application=self.application2, scope='write',
            expires=timezone.now() + timedelta(days=1)
        )
        self.tok1 = AccessToken.objects.create(
            user=self.test_user1, token='0987654321',
            application=self.application1, scope='read',
            expires=timezone.now() + timedelta(days=1)
        )

        self.application = Application(
            name="Test Application",
            redirect_uris="http://localhost",
            user=self.test_user,
            client_type=Application.CLIENT_CONFIDENTIAL,
            authorization_grant_type=Application.GRANT_AUTHORIZATION_CODE,
        )
        self.application.save()
        self.tok = AccessToken.objects.create(
            user=self.test_user, token='1234567890',
            application=self.application, scope='read write',
            expires=timezone.now() + timedelta(days=1)
        )

    # The bellow functions test /marketplace/ endpoint (GET/POST)
    def test_getAllTest(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/', format='json')
        # print(response.content)
        self.assertEqual(response.status_code, 200)

    def test_getAnauthorized(self):
        client = APIClient()
        response = client.get('/marketplace/', format='json')
        self.assertEqual(response.status_code, 401)

    def test_userOnlyWrite(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok2.token)
        response = client.get('/marketplace/', format='json')
        self.assertEqual(response.status_code, 403)

    def test_postData(self):
        # print(MarketPlace.objects.all())
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {"DSO_id": 1, "MarketOperator_id": 2, "MarketServiceType_id": 1}
        response = client.post('/marketplace/', data, format='json')
        # print(MarketPlace.objects.all())
        self.assertEqual(response.status_code, 201)

    def test_postWrongData(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {"DSO_id": 1, "MarketOperator_id": 2}
        response = client.post('/marketplace/', data, format='json')
        self.assertEqual(response.status_code, 400)

    def test_postNullData(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {}
        response = client.post('/marketplace/', data, format='json')
        self.assertEqual(response.status_code, 400)

    def test_postAnauthorized(self):
        client = APIClient()
        data = {"DSOid": 1, "marketOperatorId": 2, "marketServiceTypeId": 1}
        response = client.post('/marketplace/', data, format='json')
        self.assertEqual(response.status_code, 401)

    def test_UserOnlyRead(self):
        client = APIClient()
        data = {"DSO_id": 1, "MarketOperator_id": 2, "MarketServiceType_id": 1}
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok1.token)
        response = client.post('/marketplace/', data, format='json')
        self.assertEqual(response.status_code, 403)

    # End of testing for /marketplace/ endpoint

    #### Test /marketplce/<pk>/ endpoint ####

    def test_getMarketPlaceDetail(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/1/', format='json')
        self.assertEqual(response.status_code, 200)

    def test_getNotExistingMarketPlace(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/10/', format='json')
        self.assertEqual(response.status_code, 404)

    def test_getMarketPlaceDetailAnAuth(self):
        client = APIClient()
        response = client.get('/marketplace/2/', format='json')
        self.assertEqual(response.status_code, 401)

    def test_getMarketPlaceDetailNonReadScope(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok2.token)
        response = client.get('/marketplace/2/', format='json')
        self.assertEqual(response.status_code, 403)

    def test_putMarketPlace(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {"DSO_id": 2, "MarketOperator_id": 3, "MarketServiceType_id": 1}
        response = client.put('/marketplace/1/', data, format='json')
        self.assertEqual(response.status_code, 200)

    def test_put_notExistingMarketPlace(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {"DSO_id": 2, "MarketOperator_id": 3, "MarketServiceType_id": 1}
        response = client.put('/marketplace/10/', data, format='json')
        self.assertEqual(response.status_code, 404)

    def test_putMarketPlace_NullData(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {}
        print(Marketplace.objects.get(pk=1))
        response = client.put('/marketplace/1/', data, format='json')
        self.assertEqual(response.status_code, 200)

    def test_putMarketPlacePartial(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {"MarketOperator_id": 1}
        response = client.put('/marketplace/1/', data, format='json')
        self.assertEqual(response.status_code, 200)

    def test_putNotAuthorizedMarketPlaceDetail(self):
        client = APIClient()
        data = {}
        response = client.put('/marketplace/1/', data, format='json')
        self.assertEqual(response.status_code, 401)

    def test_put_ReadOnlyMarketPlaceDetail(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok1.token)
        data = {}
        response = client.put('/marketplace/1/', data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_deleteMarketPlace(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.delete('/marketplace/1/')
        self.assertEqual(response.status_code, 204)

    def test_deleteNotExistingMarketPlace(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.delete('/marketplace/10/')
        self.assertEqual(response.status_code, 404)

    def test_deleteUnauthorizedMarketPlace(self):
        client = APIClient()
        response = client.delete('/marketplace/1/')
        self.assertEqual(response.status_code, 401)

    def test_read_Only_delete_MarketPlace(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok1.token)
        response = client.delete('/marketplace/1/')
        self.assertEqual(response.status_code, 403)

    #### End of testing /marketplce/<pk>/ endpoint ###

    ### Test /marketplace/<pk>/marketsessions/ endpoint ###
    def test_get_marketSessions(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/1/marketsessions/')
        self.assertEqual(response.status_code, 200)

    def test_get_AnauthorizedMarketSessions(self):
        client = APIClient()
        response = client.get('/marketplace/1/marketsessions/')
        self.assertEqual(response.status_code, 401)

    def test_getNotExistingMarketSessions(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/2/marketsessions/')
        self.assertEqual(response.status_code, 200)

    def test_get_notExistingMarketPlaceMarketSessions(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/20/marketsessions/')
        self.assertEqual(response.status_code, 404)

    def test_ForbiddenMarketSessions(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok2.token)
        response = client.get('/marketplace/1/marketsessions/')
        self.assertEqual(response.status_code, 403)

    def test_post_MarketSessions(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {'Marketplace_id': 2, 'SessionStatus_id': 2, 'Form_id': 1, 'sessionStartTime': datetime.today(),
                'sessionEndTime': datetime.today(), 'deliveryStartTime': datetime.today(),
                'deliveryEndTime': datetime.today(), 'clearingPrice': 10}
        response = client.post('/marketplace/2/marketsessions/', data, format='json')
        self.assertEqual(response.status_code, 201)

    def test_post_MarketSessionOnNonExistingMarketPlace(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {'Marketplace_id': 20, 'SessionStatus_id': 2, 'Form_id': 1, 'sessionStartTime': datetime.today(),
                'sessionEndTime': datetime.today(), 'deliveryStartTime': datetime.today(),
                'deliveryEndTime': datetime.today(), 'clearingPrice': 10}
        response = client.post('/marketplace/20/marketsessions/', data, format='json')
        self.assertEqual(response.status_code, 404)

    def test_post_MarketSessionsUnauthorized(self):
        client = APIClient()
        data = {'Marketplace_id': 2, 'SessionStatus_id': 2, 'Form_id': 1, 'sessionStartTime': datetime.today(),
                'sessionEndTime': datetime.today(), 'deliveryStartTime': datetime.today(),
                'deliveryEndTime': datetime.today(), 'clearingPrice': 10}
        response = client.post('/marketplace/2/marketsessions/', data, format='json')
        self.assertEqual(response.status_code, 401)

    def test_postMarketSessionsOnlyRead(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok1.token)
        data = {'Marketplace_id': 2, 'SessionStatus_id': 2, 'Form_id': 1, 'sessionStartTime': datetime.today(),
                'sessionEndTime': datetime.today(), 'deliveryStartTime': datetime.today(),
                'deliveryEndTime': datetime.today(), 'clearingPrice': 10}
        response = client.post('/marketplace/2/marketsessions/', data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_postNullSessionData(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {}
        response = client.post('/marketplace/1/marketsessions/', data, format='json')
        self.assertEqual(response.status_code, 400)

    def test_postWrongSessionData(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {'Marketplace_id': 2, 'SessionStatus_id': 2, 'Form_id': 1, 'sessionStartTime': datetime.today(),
                'sessionEndTime': datetime.today()}
        response = client.post('/marketplace/1/marketsessions/', data, format='json')
        self.assertEqual(response.status_code, 400)

    ### End Testing /marketplace/<pk>/marketsessions/ ###

    ### Test marketplace/(?P<pk>[0-9]+)/marketsessions/active/ ###

    def test_getActiveMarketSessions(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/1/marketsessions/active/', format='json')
        self.assertEqual(response.status_code, 200)

    def test_getNoExistingMarketPlaceActiveSessions(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/11/marketsessions/active/', format='json')
        self.assertEqual(response.status_code, 404)

    def test_getActiveSessionsUnauthorized(self):
        client = APIClient()
        response = client.get('/marketplace/1/marketsessions/active/', format='json')
        self.assertEqual(response.status_code, 401)

    def test_getEmptyActiveSessions(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/2/marketsessions/active/', format='json')
        self.assertEqual(response.status_code, 200)

    ### End testing marketplace/(?P<pk>[0-9]+)/marketsessions/active/ endpoint ###

    ### Test marketplace/all/update/marketsessions/(?P<session_status>[\w\-]+)/ endpoint ###

    def test_allUpdate(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/all/update/marketsessions/cleared/', format='json')
        self.assertEqual(response.status_code, 200)

    def test_wrong_allUpdate(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/all/update/marketsessions/active/', format='json')
        self.assertEqual(response.status_code, 404)

    def test_allUpdateUnauthorized(self):
        client = APIClient()
        response = client.get('/marketplace/all/update/marketsessions/completed/', format='json')
        self.assertEqual(response.status_code, 401)

    def test_allUpdatePut(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {}
        response = client.put('/marketplace/all/update/marketsessions/closed/', data)
        self.assertEqual(response.status_code, 200)

    def test_allUpdatePutForbidden(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok1.token)
        response = client.put('/marketplace/all/update/marketsessions/closed/')
        self.assertEqual(response.status_code, 403)

    def test_allUpdatePutWrong(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.put('/marketplace/all/update/marketsessions/cleared/')
        self.assertEqual(response.status_code, 404)

    ### End of testing marketplace/all/update/marketsessions/(?P<session_status>[\w\-]+)/ endpoint ###

    ### marketplace/(?P<pk>[0-9]+)/marketsessions/(?P<sid>\d+)/endclear/ endpoint ###

    def test_putClearedSessions(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {}
        response = client.put('/marketplace/1/marketsessions/1/endclear/', data, format='json')
        self.assertEqual(response.status_code, 200)

    def test_putUnauthorizedClearedSessions(self):
        client = APIClient()
        data = {}
        response = client.put('/marketplace/1/marketsessions/1/endclear/', data, format='json')
        self.assertEqual(response.status_code, 401)

    def test_putReadOnlyClearedSessions(self):
        client = APIClient()
        data = {}
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok1.token)
        response = client.put('/marketplace/1/marketsessions/1/endclear/', data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_putClearedSessionsNotExisting(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {}
        response = client.put('/marketplace/10/marketsessions/1/endclear/', data, format='json')
        self.assertEqual(response.status_code, 404)

    def test_putClearedSessionNotExistingSession(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {}
        response = client.put('/marketplace/1/marketsessions/12/endclear/', data, format='json')
        self.assertEqual(response.status_code, 404)

    ### End testing marketplace/(?P<pk>[0-9]+)/marketsessions/(?P<sid>\d+)/endclear/ endpoint ###

    def test_putCompletedSessions(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {}
        response = client.put('/marketplace/1/marketsessions/1/completion/', data, format='json')
        self.assertEqual(response.status_code, 200)

    def test_putCompletedSessionsUnauthorized(self):
        client = APIClient()
        data = {}
        response = client.put('/marketplace/1/marketsessions/1/completion/', data, format='json')
        self.assertEqual(response.status_code, 401)

    def test_putCompletedSessionsReadOnly(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok1.token)
        data = {}
        response = client.put('/marketplace/1/marketsessions/1/completion/', data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_putCompletedSessionsNotExisting(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {}
        response = client.put('/marketplace/10/marketsessions/1/completion/', data, format='json')
        self.assertEqual(response.status_code, 404)

    def test_putCompletedSessionNotExistingSession(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {}
        response = client.put('/marketplace/1/marketsessions/12/completion/', data, format='json')
        self.assertEqual(response.status_code, 404)

    ### End testing marketplace/(?P<pk>[0-9]+)/marketsessions/(?P<sid>\d+)/completion/ endpoint ###

    def test_putEndedSessions(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {}
        response = client.put('/marketplace/1/marketsessions/1/end/', data, format='json')
        self.assertEqual(response.status_code, 200)

    def test_putEndedSessionsUnauthorized(self):
        client = APIClient()
        data = {}
        response = client.put('/marketplace/1/marketsessions/1/end/', data, format='json')
        self.assertEqual(response.status_code, 401)

    def test_putEndedSessionsReadOnly(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok1.token)
        data = {}
        response = client.put('/marketplace/1/marketsessions/1/end/', data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_putEndedSessionsNotExisting(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {}
        response = client.put('/marketplace/10/marketsessions/1/end/', data, format='json')
        self.assertEqual(response.status_code, 404)

    def test_putEndedSessionsNotExistingSession(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {}
        response = client.put('/marketplace/1/marketsessions/21/end/', data, format='json')
        self.assertEqual(response.status_code, 404)

        ### End testing marketplace/(?P<pk>[0-9]+)/marketsessions/(?P<sid>\d+)/end/ endpoint ###

    def test_getOffers(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/1/marketsessions/1/offers/', format='json')
        self.assertEqual(response.status_code, 200)

    def test_getOffersUnauthorized(self):
        client = APIClient()
        response = client.get('/marketplace/1/marketsessions/1/offers/', format='json')
        self.assertEqual(response.status_code, 401)

    def test_getOffersNotExistingPlace(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/10/marketsessions/1/offers/', format='json')
        self.assertEqual(response.status_code, 404)

    def test_getOffersNotExistingSession(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/1/marketsessions/10/offers/', format='json')
        self.assertEqual(response.status_code, 404)

    def test_getOffersNonRead(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok2.token)
        response = client.get('/marketplace/1/marketsessions/1/offers/', format='json')
        self.assertEqual(response.status_code, 403)

    ### End testing marketplace/(?P<pk>[0-9]+)/marketsessions/(?P<sid>\d+)/offers/ ####

    def test_getBids(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/2/marketsessions/2/bids/', format='json')
        self.assertEqual(response.status_code, 200)

    def test_getBidsUnauthorized(self):
        client = APIClient()
        response = client.get('/marketplace/2/marketsessions/2/bids/', format='json')
        self.assertEqual(response.status_code, 401)

    def test_getBidsNotExistingPlace(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/20/marketsessions/2/bids/', format='json')
        self.assertEqual(response.status_code, 404)

    def test_getBidsNotExistingSession(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/2/marketsessions/20/bids/', format='json')
        self.assertEqual(response.status_code, 404)

    def test_getBidsNonRead(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok2.token)
        response = client.get('/marketplace/2/marketsessions/2/offers/', format='json')
        self.assertEqual(response.status_code, 403)

    ### End testing marketplace/(?P<pk>[0-9]+)/marketsessions/(?P<sid>\d+)/bids/ ####

    def test_MarketAction(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/2/marketsessions/2/actions/', format='json')
        self.assertEqual(response.status_code, 200)

    def test_MarketActionNotAuthorized(self):
        client = APIClient()
        response = client.get('/marketplace/2/marketsessions/2/actions/', format='json')
        self.assertEqual(response.status_code, 401)

    def test_MarketActionNonRead(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok2.token)
        response = client.get('/marketplace/2/marketsessions/2/actions/', format='json')
        self.assertEqual(response.status_code, 403)

    def test_MarketActionNotExistingPlace(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/20/marketsessions/2/actions/', format='json')
        self.assertEqual(response.status_code, 404)

    def test_MarketActionNotExistingSession(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/2/marketsessions/1/actions/', format='json')
        self.assertEqual(response.status_code, 404)

    def test_postMarketAction(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {'date': datetime.today(), 'actionStartTime': datetime.today(), 'actionEndTime': datetime.today(),
                'value': 23.678, 'uom': "kwh", 'price': 45.890, 'deliveryPoint': 'Thessaloniki',
                'MarketSession_id': 2, 'MarketActor_id': 1, 'Form_id': 1, 'ActionType_id': 2, 'ActionStatus_id': 4}
        response = client.post('/marketplace/2/marketsessions/2/actions/', data, format='json')
        self.assertEqual(response.status_code, 201)

    def test_postMarketActionAnauthorized(self):
        client = APIClient()
        data = {'date': datetime.today(), 'actionStartTime': datetime.today(), 'actionEndTime': datetime.today(),
                'value': 23.678, 'uom': "kwh", 'price': 45.890, 'deliveryPoint': 'Thessaloniki',
                'MarketSession_id': 2, 'MarketActor_id': 1, 'Form_id': 1, 'ActionType_id': 2, 'ActionStatus_id': 4}
        response = client.post('/marketplace/2/marketsessions/2/actions/', data, format='json')
        self.assertEqual(response.status_code, 401)

    def test_postMarketActionOnlyReadScope(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok1.token)
        data = {'date': datetime.today(), 'actionStartTime': datetime.today(), 'actionEndTime': datetime.today(),
                'value': 23.678, 'uom': "kwh", 'price': 45.890, 'deliveryPoint': 'Thessaloniki',
                'MarketSession_id': 2, 'MarketActor_id': 1, 'Form_id': 1, 'ActionType_id': 2, 'ActionStatus_id': 4}
        response = client.post('/marketplace/2/marketsessions/2/actions/', data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_postMarketActionNullData(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {}
        response = client.post('/marketplace/2/marketsessions/2/actions/', data, format='json')
        self.assertEqual(response.status_code, 400)

    def test_postMarketActionWrondData1(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {'date': datetime.today(), 'actionStartTime': datetime.today()}
        response = client.post('/marketplace/2/marketsessions/2/actions/', data, format='json')
        self.assertEqual(response.status_code, 400)

    def test_postMarketActionWrondData2(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {'date': 2344, 'actionStartTime': 23455, 'actionEndTime': datetime.today(),
                'value': 23.678, 'uom': "kwh", 'price': 45.890, 'deliveryPoint': 'Thessaloniki',
                'MarketSession_id': 2, 'MarketActor_id': 1, 'Form_id': 1, 'ActionType_id': 2, 'ActionStatus_id': 4}
        response = client.post('/marketplace/2/marketsessions/2/actions/', data, format='json')
        self.assertEqual(response.status_code, 400)

    ### End Testing /marketplace/pk/marketsessions/sid/actions/ ###

    def test_getMarketOperators(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketoperators/1/', format='json')
        self.assertEqual(response.status_code, 200)

    def test_getMarketOperatorsUnauthorized(self):
        client = APIClient()
        response = client.get('/marketoperators/1/', format='json')
        self.assertEqual(response.status_code, 401)

    def test_getMarketOperatorsNotReadScope(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok2.token)
        response = client.get('/marketoperators/1/', format='json')
        self.assertEqual(response.status_code, 403)

    def test_getMarketOperatorsNotExistingPlace(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketoperators/14/', format='json')
        self.assertEqual(response.status_code, 404)

    def test_postMarketOperators(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {"companyName": "protergia", "email": "user@email.com", "phone": "6986672571",
                "representativeName": "Name Surname", "VAT": "023456778999"}
        response = client.post('/marketoperators/1/', data, format='json')
        self.assertEqual(response.status_code, 201)

    def test_postMarketOperatorsUnauthorized(self):
        client = APIClient()
        data = {"companyName": "protergia", "email": "user@email.com", "phone": "6986672571",
                "representativeName": "Name Surname", "VAT": "023456778999"}
        response = client.post('/marketoperators/1/', data, format='json')
        self.assertEqual(response.status_code, 401)

    def test_postMarketOperatorsNotWriteScope(self):
        client = APIClient()
        data = {"companyName": "protergia", "email": "user@email.com", "phone": "6986672571",
                "representativeName": "Name Surname", "VAT": "023456778999"}
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok1.token)
        response = client.post('/marketoperators/1/', data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_postMarketOperatorsNullData(self):
        client = APIClient()
        data = {}
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.post('/marketoperators/1/', data, format='json')
        self.assertEqual(response.status_code, 400)

    def test_postMarketOperatorsWrongData(self):
        client = APIClient()
        data = {"companyName": "protergia", "email": "user@email.com", "phone": "6986672571"}
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.post('/marketoperators/1/', data, format='json')
        self.assertEqual(response.status_code, 400)

    ### End testing /marketoperators/<pk>/ endpoint ###

    def test_getDSO(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/dso/', format='json')
        self.assertEqual(response.status_code, 200)

    def test_getDSOUnauthorized(self):
        client = APIClient()
        response = client.get('/dso/', format='json')
        self.assertEqual(response.status_code, 401)

    def test_getDSONotRead(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok2.token)
        response = client.get('/dso/', format='json')
        self.assertEqual(response.status_code, 403)

    def test_postDSO(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {"companyName": "Mutilinaios", "email": "user1@email1.com.com", "phone": "6986672571",
                "representativeName": "user mut", "VAT": "023456778999"}
        response = client.post('/dso/', data, format='json')
        self.assertEqual(response.status_code, 201)

    def test_postDSOUnauthorized(self):
        client = APIClient()
        data = {"companyName": "Mutilinaios", "email": "user1@email1.com.com", "phone": "6986672571",
                "representativeName": "user mut", "VAT": "023456778999"}
        response = client.post('/dso/', data, format='json')
        self.assertEqual(response.status_code, 401)

    def test_postDSONoWriteScope(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok1.token)
        data = {"companyName": "Mutilinaios", "email": "user1@email1.com.com", "phone": "6986672571",
                "representativeName": "user mut", "VAT": "023456778999"}
        response = client.post('/dso/', data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_postDSONullData(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {}
        response = client.post('/dso/', data, format='json')
        self.assertEqual(response.status_code, 400)

    def test_postDSOWrongData(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {"companyName": "Mutilinaios", "email": "user1@email1.com.com"}
        response = client.post('/dso/', data, format='json')
        self.assertEqual(response.status_code, 400)

    ### End testing /dso/ endpoint ###

    def test_postElectricityPrice(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {'price': 34.78900}
        response = client.post('/price/electricity/', data, format='json')
        self.assertEqual(response.status_code, 201)

    def test_postELectricityPriceUnauthorized(self):
        client = APIClient()
        data = {'price': 34.78900}
        response = client.post('/price/electricity/', data, format='json')
        self.assertEqual(response.status_code, 401)

    def test_postElectricityPriceNullData(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {}
        response = client.post('/price/electricity/', data, format='json')
        self.assertEqual(response.status_code, 400)

    def test_postElectricityPriceBadRequest(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {'electricity': 34.78900}
        response = client.post('/price/electricity/', data, format='json')
        self.assertEqual(response.status_code, 400)

    def test_postElectricityPriceNotWriteScope(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok1.token)
        data = {'price': 34.78900}
        response = client.post('/price/electricity/', data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_getElectricityPrice(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/price/electricity/', format='json')
        self.assertEqual(response.status_code, 200)

    def test_getELectricityPriceUnauthorized(self):
        client = APIClient()
        response = client.get('/price/electricity/', format='json')
        self.assertEqual(response.status_code, 401)

    ### End testing price/electricity/ endpoint ###

    def test_findLastMarketSessionClearingPrice(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/2/marketsessions/form/1/sessionstatus/closed/lastClearingPrice/',
                              format='json')
        self.assertEqual(response.status_code, 200)

    def test_test_findLastMarketSessionClearingPriceUnauthorized(self):
        client = APIClient()
        response = client.get('/marketplace/2/marketsessions/form/1/sessionstatus/closed/lastClearingPrice/',
                              format='json')
        self.assertEqual(response.status_code, 401)

    def test_findLastMarketSessionClearingPriceNoReadScope(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok2.token)
        response = client.get('/marketplace/2/marketsessions/form/1/sessionstatus/closed/lastClearingPrice/',
                              format='json')
        self.assertEqual(response.status_code, 403)

    def test_findLastMarketSessionClearingPrice404(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/20/marketsessions/form/1/sessionstatus/closed/lastClearingPrice/',
                              format='json')
        self.assertEqual(response.status_code, 404)

    # def test_findLastMarketSessionClearingPrice404(self):
    #     client = APIClient()


    ### End testing /marketplace/<pk>/marketsessions/form/<formid>/sessionstatus/closed/lastClearingPrice/ ###

    def test_FindMarketActorInMarketplaceReturnFalse(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/1/marketactor/1/exists/', format='json')
        self.assertEqual(response.status_code, 200)

    def test_FindMarketActorInMarketplaceReturnTrue(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/1/marketactor/6/exists/', format='json')
        self.assertEqual(response.status_code, 200)

    def test_FindMarketActorInMarketplaceUnauthorized(self):
        client = APIClient()
        response = client.get('/marketplace/1/marketactor/6/exists/', format='json')
        self.assertEqual(response.status_code, 401)

    def test_findMarketActorInMarketPlace404(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/100/marketactor/6/exists/', format='json')
        self.assertEqual(response.status_code, 404)

    def test_FindMarketActorInMarketPlace403(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok2.token)
        response = client.get('/marketplace/1/marketactor/6/exists/', format='json')
        self.assertEqual(response.status_code, 403)

    ### End testing /marketplace/<pk>/marketactor/<actor>/exists/ ###

    def test_AllActiveActions(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/1/marketsessions/active/actions/active/')
        self.assertEqual(response.status_code, 200)

    def test_AllActiveOffers(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/1/marketsessions/active/offers/active/')
        self.assertEqual(response.status_code, 200)

    def test_AllActiveBids(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/2/marketsessions/active/bids/active/')
        self.assertEqual(response.status_code, 200)

    def test_AllActiveWrongPath(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/2/marketsessions/active/sessions/active/')
        self.assertEqual(response.status_code, 404)

    def test_AllActiveUnauthorized(self):
        client = APIClient()
        response = client.get('/marketplace/2/marketsessions/active/bids/active/')
        self.assertEqual(response.status_code, 401)

    ### End testing marketplace/(?P<pk>[0-9]+)/marketsessions/active/(?P<type>[\w\-]+)/active/ endpoint ####

    def test_SelectedActionsofGAMSession(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {}
        response = client.put('/marketplace/1/marketsessions/1/actions/3/selected/', data, format='json')
        self.assertEqual(response.status_code, 200)

    def test_SelectedActionsofGAMSessionUnauthorized(self):
        client = APIClient()
        data = {}
        response = client.put('/marketplace/2/marketsessions/2/actions/2/selected/', data, format='json')
        self.assertEqual(response.status_code, 401)

    def test_SelectedActionsofGAMSessionReadOnly(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok1.token)
        data = {}
        response = client.put('/marketplace/2/marketsessions/2/actions/2/selected/', data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_SelectedActionsofGAMSession404(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {}
        response = client.put('/marketplace/20/marketsessions/2/actions/2/selected/', data, format='json')
        self.assertEqual(response.status_code, 404)
        ### End testing /marketplace/<pk>/marketsessions/<sid>/actions/<action_id>/selected/ endpoint ###

    def test_ModifyMarketSessions(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)

        data = {'Marketplace_id': 1, 'SessionStatus_id': 2, 'Form_id': 1, 'sessionStartTime': datetime.today(),
                'sessionEndTime': datetime.today(), 'deliveryStartTime': datetime.today(),
                'deliveryEndTime': datetime.today(), 'clearingPrice': 10}

        response = client.put('/marketplace/1/marketsessions/1/', data, format='json')
        self.assertEqual(response.status_code, 200)

    def test_ModifyMarketSessionsBadRequest(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {}
        response = client.put('/marketplace/1/marketsessions/1/', data, format='json')
        self.assertEqual(response.status_code, 400)

    def test_ModifyMarketSessions404(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {'Marketplace_id': 1, 'SessionStatus_id': 2, 'Form_id': 1, 'sessionStartTime': datetime.today(),
                'sessionEndTime': datetime.today(), 'deliveryStartTime': datetime.today(),
                'deliveryEndTime': datetime.today(), 'clearingPrice': 10}
        response = client.put('/marketplace/10/marketsessions/1/', data, format='json')
        self.assertEqual(response.status_code, 404)

    def test_ModifyMarketSessionsUnauthorized(self):
        client = APIClient()
        data = {'Marketplace_id': 1, 'SessionStatus_id': 2, 'Form_id': 1, 'sessionStartTime': datetime.today(),
                'sessionEndTime': datetime.today(), 'deliveryStartTime': datetime.today(),
                'deliveryEndTime': datetime.today(), 'clearingPrice': 10}
        response = client.put('/marketplace/1/marketsessions/1/', data, format='json')
        self.assertEqual(response.status_code, 401)

    def test_ModifyMarketSessions403(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok1.token)
        data = {'Marketplace_id': 1, 'SessionStatus_id': 2, 'Form_id': 1, 'sessionStartTime': datetime.today(),
                'sessionEndTime': datetime.today(), 'deliveryStartTime': datetime.today(),
                'deliveryEndTime': datetime.today(), 'clearingPrice': 10}
        response = client.put('/marketplace/1/marketsessions/1/', data, format='json')
        self.assertEqual(response.status_code, 403)
        ### End testing /marketplace/<pk>/marketsessions/<ses_id>/ ###

    def test_NotBilledOffers(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/2/marketsessions/cleared/offers/notbilled/')
        self.assertEqual(response.status_code, 200)

    def test_NotBilledOffersWrongPath(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/2/marketsessions/cleared/offers/active/')
        self.assertEqual(response.status_code, 404)

    def test_NotBilledOffers404(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/20/marketsessions/cleared/offers/active/')
        self.assertEqual(response.status_code, 404)

    def test_NotBilledOffersUnauthorized(self):
        client = APIClient()
        response = client.get('/marketplace/2/marketsessions/cleared/offers/notbilled/')
        self.assertEqual(response.status_code, 401)

    def test_BilledActions(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.put('/marketplace/2/marketsessions/cleared/actions/billed/')
        self.assertEqual(response.status_code, 200)

    def test_BilledActions401(self):
        client = APIClient()
        response = client.put('/marketplace/2/marketsessions/cleared/actions/billed/')
        self.assertEqual(response.status_code, 401)

    def test_BilledActions403(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok1.token)
        response = client.put('/marketplace/2/marketsessions/cleared/actions/billed/')
        self.assertEqual(response.status_code, 403)

    def test_BilledActionsWrongPath(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok1.token)
        response = client.put('/marketplace/2/marketsessions/cleared/actions/clcl/')
        self.assertEqual(response.status_code, 404)

    def test_BilledActions404(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.put('/marketplace/20/marketsessions/cleared/actions/billed/')
        self.assertEqual(response.status_code, 404)

    ### End of testing /marketplace/2/marketsessions/cleared/offers/notbilled/ endpoint ###

    def test_ActionsOfMarketSessionOffers(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/2/marketsessions/2/offers/active/')
        self.assertEqual(response.status_code, 200)

    def test_ActionsOfMarketSessionBids(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/2/marketsessions/2/bids/active/')
        self.assertEqual(response.status_code, 200)

    def test_ActionsOfMarketSession404Path(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/2/marketsessions/2/bids/completed/')
        self.assertEqual(response.status_code, 404)

    def test_ActionsOfMarketSessions(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/2/marketsessions/2/actions/active/')
        self.assertEqual(response.status_code, 200)

    def test_ActionsOfMarketSessionsDelivered(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/2/marketsessions/2/actions/delivered/')
        self.assertEqual(response.status_code, 200)

    def test_ActionsOfMarketSessionsSorted(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/2/marketsessions/2/actions/sorted/')
        self.assertEqual(response.status_code, 200)

    def test_ActionsOfMarketSessionsUnauthorized(self):
        client = APIClient()
        response = client.get('/marketplace/2/marketsessions/2/actions/sorted/')
        self.assertEqual(response.status_code, 401)

    def test_ActionsOfMarketSessionsToChecked(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {'ActionStatus_id': 2}
        response = client.put('/marketplace/2/marketsessions/2/actions/checked/', data, format='json')
        self.assertEqual(response.status_code, 200)

    def test_ActionsOfMarketSessionsToChecked401(self):
        client = APIClient()
        data = {'ActionStatus_id': 2}
        response = client.put('/marketplace/2/marketsessions/2/actions/checked/', data, format='json')
        self.assertEqual(response.status_code, 401)

    def test_ActionsOfMarketSessionsToChecked403(self):
        client = APIClient()
        data = {'ActionStatus_id': 2}
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok1.token)
        response = client.put('/marketplace/2/marketsessions/2/actions/checked/', data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_ActionsOfMarketSessionsToChecked404(self):
        client = APIClient()
        data = {'ActionStatus_id': 2}
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.put('/marketplace/20/marketsessions/2/actions/checked/', data, format='json')
        self.assertEqual(response.status_code, 404)
        ### End testing /marketplace/20/marketsessions/2/<action-type>/<action-status>/ endpoint ###

    def test_getMarketParticipants(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketparticipants/1/')
        self.assertEqual(response.status_code, 200)

    def test_getMarketParticipants401(self):
        client = APIClient()
        response = client.get('/marketparticipants/1/')
        self.assertEqual(response.status_code, 401)

    def test_getMarketParticipants404(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketparticipants/10/')
        self.assertEqual(response.status_code, 404)

    def test_postMarketParticipant(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {"companyName": "CivilTech", "email": "user2@email2.com", "phone": "6986672571",
                "representativeName": "user civil", "VAT": "02345677899912"}
        response = client.post('/marketparticipants/1/', data, format='json')
        self.assertEqual(response.status_code, 201)

    def test_postMarketParticipant401(self):
        client = APIClient()
        data = {"companyName": "CivilTech", "email": "user2@email2.com", "phone": "6986672571",
                "representativeName": "user civil", "VAT": "02345677899912"}
        response = client.post('/marketparticipants/1/', data, format='json')
        self.assertEqual(response.status_code, 401)

    def test_postMarketParticipant403(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok1.token)
        data = {"companyName": "CivilTech", "email": "user2@email2.com", "phone": "6986672571",
                "representativeName": "user civil", "VAT": "02345677899912"}
        response = client.post('/marketparticipants/1/', data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_postMarketParticipantNullData(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {}
        response = client.post('/marketparticipants/1/', data, format='json')
        self.assertEqual(response.status_code, 400)

    def test_postMarketParticipantBadRequest(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {"email": "user2@email2.com", "phone": "6986672571",
                "representativeName": "user civil", "VAT": "02345677899912"}
        response = client.post('/marketparticipants/1/', data, format='json')
        self.assertEqual(response.status_code, 400)

    ### End testing /marketparticipants/<pk>/ endpoint ###

    def test_getMarketActionCounterOffers(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/2/counteroffers/', format='json')
        self.assertEqual(response.status_code, 200)

    def test_getMarketActionCounterOffers401(self):
        client = APIClient()
        response = client.get('/marketplace/2/counteroffers/', format='json')
        self.assertEqual(response.status_code, 401)

    def test_postMarketActionCounterOffers(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {"MarketAction_Bid_id": 1, "MarketAction_Offer_id": 2, "exchangedValue": 45.67}
        response = client.post('/marketplace/2/counteroffers/', data, format='json')
        self.assertEqual(response.status_code, 201)

    def test_postMarketActionCounterOffers401(self):
        client = APIClient()
        data = {"MarketAction_Bid_id": 1, "MarketAction_Offer_id": 2, "exchangedValue": 45.67}
        response = client.post('/marketplace/2/counteroffers/', data, format='json')
        self.assertEqual(response.status_code, 401)

    def test_postMarketActionCounterOffers403(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok1.token)
        data = {"MarketAction_Bid_id": 1, "MarketAction_Offer_id": 2, "exchangedValue": 45.67}
        response = client.post('/marketplace/2/counteroffers/', data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_postMarketActionCounterOffers400(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {"MarketAction_Bid_id": 1, "MarketAction_Offer_id": 2, "exchangedValue": 45.677777}
        response = client.post('/marketplace/2/counteroffers/', data, format='json')
        self.assertEqual(response.status_code, 400)

    def test_postMarketActionCounterOffers400_1(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {}
        response = client.post('/marketplace/2/counteroffers/', data, format='json')
        self.assertEqual(response.status_code, 400)

        ### ###

    def test_getTransactions(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/2/marketsessions/2/transactions/', format='json')
        self.assertEqual(response.status_code, 200)

    def test_getTransactions401(self):
        client = APIClient()
        response = client.get('/marketplace/2/marketsessions/2/transactions/', format='json')
        self.assertEqual(response.status_code, 401)

    def test_getTransactions404(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/20/marketsessions/2/transactions/', format='json')
        self.assertEqual(response.status_code, 404)
        ### End testing /marketplace/<pk>/marketsessions/<sid>/transactions/ endpoint ###

    def test_postInvoices(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {"date": "2012-03-31T11:06:05.182371Z", "Transaction_id": 1, "penalty": 34.789, "fixedFee": 5.567,
                "amount": 23.678}
        response = client.post('/marketplace/2/marketsessions/2/invoices/', data, format='json')
        self.assertEqual(response.status_code, 201)

    def test_postInvoices401(self):
        client = APIClient()
        data = {"date": "2012-03-31T11:06:05.182371Z", "Transaction_id": 1, "penalty": 34.789, "fixedFee": 5.567,
                "amount": 23.678}
        response = client.post('/marketplace/2/marketsessions/2/invoices/', data, format='json')
        self.assertEqual(response.status_code, 401)

    def test_postInvoices403(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok1.token)
        data = {"date": "2012-03-31T11:06:05.182371Z", "Transaction_id": 1, "penalty": 34.789, "fixedFee": 5.567,
                "amount": 23.678}
        response = client.post('/marketplace/2/marketsessions/2/invoices/', data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_postInvoicesNullData(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {}
        response = client.post('/marketplace/2/marketsessions/2/invoices/', data, format='json')
        self.assertEqual(response.status_code, 400)

    def test_postInvoicesBadRequest(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {"Transaction_id": 1, "penalty": 34.789, "fixedFee": 5.567,
                "amount": 23.678}
        response = client.post('/marketplace/2/marketsessions/2/invoices/', data, format='json')
        self.assertEqual(response.status_code, 400)
        ### End testing /invoices/ endpoint ###

    def test_postClearedActions(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {
            "date": "2017-11-09T00:00:00Z",
            "actionStartTime": "2017-11-09T00:00:00Z",
            "actionEndTime": "2017-11-09T00:00:00Z",
            "value": "19.010",
            "uom": "VAT",
            "price": "18.992",
            "deliveryPoint": "my company",
            "MarketActor_id": 1,
            "Form_id": 1,
            "ActionType_id": 1,
            "ActionStatus_id": 7
        }
        response = client.post('/marketplace/2/marketsessions/2/clearing/actions/', data, format='json')
        self.assertEqual(response.status_code, 201)

    def test_postClearedActions401(self):
        client = APIClient()
        data = {
            "date": "2017-11-09T00:00:00Z",
            "actionStartTime": "2017-11-09T00:00:00Z",
            "actionEndTime": "2017-11-09T00:00:00Z",
            "value": "19.010",
            "uom": "VAT",
            "price": "18.992",
            "deliveryPoint": "my company",
            "MarketActor_id": 1,
            "Form_id": 1,
            "ActionType_id": 1,
            "ActionStatus_id": 7
        }
        response = client.post('/marketplace/2/marketsessions/2/clearing/actions/', data, format='json')
        self.assertEqual(response.status_code, 401)

    def test_postClearedActions403(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok1.token)
        data = {
            "date": "2017-11-09T00:00:00Z",
            "actionStartTime": "2017-11-09T00:00:00Z",
            "actionEndTime": "2017-11-09T00:00:00Z",
            "value": "19.010",
            "uom": "VAT",
            "price": "18.992",
            "deliveryPoint": "my company",
            "MarketActor_id": 1,
            "Form_id": 1,
            "ActionType_id": 1,
            "ActionStatus_id": 7
        }
        response = client.post('/marketplace/2/marketsessions/2/clearing/actions/', data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_postClearedActions400(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {
            "date": "2017-11-09T00:00:00Z",
            "actionStartTime": "2017-11-09T00:00:00Z",
            "actionEndTime": "2017-11-09T00:00:00Z",
            "value": "19.010",
            "uom": "VAT",
            "price": "18.992",
            "deliveryPoint": "my company",
            "MarketActor_id": 1,
            "Form_id": 1,
            "ActionType_id": 1,
            "ActionStatus_id": 7
        }
        response = client.post('/marketplace/2/marketsessions/20/clearing/actions/', data, format='json')
        self.assertEqual(response.status_code, 400)

    def test_actionStatus(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/actionstatus/', format='json')
        self.assertEqual(response.status_code, 200)

    def test_actionStatus401(self):
        client = APIClient()
        response = client.get('/actionstatus/', format='json')
        self.assertEqual(response.status_code, 401)

    def test_editSingleAction(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {
            "date": "2017-11-09T00:00:00Z",
            "actionStartTime": "2017-11-09T00:00:00Z",
            "actionEndTime": "2017-11-09T00:00:00Z",
            "value": "19.010",
            "uom": "VAT",
            "price": "18.992",
            "deliveryPoint": "my company",
            "MarketActor_id": 1,
            "Form_id": 1,
            "ActionType_id": 1,
            "ActionStatus_id": 7,
            "MarketSession_id": 2
        }
        response = client.put('/marketplace/2/marketsessions/2/actions/2/edit/', data, format='json')
        self.assertEqual(response.status_code, 200)

    def test_editSingleAction401(self):
        client = APIClient()
        data = {
            "date": "2017-11-09T00:00:00Z",
            "actionStartTime": "2017-11-09T00:00:00Z",
            "actionEndTime": "2017-11-09T00:00:00Z",
            "value": "19.010",
            "uom": "VAT",
            "price": "18.992",
            "deliveryPoint": "my company",
            "MarketActor_id": 1,
            "Form_id": 1,
            "ActionType_id": 1,
            "ActionStatus_id": 7,
            "MarketSession_id": 2
        }
        response = client.put('/marketplace/2/marketsessions/2/actions/2/edit/', data, format='json')
        self.assertEqual(response.status_code, 401)

    def test_editSingleAction403(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok1.token)
        data = {
            "date": "2017-11-09T00:00:00Z",
            "actionStartTime": "2017-11-09T00:00:00Z",
            "actionEndTime": "2017-11-09T00:00:00Z",
            "value": "19.010",
            "uom": "VAT",
            "price": "18.992",
            "deliveryPoint": "my company",
            "MarketActor_id": 1,
            "Form_id": 1,
            "ActionType_id": 1,
            "ActionStatus_id": 7,
            "MarketSession_id": 2
        }
        response = client.put('/marketplace/2/marketsessions/2/actions/2/edit/', data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_editSingleActionNullData(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {}
        response = client.put('/marketplace/2/marketsessions/2/actions/2/edit/', data, format='json')
        self.assertEqual(response.status_code, 400)

    def test_editSingleAction400(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {
            "date": "2017-12-09T00:00:00Z",
        }
        response = client.put('/marketplace/2/marketsessions/2/actions/2/edit/', data, format='json')
        self.assertEqual(response.status_code, 400)

    def test_getReferencePrice(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/2/form/1/date/2017-11-12/referencePrice/', format='json')
        self.assertEqual(response.status_code, 200)

    def test_getReferencePrice401(self):
        client = APIClient()
        response = client.get('/marketplace/2/form/1/date/2017-11-12/referencePrice/', format='json')
        self.assertEqual(response.status_code, 401)

    def test_getReferencePrice404(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/marketplace/2/form/10/date/2017-11-12/referencePrice/', format='json')
        self.assertEqual(response.status_code, 404)

    def test_postTransactions(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {"dateTime": "2017-12-09T00:00:00Z", "MarketActionCounterOffer_id": 1}
        response = client.post('/marketplace/2/marketsessions/2/transactions/', data, format='json')
        self.assertEqual(response.status_code, 201)

    def test_postTransaction401(self):
        client = APIClient()
        data = {"dateTime": "2017-12-09T00:00:00Z", "MarketActionCounterOffer_id": 1}
        response = client.post('/marketplace/2/marketsessions/2/transactions/', data, format='json')
        self.assertEqual(response.status_code, 401)

    def test_postTransaction403(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok1.token)
        data = {"dateTime": "2017-12-09T00:00:00Z", "MarketActionCounterOffer_id": 1}
        response = client.post('/marketplace/2/marketsessions/2/transactions/', data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_postTransaction400(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        data = {}
        response = client.post('/marketplace/2/marketsessions/2/transactions/', data, format='json')
        self.assertEqual(response.status_code, 400)

    def test_getRules(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.tok.token)
        response = client.get('/rules/', format='json')
        self.assertEqual(response.status_code, 200)

    def test_getRules401(self):
        client = APIClient()
        response = client.get('/rules/', format='json')
        self.assertEqual(response.status_code, 401)
