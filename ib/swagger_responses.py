from drf_yasg import openapi
from rest_framework import status
from ib.serializer import *

# ==================================
#   Information Broker Responses
# ==================================
MarketPlaceList_GET = {
    status.HTTP_200_OK: MarketPlaceSerializer(many=True),
    status.HTTP_404_NOT_FOUND: 'Information not found',
}
MarketPlaceList_POST = {
    status.HTTP_201_CREATED: 'Marketplace created',
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
}
MarketPlaceDetail_GET = {
    status.HTTP_200_OK: MarketPlaceSerializer,
    status.HTTP_404_NOT_FOUND: 'Requested marketplace not found'
}

MarketPlaceDetail_DELETE = {
    status.HTTP_204_NO_CONTENT: "Marketplace deleted",
    status.HTTP_404_NOT_FOUND: 'Requested marketplace not found'
}

MarketPlaceDetail_PUT = {
    status.HTTP_200_OK: "Marketplace Updated",
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
    status.HTTP_404_NOT_FOUND: 'Requested marketplace not found'
}
MarketPlaceOfForm_GET = {
    status.HTTP_200_OK: MarketPlaceSerializer(many=True),
    status.HTTP_404_NOT_FOUND: 'Marketplaces of given form with active sessions not found',
}
MarketSessions_GET = {
    status.HTTP_200_OK: MarketSessionSerializer(many=True),
    status.HTTP_404_NOT_FOUND: 'Requested market session not found'
}
MarketSessions_POST = {
    status.HTTP_404_NOT_FOUND: 'Requested marketplace not found',
    status.HTTP_201_CREATED: 'Marketsession created',
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed'
}
allUpdateMarketSessions_PUT = {
    status.HTTP_404_NOT_FOUND: 'Requested market session not found',
    status.HTTP_200_OK: "Market session Updated",
}
putClearedSessions = {
    status.HTTP_200_OK: "Market session updated",
    status.HTTP_404_NOT_FOUND: 'Requested market session not found',
}
putCompletedSessions_PUT = {
    status.HTTP_200_OK: "Market session Updated",
    status.HTTP_404_NOT_FOUND: 'Requested marketplace, form or marketsession not found',
}

postListActionToClearing_POST = {
    #status.HTTP_200_OK: MarketActionClearingSerializer(many=True),
    status.HTTP_200_OK: "Provided MarketActions were succesfully sent to the ITLB for the clearing request",
    status.HTTP_400_BAD_REQUEST: "Provided data are invalid or malformed",
    status.HTTP_405_METHOD_NOT_ALLOWED: "Method not allowed on this URL",
}

getOffers_GET = {
    status.HTTP_404_NOT_FOUND: 'Requested marketsession not found',
    status.HTTP_200_OK: MarketActionSerializer(many=True)
}


Load_GET = {
    status.HTTP_404_NOT_FOUND: 'Requested load not found',
    status.HTTP_200_OK: LoadSerializer(many=True)
}

Load_POST = {
    status.HTTP_404_NOT_FOUND: 'Requested load not found',
    status.HTTP_201_CREATED: 'Load created',
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed'
}


LoadValue_GET = {
    status.HTTP_404_NOT_FOUND: 'Requested load value not found',
    status.HTTP_200_OK: LoadValueSerializer(many=True)
}

LoadValue_POST = {
    status.HTTP_404_NOT_FOUND: 'Requested load value  not found',
    status.HTTP_201_CREATED: 'Load value created',
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed'
}


MarketActions_GET = {
    status.HTTP_404_NOT_FOUND: 'Requested marketsession not found',
    status.HTTP_200_OK: MarketActionSerializer(many=True)
}

MarketAction_GET = {
    status.HTTP_404_NOT_FOUND: 'Requested marketaction not found',
    status.HTTP_200_OK: MarketActionSerializer
}

MarketActions_POST = {
    status.HTTP_404_NOT_FOUND: 'Requested marketsession not found',
    status.HTTP_201_CREATED: 'MarketAction created',
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed'
}
getTransactions_GET = {
    status.HTTP_404_NOT_FOUND: 'Requested marketsession not found',
    status.HTTP_200_OK: TransactionSerializer(many=True)
}
getTransactions_POST = {
    status.HTTP_404_NOT_FOUND: 'Requested marketsession not found',
    status.HTTP_201_CREATED: 'Transaction created',

}
MarketOperator_GET = {
    status.HTTP_200_OK: MarketActorSerializer1,
    status.HTTP_404_NOT_FOUND: 'Requested Market Operator not found',

}
MarketOperator_POST = {
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
    status.HTTP_201_CREATED: 'Operator created',
    status.HTTP_404_NOT_FOUND: 'Marketplace not found'
}
PARTICIPANT_GET = {
    status.HTTP_200_OK: MarketActorSerializer1,
    status.HTTP_404_NOT_FOUND: 'Requested Market Participant not found',
}
PARTICIPANTOFMARKET_GET = {
    status.HTTP_200_OK: MarketActorSerializer1,
    status.HTTP_404_NOT_FOUND: 'Requested Marketplace not found',
}
PARTICIPANT_POST = {
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
    status.HTTP_201_CREATED: 'Market Participant created',
}
PARTICIPANTOFMARKET_POST = {
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
    status.HTTP_201_CREATED: 'Market Participant created for the given marketplace',
    status.HTTP_404_NOT_FOUND: 'Marketplace not found'
}
PARTICIPANT_PUT = {
    status.HTTP_201_CREATED: 'Market Participant profile updated',
    status.HTTP_404_NOT_FOUND: 'Market Participant not found',
    status.HTTP_403_FORBIDDEN: 'Only own profile is editable'
}
GET_pendingRequests = {
    status.HTTP_200_OK: UserActorRelSerializer(many=True)
}

BIDS_PARAMETER = openapi.Parameter(
    name='type', in_=openapi.IN_PATH,
    type=openapi.TYPE_STRING,
    description="bids/offers/actions",
    required=True
)
RULE_GET = {
    status.HTTP_200_OK: RuleSerializer,
    status.HTTP_404_NOT_FOUND: 'Rule not found',
}
RULE_DELETE = {
    status.HTTP_204_NO_CONTENT: 'Rule deleted',
    status.HTTP_404_NOT_FOUND: 'Rule not found'
}
getRules_GET = {
    status.HTTP_200_OK: RuleSerializer(many=True),
}

getRules_POST = {
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
    status.HTTP_200_OK: 'Rule created',
}

Form_GET = {
    status.HTTP_200_OK: FormSerializer(many=True)
}
getFormById_GET = {
    status.HTTP_200_OK: FormSerializer,
    status.HTTP_404_NOT_FOUND: "Form not found"
}
TimeframeList_GET = {
    status.HTTP_200_OK: TimeFrameSerializer(many=True)
}
getActionById_GET = {
    status.HTTP_200_OK: MarketActionSerializer(many=True),
    status.HTTP_404_NOT_FOUND: "Action not found"
}
getActionById_DELETE = {
    status.HTTP_204_NO_CONTENT: "Action deleted",
    status.HTTP_404_NOT_FOUND: "Requested action(s) not found",
}
postInvoices_POST = {
    status.HTTP_201_CREATED: "Invoice created",
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
}
Invoices_GET = {
    status.HTTP_200_OK: InvoiceSerializer(many=True),
    status.HTTP_404_NOT_FOUND: 'Invoice not found',
}
User_Invoices_GET = {
    status.HTTP_200_OK: InvoiceSerializer(many=True),
    status.HTTP_404_NOT_FOUND: 'Invoices for requested user not found',
}

User_Markets_GET = {
    status.HTTP_200_OK: UserMarketSerializer(many=True),
    status.HTTP_404_NOT_FOUND: 'Markets for requested user',
}

Constraints_GET = {
    status.HTTP_200_OK: ConstraintsSerializer(many=True)
}

postCorrelatedMarketActions = {

    status.HTTP_201_CREATED: CorrelatedMarketActionsSerializer(many=True),
    status.HTTP_400_BAD_REQUEST: 'Invalid market actions',
}

GetDSO_GET = {
    status.HTTP_200_OK: MarketActorSerializer1(many=True)
}

electricityPrice_POST = {
    status.HTTP_200_OK: "Electricity Price is set",
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
}
electricityPrice_GET = {
    status.HTTP_200_OK: ElectricityPriceSerializer(many=True),
    status.HTTP_404_NOT_FOUND: "Electricity prices not found"
}
getActionStatus_GET = {
    status.HTTP_200_OK: ActionStatusSerializer(many=True),
    status.HTTP_404_NOT_FOUND: "Action status not found",
}
getActionStatusId_GET = {
    status.HTTP_200_OK: ActionStatusSerializer,
    status.HTTP_404_NOT_FOUND: "Action status not found",
}
getSessionStatus_GET = {
    status.HTTP_200_OK: SessionStatusSerializer(many=True),
    status.HTTP_404_NOT_FOUND: "Session status not found",
}

actionType_GET = {
    status.HTTP_200_OK: ActionTypeSerializer(many=True),
    status.HTTP_404_NOT_FOUND: "Actions types not found",
}
SelectedActionsOfGamSession_PUT = {
    status.HTTP_404_NOT_FOUND: 'Action not found',
    status.HTTP_200_OK: "Offer status updated to selected"
}
MarketSession_GET = {
    status.HTTP_200_OK: MarketSessionSerializer,
    status.HTTP_404_NOT_FOUND: 'Session not found',
}
ModifyMarketSession_GET = {
    status.HTTP_200_OK: MarketSessionSerializer,
    status.HTTP_404_NOT_FOUND: 'Session or marketplace not found',
}
ModifyMarketSession_PUT = {
    status.HTTP_404_NOT_FOUND: 'Session not found',
    status.HTTP_204_NO_CONTENT: "Session modified",
}
NOTBILLEDOFFERS_GET = {
    status.HTTP_200_OK: MarketActionSerializer(many=True),
    status.HTTP_404_NOT_FOUND: 'Marketplace not found',
}
NOTBILLEDOFFERSFLEX_GET  = {
    status.HTTP_200_OK: MarketActionSerializer(many=True),
    status.HTTP_404_NOT_FOUND: 'Offers not found',
}
MarketActionCounterOffers_GET = {
    status.HTTP_200_OK: MarketActionCounterOfferSerializer(many=True),
    status.HTTP_404_NOT_FOUND: 'Requested marketplace does not exist',
}
MarketActionCounterOffer_GET = {
    status.HTTP_200_OK: MarketActionCounterOfferSerializer,
    status.HTTP_404_NOT_FOUND: 'Requested counter action does not exist',
}
MarketActionCounterOffers_POST = {
    status.HTTP_201_CREATED: "counteroffer created",
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
    status.HTTP_404_NOT_FOUND: 'Market action not found',
}

deleteMarketActionCounterOffers_DELETE = {
    status.HTTP_200_OK: "counteroffer deleted",
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
    status.HTTP_404_NOT_FOUND: 'Requested Marketplace not found',
}


editSingleAction_PUT = {
    status.HTTP_200_OK: "Action modified",
    status.HTTP_404_NOT_FOUND: "Requested action not found",
    status.HTTP_400_BAD_REQUEST: "Provided data is invalid or malformed"
}
getReferencePrice_GET = {
    status.HTTP_404_NOT_FOUND: "Requested reference price not found",
    status.HTTP_200_OK: ReferencePriceSerializer(many=True),
    status.HTTP_400_BAD_REQUEST: "Mismatch in input parameters"
}
clearingPrice_GET = {
    status.HTTP_404_NOT_FOUND: "Requested clearing price not found",
    status.HTTP_200_OK: "True",
    status.HTTP_400_BAD_REQUEST: "Mismatch in input parameters"
}
actionByUser_RESPONSE = {
    status.HTTP_200_OK: MarketActionSerializer(many=True),
    status.HTTP_404_NOT_FOUND: "The requested information not found"
}
postReferencePrice = {
    status.HTTP_201_CREATED: "Reference Price is set",
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
}


returnSpecificActor_GET = {
    status.HTTP_404_NOT_FOUND: "The requested Market Actor not found",
    status.HTTP_200_OK: MarketActorSerializer1
}
AccessManagerDec_GET = {
    status.HTTP_404_NOT_FOUND: "The requested information not found",
    status.HTTP_200_OK: UserActorRelSerializer(many=True)
}
ActionsOfMarketSession_GET = {
    status.HTTP_200_OK: MarketActionSerializer(many=True),
    status.HTTP_404_NOT_FOUND: "The requested information not found"
}
PARTICIPANT_STATUS_PUT = {
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
    status.HTTP_404_NOT_FOUND: 'Requested Marketplace Participant not found',
    status.HTTP_204_NO_CONTENT: 'Marketplace Participant status Updated',
}
ActionsOfMarketSession_ACTION_TYPE = openapi.Parameter(
    name='action_type', in_=openapi.IN_PATH,
    type=openapi.TYPE_STRING,
    description="offers/bids/actions",
    required=True
)
ActionsOfMarketSession_ACTION_STATUS = openapi.Parameter(
    name='action_status', in_=openapi.IN_PATH,
    type=openapi.TYPE_STRING,
    description="active/sorted/delivered",
    required=True
)
ActionsOfMarketSession_PUT = {
    status.HTTP_404_NOT_FOUND: "The requested information not found",
    status.HTTP_200_OK: "The requested action updated"
}
ActionsOfMarketSession_POST = {
    status.HTTP_404_NOT_FOUND: "The requested information not found",
    status.HTTP_200_OK: "The requested action(s) were created",
    status.HTTP_400_BAD_REQUEST: "Provided data are invalid or malformed",
    status.HTTP_405_METHOD_NOT_ALLOWED: "Method not allowed on this URL",
}
ISPARTICIPANTOFMARKET = {
    status.HTTP_404_NOT_FOUND: "The requested information not found",
    status.HTTP_200_OK: "Actor belongs to given marketplace",
}

ActionsOfMarketSession_PUT_TYPE = openapi.Parameter(
    name='action_type', in_=openapi.IN_PATH,
    type=openapi.TYPE_STRING,
    description="Set param to string actions",
    required=True
)

ActionsOfMarketSession_PUT_STATUS = openapi.Parameter(
    name='action_status', in_=openapi.IN_PATH,
    type=openapi.TYPE_STRING,
    description="Set param to string checked",
    required=True
)
ActionsOfMarketSession_POST_TYPE = openapi.Parameter(
    name='action_type', in_=openapi.IN_PATH,
    type=openapi.TYPE_STRING,
    description="Set param to string clearing",
    required=True
)
ActionsOfMarketSession_POST_STATUS = openapi.Parameter(
    name='action_status', in_=openapi.IN_PATH,
    type=openapi.TYPE_STRING,
    description="Set param to string actions",
    required=True
)
MARKETPLACE_ID = openapi.Parameter(
    name='id', in_=openapi.IN_PATH,
    type=openapi.TYPE_STRING,
    description="marketplace ID",
    required=True
)

ACTION_STATUS = openapi.Parameter(
    name='action_status', in_=openapi.IN_PATH,
    type=openapi.TYPE_STRING,
    description="Action status",
    required=True
)


MARKETACTOR_ID = openapi.Parameter(
    name='id', in_=openapi.IN_PATH,
    type=openapi.TYPE_STRING,
    description="market actor ID",
    required=True
)
LOAD_ID = openapi.Parameter(
    name='lid', in_=openapi.IN_PATH,
    type=openapi.TYPE_STRING,
    description="load ID",
    required=True
)
SESSION_ID = openapi.Parameter(
    name='sid', in_=openapi.IN_PATH,
    type=openapi.TYPE_STRING,
    description="marketsession ID",
    required=True
)
USERNAME = openapi.Parameter(
    name='username', in_=openapi.IN_PATH,
    type=openapi.TYPE_STRING,
    description="Username of actor for whom data are requested",
    required=True
)

PATHINVOICES = openapi.Parameter(
    name='pathInvoices', in_=openapi.IN_PATH,
    type=openapi.TYPE_STRING,
    description="Path for invoices for whom data are requested",
    required=True
)

PATHHISTORY = openapi.Parameter(
    name='pathHistory', in_=openapi.IN_PATH,
    type=openapi.TYPE_STRING,
    description="Path for history for whom data are requested (energy-history/heat-history/ancillary-history/it_load-history)",
    required=True
)

IS_DSO = openapi.Parameter(
    name='is_dso', in_=openapi.IN_PATH,
    type=openapi.TYPE_INTEGER ,
    description="Equals to 1 if user is DSO or 0 otherwise",
    required=True
)
ACTOR_ID = openapi.Parameter(
    name='actor', in_=openapi.IN_PATH,
    type=openapi.TYPE_INTEGER,
    description="Actor ID",
    required=True
)
FORM = openapi.Parameter(
    name='form', in_=openapi.IN_PATH,
    type=openapi.TYPE_STRING,
    description="Form (electric_energy/thermal_energy/congestion_management/reactive_power_compensation/spinning_reserve/scheduling/it_load)",
    required=True
)
ACTOROFMARKETPLACE_RESPONSES = {
    status.HTTP_201_CREATED: "Request addressed",
    status.HTTP_404_NOT_FOUND: "The requested information not found",
}
ACTOROFMARKETPLACE_DELETE = {
    status.HTTP_404_NOT_FOUND: "Requested information not found",
    status.HTTP_204_NO_CONTENT: "Actor removed from marketplace",
}
MarketActorMplaces_GET = {
    status.HTTP_200_OK: MarketPlaceSerializer(many=True),
    status.HTTP_404_NOT_FOUND: "Actor not found"
}
TOKEN_POST = {
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
    status.HTTP_201_CREATED: 'Token Created',
}
MIGRATIONDETAILS_GET = {
    status.HTTP_200_OK: MigrationDetailsSerializer,
    status.HTTP_404_NOT_FOUND: 'Information not found',
}

SESSION_STATUS = openapi.Parameter(
    name='status', in_=openapi.IN_PATH,
    type=openapi.TYPE_STRING,
    description="Session status (active/inactive/closed/cleared/completed)",
    required=True
)
ACTION_ID = openapi.Parameter(
    name='action_id', in_=openapi.IN_PATH,
    type=openapi.TYPE_INTEGER,
    description="Action ID",
    required=True
)
RULE_ID = openapi.Parameter(
    name='status', in_=openapi.IN_PATH,
    type=openapi.TYPE_INTEGER,
    description="Rule ID",
    required=True
)
SERVICE_TYPE = openapi.Parameter(
    name='servicetype', in_=openapi.IN_PATH,
    type=openapi.TYPE_STRING,
    description="Market service type - energy/ancillary-services/thermal_energy/it_load",
    required=True
)
TIMEFRAME = openapi.Parameter(
    name='timeframe', in_=openapi.IN_PATH,
    type=openapi.TYPE_STRING,
    description="Timeframe - intra_day/day_ahead/real_time",
    required=True
)
ACTION_STATUS_ID = openapi.Parameter(
    name='action_status_id', in_=openapi.IN_PATH,
    type=openapi.TYPE_INTEGER,
    description="Action status ID",
    required=True
)
FORM_ID = openapi.Parameter(
    name='form_id', in_=openapi.IN_PATH,
    type=openapi.TYPE_INTEGER,
    description="Form ID",
    required=True
)
COUNTER_ID = openapi.Parameter(
    name='counter_id', in_=openapi.IN_PATH,
    type=openapi.TYPE_INTEGER,
    description="Counter action ID",
    required=True
)