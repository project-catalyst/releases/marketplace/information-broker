# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2019-10-23 12:44
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ib', '0014_auto_20190424_1103'),
    ]

    operations = [
        migrations.AlterField(
            model_name='marketsession',
            name='clearingPrice',
            field=models.IntegerField(blank=True, help_text='equilibribrium price resulting from the market clearing process', null=True),
        ),
    ]
