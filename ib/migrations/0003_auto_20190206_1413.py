# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2019-02-06 12:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ib', '0002_auto_20190206_1339'),
    ]

    operations = [
        migrations.AlterField(
            model_name='actionstatus',
            name='status',
            field=models.CharField(choices=[('unchecked', 'Unchecked'), ('valid', 'Valid'), ('invalid', 'Invalid'), ('active', 'Active'), ('inactive', 'Inactive'), ('withdrawn', 'Withdrawn'), ('accepted', 'Accepted'), ('partially_accepted', 'Partially_accepted'), ('rejected', 'Rejected'), ('selected', 'Selected'), ('delivered', 'Delivered'), ('not_delivered', 'Not_delivered'), ('billed', 'Billed')], help_text='status of an action (unchecked, valid, eligible, etc.)', max_length=45),
        ),
        migrations.AlterField(
            model_name='sessionstatus',
            name='status',
            field=models.CharField(choices=[('active', 'Active'), ('inactive', 'Inactive'), ('closed', 'Closed'), ('cleared', 'Cleared'), ('completed', 'Completed')], max_length=45),
        ),
    ]
