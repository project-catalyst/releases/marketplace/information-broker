import logging

from rest_framework import permissions

from ib.models import *
from django.contrib.auth.models import User
from auth.clients.keycloak.api import Client
from django.http.response import JsonResponse
from rest_framework.exceptions import PermissionDenied, AuthenticationFailed, NotAuthenticated
from rest_framework.response import Response

log = logging.getLogger(__name__)




def get_user_from_request(request):

    keycloak = Client().get_client()

    if 'HTTP_AUTHORIZATION' not in request.META:
        return JsonResponse({"detail": NotAuthenticated.default_detail},
                            status=NotAuthenticated.status_code)

    auth_header = request.META.get('HTTP_AUTHORIZATION').split()
    token = auth_header[1] if len(auth_header) == 2 else auth_header[0]

    info = keycloak.userinfo(token)
    username = info['preferred_username']

    user = User.objects.get(username=username)

    if user is None:
        return Response(status=404,
                        data={"msg": "No user found in Marketplace for the presented token and username :{}".format(username)})

    return user

def get_actor_from_request(request):
    """
    Gets the actor from a request object
    :param request: The request to action_validity_check the user from
    :return: The corresponding actor
    """
    try:
        user = get_user_from_request(request)
        return UserActorRel.objects.get(user=user).actor
    except Exception as e:
        log.warning("Could not find actor for user {}: {}".format(request.user, e))
        return None

def get_actor_from_username(username):
    """
    Gets the actor from given the username of the corresponding user
    :param username: The username of the associated user
    :return: The corresponding actor
    """
    try:
        return UserActorRel.objects.get(user__username=username).actor
    except Exception as e:
        log.warning("Could not find actor for username {}: {}".format(username, e))
        return None


def check_user_role(request, types):
    """
    Checks if a user from a request is of a particular type
    :param request:
    :param types:
    :return:
    """
    try:
        return get_actor_from_request(request).marketActorTypeid.type in types
    except Exception as e:
        log.warning("User {} is not of type {}. Exception: {}".format(request.user, type, e))
        return False


############################################################################################################
# Easy ones - dead simple                                                                                  #
############################################################################################################
class IsKnownMarketActorReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        allowed_methods = permissions.SAFE_METHODS
        if request.method not in allowed_methods:
            return False
        allowed_types = ["dso", "aggregator", "generic participant", "market operator"]
        return check_user_role(request, allowed_types)

class IsKnownMarketActorWrite(permissions.BasePermission):
    def has_permission(self, request, view):
        allowed_methods = {'POST', 'PUT'}
        if request.method not in allowed_methods:
            return False
        allowed_types = ["dso", "aggregator", "generic participant", "market operator"]
        return check_user_role(request, allowed_types)


class IsKnownMarketActorRead(permissions.BasePermission):
    def has_permission(self, request, view):
        allowed_methods = {'GET'}
        if request.method not in allowed_methods:
            return False
        allowed_types = ["dso", "aggregator", "generic participant", "market operator"]
        return check_user_role(request, allowed_types)


class IsDSO(permissions.BasePermission):
    """
    Only allow DSOs to enter the relevant urls
    """

    def has_permission(self, request, view):
        allowed_types = ["dso"]
        return check_user_role(request, allowed_types)

class IsAny(permissions.BasePermission):
    """
    Only allow DSOs to enter the relevant urls
    """

    def has_permission(self, request, view):
        return True




class IsMarketOperator(permissions.BasePermission):
    """
    Only allow Market operators
    """

    def has_permission(self, request, view):
        allowed_types = ["market operator", "market_operator", "Market Operator", "MarketOperator"]
        return check_user_role(request, allowed_types)


class IsMarketParticipant(permissions.BasePermission):
    """
    Only allow Market operators
    """

    def has_permission(self, request, view):
        allowed_types = ["generic participant", "generic_participant"]
        return check_user_role(request, allowed_types)


class IsAggregator(permissions.BasePermission):
    """
    Only allow Market operators
    """

    def has_permission(self, request, view):
        allowed_types = ["aggregator"]
        return check_user_role(request, allowed_types)


class IsMSM(permissions.BasePermission):
    """
    Only allow Market operators
    """

    def has_permission(self, request, view):
        allowed_types = ["msm", "MSM"]
        return check_user_role(request, allowed_types)

class IsMCM(permissions.BasePermission):
    """
    Only allow Market Clearing Managers
    """

    def has_permission(self, request, view):
        allowed_types = ["mcm", "MCM"]
        return check_user_role(request, allowed_types)

class IsMBM(permissions.BasePermission):
    """
    Only allow Market Billing Managers
    """

    def has_permission(self, request, view):
        allowed_types = ["mbm", "MBM"]
        return check_user_role(request, allowed_types)

class IsMEMO(permissions.BasePermission):
    """
    Only allow Multiple-Energy Marketplace Operators
    """

    def has_permission(self, request, view):
        allowed_types = ["memo", "MEMO"]
        return check_user_role(request, allowed_types)

#################################################################################################
# Complex ones                                                                                  #
#################################################################################################



class IsMarketplaceParticipantReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method not in permissions.SAFE_METHODS:
            return False

        marketplace = None
        actor = get_actor_from_request(request)
        if actor is None:
            return False

        if 'pk' not in view.kwargs:
            return False
        try:
            marketplace = Marketplace.objects.get(id=view.kwargs['pk'])
        except Exception as e:
            log.warning(e)
            return False

        if check_user_role(request, ["generic participant"]):
            if actor in [x.marketActor for x in Marketplace_has_MarketActor.objects.filter(marketplace=marketplace)]:
                return True
            else:
                return False
        else:
            return False


class IsMarketplaceParticipantReadWrite(permissions.BasePermission):
    def has_permission(self, request, view):
        allowed_methods = permissions.SAFE_METHODS + ('POST', 'PUT',)
        if request.method not in allowed_methods:
            return False

        marketplace = None
        actor = get_actor_from_request(request)
        if actor is None:
            return False

        if 'pk' not in view.kwargs:
            return False
        try:
            marketplace = Marketplace.objects.get(id=view.kwargs['pk'])
        except Exception as e:
            log.warning(e)
            return False

        if check_user_role(request, ["generic participant"]):
            if actor in [x.marketActor for x in Marketplace_has_MarketActor.objects.filter(marketplace=marketplace)]:
                return True
            else:
                return False
        else:
            return False


class IsMarketplaceDSOReadOnly(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.method not in permissions.SAFE_METHODS:
            return False

        marketplace = None
        actor = get_actor_from_request(request)
        if actor is None:
            return False

        if 'pk' not in view.kwargs:
            return False
        try:
            marketplace = Marketplace.objects.get(id=view.kwargs['pk'])
        except Exception as e:
            log.warning(e)
            return False

        if check_user_role(request, ["dso"]):
            if marketplace.dSOid == actor:
                return True
            else:
                return False
        else:
            return False


class IsMarketplaceDSOReadWrite(permissions.BasePermission):

    def has_permission(self, request, view):
        allowed_methods = permissions.SAFE_METHODS + ('POST', 'PUT',)
        if request.method not in allowed_methods:
            return False

        marketplace = None
        actor = get_actor_from_request(request)
        if actor is None:
            return False

        if 'pk' not in view.kwargs:
            return False
        try:
            marketplace = Marketplace.objects.get(id=view.kwargs['pk'])
        except Exception as e:
            log.warning(e)
            return False

        if check_user_role(request, ["dso"]):
            if marketplace.dSOid == actor:
                return True
            else:
                return False
        else:
            return False


class CanEditMarketAction(permissions.BasePermission):
    def has_permission(self, request, view):
        allowed_methods = permissions.SAFE_METHODS + ('PUT',)
        if request.method not in allowed_methods:
            return False

        marketplace = None
        action = None
        actor = get_actor_from_request(request)
        if actor is None:
            return False

        if 'action_id' not in view.kwargs:
            return False

        try:
            action = MarketAction.objects.get(id=int(view.kwargs['action_id']))
        except Exception as e:
            log.warning(e)
            return False

        if 'pk' in view.kwargs:
            try:
                if 'sid' not in view.kwargs:
                    return False
                marketplace = Marketplace.objects.get(id=view.kwargs['pk'])
                session = MarketSession.objects.get(id=view.kwargs['sid'])
                if session.marketplaceid != marketplace:
                    return False
                if action.marketSessionid != session:
                    return False
            except Exception as e:
                log.warning(e)
                return False

        if action.marketActorid == actor:
            return True
        else:
            return False


class CanViewInvoice(permissions.BasePermission):
    def has_permission(self, request, view):
        allowed_methods = permissions.SAFE_METHODS
        if request.method not in allowed_methods:
            return False

        invoice = None
        actor = get_actor_from_request(request)
        if actor is None:
            return False

        if 'pk' not in view.kwargs:
            return False
        try:
            invoice = Invoice.objects.get(id=view.kwargs['pk'])
        except Exception as e:
            log.warning('Could not find invoice {}: {}'.format(view.kwargs['pk'], e))
            return False

        if invoice.transactionid.marketActionCounterOfferid.marketAction_Bid_id.marketActorid == actor:
            return True
        elif invoice.transactionid.marketActionCounterOfferid.marketAction_Offer_id.marketActorid == actor:
            return True

        return False


class HasOwnUsername(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.username != view.kwargs['username']:
            return False
        return request.user.username == view.kwargs['username']


class CanViewActions(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method not in permissions.SAFE_METHODS:
            return False

        actor = get_actor_from_request(request)
        if actor is None:
            return False

        if 'pk' not in view.kwargs:
            return False
        if 'is_dso' not in view.kwargs:
            return False
        if 'sid' not in view.kwargs:
            return False
        try:
            marketplace = Marketplace.objects.get(id=view.kwargs['pk'])
            marketsession = MarketSession.objects.get(id=view.kwargs['sid'])
        except Exception as e:
            log.warning(e)
            return False

        try:
            if check_user_role(request, ["dso"]):
                if view.kwargs['is_dso'] == '1':
                    if marketplace.dSOid == actor:
                        return True
                    else:
                        return False
                else:
                    return False
            elif check_user_role(request, ["market operator"]):
                if view.kwargs['is_dso'] == '0':
                    return True
            else:
                if view.kwargs['is_dso'] != '0':
                    return False
                else:
                    if actor in [x.marketActorid for x in
                                 MarketAction.objects.filter(marketSessionid=marketsession)]:
                        return True
                    else:
                        return False
            return False
        except Exception as e:
            log.warning('Could not find invoice {}: {}'.format(view.kwargs['pk'], e))
        return False

