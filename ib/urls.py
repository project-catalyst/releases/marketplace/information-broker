import os 

from django.conf import settings
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

from ib import auth
from ib.views import api
from ib.views import test_api
from django.conf.urls import url


def get_swagger_url():
    url = None
    ext_url = os.getenv("EXTERNAL_URL", "")
    rel_url = os.getenv("RELATIVE_URL", "")
    if len(ext_url) == 0:
        return url
    url = f"{ext_url}/{rel_url}/swagger/".replace("//", "/")
    return f"http://{url}"

# ==================================
#   SWAGGER ADD-ONS
# ==================================
API_DESCRIPTION = """Api endpoints exposed by the Information Broker component.

The `swagger-ui` view can be found [here](/swagger).  
The `ReDoc` view can be found [here](/swagger.json).  
The swagger YAML document can be found [here](/swagger.yaml).  
"""

schema_view = get_schema_view(
    openapi.Info(
        title="Information Broker",
        default_version='v1',
        description=API_DESCRIPTION,
        contact=openapi.Contact(email="info@project-catalyst.eu"),
        license=openapi.License(name="GNU LGPLv3"),
    ),
    validators=[],
    public=True,
    permission_classes=(permissions.AllowAny,),
    url=get_swagger_url()
)
# ==================================

urlpatterns = [

    # Tokens
    url(r'^tokens/$', auth.TokenGeneration.as_view()),

    # DSO URLs
    url(r'^dso/$', api.GetDSO.as_view()),

    # Actions URLs
    url(r'^actionstatus/$', api.actionStatus.as_view()),
    url(r'^actionstatus/(?P<action_status_id>[0-9]+)/$', api.actionStatusById.as_view()),
    url(r'^actiontypes/$', api.actionType.as_view()),

    # Get Action by Id ---> used in the AM for UPDATE and WITHDRAW
    url(r'^action/(?P<action_id>[0-9]+)/$', api.getActionById.as_view()),

    # Session URLs
    url(r'^sessionstatus/$', api.sessionStatus.as_view()),

    # User Marketplaces
    url(r'^user_markets/(?P<username>[\w\-]+)/$', test_api.getUserMarketMEMO.as_view()),
    #memo constraints
    url(r'^constraints/$', test_api.getConstraintsMEMO.as_view()),

    #flexibility parameters
    url(r'^systemparameter/$', test_api.systemParameterConfig.as_view()),

    url(r'^marketplace/all/coupleble/actor/actions/coupled/(?P<timeframe>[\w\-]+)/(?P<username>[\w\-]+)/$', test_api.correlatedMarketActionsHistory.as_view()),
    url(r'^marketplace/all/coupleble/actor/actions/coupled/(?P<timeframe>[\w\-]+)/(?P<start>[\w\-]+)/(?P<end>[\w\-]+)/(?P<username>[\w\-]+)/$', test_api.correlatedMarketActions.as_view()),
    url(r'^marketplace/all/coupleble/actor/actions/coupled/$', test_api.correlatedMarketActions.as_view()),
    url(r'^userMarkets/IB/(?P<username>[\w\-]+)/$', test_api.getUserMarketMEMO.as_view()),
    url(r'^marketplaces/IB/$', test_api.getMarketsMEMO.as_view()),
    url(r'^marketparticipant/request/$', test_api.postMarketRegistrationRequestMEMO.as_view()),

    # History URLs
    url(r'^history/(?P<pathHistory>[\w\-]+)/(?P<username>[\w\-]+)/$', api.generalHistory.as_view()),

    # Form URLs
    url(r'^form/$', api.FormList.as_view()),
    url(r'^form/(?P<form_id>[0-9]+)/$', api.FormById.as_view()),
    url(r'^form/details/$', api.signupForm.as_view()),

    # Profile URLs
    url(r'^getProfile/(?P<username>[\w\-]+)/$', api.getProfileData.as_view()),

    # Invoices URLs
    url(r'^invoices/$', api.Invoices.as_view()),
    url(r'^invoices/(?P<pathInvoices>[\w\-]+)/(?P<actor>[0-9]+)/(?P<username>[\w\-]+)/$', api.displaySpecificInvoices.as_view()),
    url(r'^invoices/(?P<pathInvoices>[\w\-]+)/details/(?P<actor>[0-9]+)/(?P<username>[\w\-]+)/$', api.displaySpecificInvoicesDetails.as_view()),

    url(r'^marketoperators/(?P<pk>[0-9]+)/$', api.MarketOperator.as_view()),
    url(r'^marketparticipants/(?P<pk>[0-9]+)/$', api.GenericParticipantofMarketplace.as_view()),
    url(r'^marketactor/(?P<pk>[0-9]+)/$', api.getMarketActorById.as_view()),
    url(r'^actor-by-username/(?P<username>[\w\-]+)/$', api.getMarketActorByUsername.as_view()),
    url(r'^marketparticipants/$', api.GenericParticipant.as_view()),
    url(r'^marketplace/$', api.MarketPlaceList.as_view(), name='marketplace'),
    url(r'^marketplace/form/(?P<form>[\w\-]+)/timeframe/(?P<timeframe>[\w\-]+)/$', api.MarketPlaceOfFormWithActiveSession.as_view()),
    url(r'^marketplace/form/(?P<form>[\w\-]+)/timeframe/(?P<timeframe>[\w\-]+)/all/$', api.MarketPlaceOfForm.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/$', api.MarketPlaceDetail.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/counteroffers/$', api.MarketActionCounterOffers.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/marketsessions/(?P<sid>\d+)/counteroffers/$', api.MarketActionCounterOffersOfSession.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/form/(?P<form>.*)/date/now/referencePrice/$', api.getReferencePrice.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/form/(?P<form>.*)/reference-prices-previous-days/date/(?P<date>[0-9]+)/$', api.getReferencePricesPreviousDay.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/form/(?P<form>.*)/clearing-prices-previous-days/date/(?P<date>[0-9]+)/$', api.getClearingPricesPreviousDay.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/form/(?P<form>.*)/date/now/referencePriceValue/$', api.getReferencePriceValue.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/form/(?P<form>.*)/date/(?P<date>.*)/referencePrice/$', api.getReferencePrice.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/form/(?P<form>.*)/date/(?P<date>.*)/referencePriceValue/$', api.getReferencePriceValue.as_view()),
    url(r'^referencePrice/$', api.postReferencePrice.as_view()),
    url(r'^marketactor/(?P<actor>[0-9]+)/marketplaces/$', api.MarketActorMplaces.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/marketactor/(?P<actor>\d+)/exists/$', api.FindMarketActorInMarketplace.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/marketparticipants/(?P<username>[\w\-]+)/dso/(?P<is_dso>[0-9]+)/marketsessions/(?P<sid>[0-9]+)/actions/$', api.actionByUser.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/marketparticipants/(?P<username>[\w\-]+)/dso/(?P<is_dso>[0-9]+)/marketsessions/(?P<sid>[0-9]+)/actions_by_status/(?P<action_status>[\w\-]+)/$', api.actionByUserAndActionStatus.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/marketsessions/$', api.MarketSessions.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/marketsessions/(?P<sid>\d+)/$', api.ModifyMarketSession.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/marketsessions/(?P<sid>\d+)/(?P<action_type>[\w\-]+)/(?P<action_status>[\w\-]+)/$', api.ActionsOfMarketSession.as_view(), name='actionsOfSession'),
    url(r'^marketplace/(?P<pk>[0-9]+)/marketsessions/(?P<sid>\d+)/actions/$', api.MarketActions.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/marketsessions/(?P<sid>\d+)/actions-simple-format/$', api.getMarketActionsSimpleObjects.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/marketsessions/(?P<sid>\d+)/actions/(?P<action_id>\d+)/edit/$', api.editSingleAction.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/marketsessions/(?P<sid>\d+)/actions/(?P<action_id>\d+)/selected/$', api.SelectedActionsOfGamSession.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/marketsessions/(?P<sid>\d+)/bids/$', api.getBids.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/marketsessions/(?P<sid>\d+)/completion/$', api.putCompletedSessions.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/marketsessions/(?P<sid>\d+)/end/$', api.putEndedSessions.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/marketsessions/(?P<sid>\d+)/endclear/$', api.putClearedSessions.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/form/(?P<form>[\w\-]+)/marketsessions/(?P<sid>\d+)/actions/toClearing/$', api.postListActionsToClearing.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/form/(?P<form>[\w\-]+)/marketsessions/(?P<sid>\d+)/actions/prioritised/$', api.postListActionsToClearing.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/marketsessions/(?P<sid>\d+)/invoices/', api.Invoices.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/marketsessions/(?P<sid>\d+)/offers/$', api.getOffers.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/marketsessions/(?P<sid>\d+)/transactions/$', api.getTransactions.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/marketsessions/active/$', api.ActiveMarketSessions.as_view()),
    url(r'^marketplace/timeframe/(?P<timeframe>[\w\-]+)/actions/(?P<status>[\w\-]+)/$', api.ValidActionsOfAllActiveSessions.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/marketsessions/active/(?P<type>[\w\-]+)/active/$', api.AllActive.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/marketsessions/cleared/actions/billed/$', api.BilledActions.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/marketsessions/completed/offers/notbilled/$', api.NotBilledOffers.as_view()),
    url(r'^marketplace/gam/marketsessions/completed/offers/notbilled/$', api.NotBilledOffersOfFlex.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/marketsessions/form/(?P<form>[\w\-]+)/(?P<status>[\w\-]+)/$', api.selectMarketSessionByForm.as_view()),
    url(r'^marketplace/(?P<pk>[0-9]+)/marketsessions/form/(?P<form>[\w\-]+)/sessionstatus/closed/lastClearingPrice/$', api.findLastMarketSessionClearingPrice.as_view()),
    url(r'^marketplace/all/update/marketsessions/(?P<status>[\w\-]+)/$', api.allUpdateMarketSessions.as_view()),

    # Marketsession URLs
    url(r'^marketsessions/(?P<sid>\d+)/$', api.MarketSessionById.as_view()),

    # Marketaction URLs
    url(r'^marketactions/(?P<action_id>\d+)/$', api.MarketActionById.as_view()),
    url(r'^marketactions/(?P<action_id>\d+)/migration/details/$', api.MigrationDetails.as_view()),

    # Marketactioncounteroffer URLs
    url(r'^marketactioncounteroffers/(?P<counter_id>\d+)/$', api.MarketActionCounterOfferById.as_view()),
    url(r'^marketactioncounteroffers/it-clearing/$', api.MarketActionClearingCounterOffers.as_view(), name='clearingResult'),
    url(r'^v2/clearingRequest/$', test_api.itlbClearingRequest.as_view()),
    url(r'^marketactioncounteroffers/$', api.deleteMarketActionCounterOffers.as_view()),

    # Load Values URLs
    url(r'^loadvalues/load/(?P<lid>[0-9]+)/$', api.getLoadValuesByLoad.as_view()),
    url(r'^loadvalues/$', api.postLoadValues.as_view()),

    # Loads URLs
    url(r'^load/$', api.postLoad.as_view()),

    url(r'^pending/requests/$', api.pendingRequests.as_view()),
    url(r'^price/electricity/$', api.electricityPrice.as_view()),
    url(r'^relations/$', api.RelationDetails.as_view()),

    url(r'^rules/$', api.getRules.as_view()),
    url(r'^rules/(?P<pk>[0-9]+)/$', api.viewRule.as_view()),

    url(r'^select/marketplace/(?P<servicetype>[\w\-]+)/(?P<timeframe>[\w\-]+)/$', api.selectMarketplace.as_view()),
    url(r'^select/marketplace/(?P<servicetype>[\w\-]+)/(?P<timeframe>[\w\-]+)/(?P<username>[\w\-]+)/$', api.selectMarketplaceByUsername.as_view()),

    # Various
    url(r'^marketplaces/admin/participant/status/', api.StatusParticipant.as_view()),
    url(r'^marketparticipant/details/', api.requestAID.as_view()),
    url(r'^admin-detail/$', api.AdminDetail.as_view()),
    url(r'^allSession/$', api.SessionView.as_view()),
    url(r'^assign/actor/(?P<actor>.*)/marketplace/(?P<pk>.*)/$', api.AssignActorToMarketplace.as_view()),
    url(r'^decorators/(?P<username>[\w\-]+)/$', api.AccessManagerDec.as_view()),
    url(r'^signup/$', api.signupUser.as_view()),
    url(r'^create-keycloak-user/$', api.createKeycloakUser.as_view()),
    url(r'^create-actor-assign-client-roles/$', api.createUserActorAndAssignClientSpecificRoles.as_view()),
    url(r'^spec-actor/(?P<username>[\w\-]+)/$', api.returnSpecificActor.as_view()),
    url(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'^timeframe/$', api.TimeframeList.as_view()),
    url(r'^update/(?P<username>[\w\-]+)/$', api.UpdateUserProfile.as_view()),
    # # # url(r'^marketplace/(?P<pk>[0-9]+)/marketsessions/(?P<sid>\d+)/(?P<type>[\w\-]+)/$',api.HandleSessionPragmas.as_view()),

]
