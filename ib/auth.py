import logging
import json

from django.conf import settings
from django.contrib.auth.models import User
from drf_yasg.utils import swagger_auto_schema
from keycloak.exceptions import KeycloakAuthenticationError
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response

from auth.clients.keycloak.api import Client
from ib.swagger_responses import *
from ib.models import UserActorRel
from ib.serializer import UserActorRelSerializer, MarketActorSerializer, UserTokenSerializer, CredentialsSerializer, \
    RefreshTokenSerializer
from ib.permissions import get_actor_from_username

log = logging.getLogger(__name__)

client_specific_roles = {
    'dso': 'dso',
    'mbm': 'mbm',
    'mcm': 'mcm',
    'aggregator': 'aggregator'
}

realm_specific_roles = {
    'market-participant': 'generic participant',
    'memo': 'memo',
    'operator': 'market operator',
}


class AuthToken(ObtainAuthToken):

    @swagger_auto_schema(responses=TOKEN_POST,
                         operation_summary="Assign specific actor to specific marketplace",
                         request_body=UserTokenSerializer)
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        if token:
            try:
                django_user = User.objects.get(username=user)
                uar = UserActorRel.objects.get(user=django_user)
                serializer = MarketActorSerializer(uar.actor)

                return Response({
                    'token': token.key,
                    'actor': serializer.data,
                    'username': django_user.username
                })
            except Exception as e:
                log.warning("User {} is not a known CATALYST market actor. Details: {}".format(user, e))
            return Response(status=401, data={"msg": "User {} is not a known CATALYST market actor".format(user)})


# This is an example of how Keycloak could be used for token generation without breaking the compatibility with the clients
# See MaaS for more info and example for logout.
# Note that the refersh token is ruled out in this example.
class TokenGeneration(ObtainAuthToken):
    serializer_class = CredentialsSerializer

    @property
    def client(self):
        return Client().get_client()

    @swagger_auto_schema(request_body=CredentialsSerializer)
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        username = serializer.validated_data['username']
        password = serializer.validated_data['password']

        try:
            # Get the token and user info from Keycloak

            token = self.client.token(username=username,
                                      password=password)
            info = self.client.userinfo(token['access_token'])

            username = info['preferred_username']
            access_token = token['access_token']

            log.warning("User {} logging in ...".format(username))
            log.warning("Token obtained for user {} : {}".format(username, token))
            log.warning("Access for user {} : {}".format(username, access_token))
            log.warning("User info for user {} : {}".format(username, info))

            user_actor_rel = UserActorRel.objects.filter(user__username=username)

            # if the target user has a KeyCloak account but not an actor and user actor rel in the IB
            # create  actor and rel matching the user's KeyCloak details
            if len(user_actor_rel) is 0 and token is not None:
                create_user_actor_rel_mapping(info, username, password)
                user_actor_rel = UserActorRel.objects.filter(user__username=username)

            if len(user_actor_rel) is 0 or user_actor_rel[0].status != 'valid':
                return Response(status=401, data={
                    "details": "The account has not been validated for username : {}".format(username)})

            actor = user_actor_rel[0].actor

            if actor is None:
                return Response(status=401, data={"details": "No actor found for username : {}".format(username)})
            actor_serializer = MarketActorSerializer(actor)

            return Response({
                "token": access_token,
                "actor": actor_serializer.data,
                "username": username
            })

        except KeycloakAuthenticationError as e:
            log.exception("Keycloak error", e)
            return Response(status=e.response_code,
                            data=json.loads(e.response_body))
        except Exception as ex:
            log.exception("Exception while processing token request.", ex)
            return Response(status=500,
                            data={"details": "{}".format(ex)})


def create_user_actor_rel_mapping(info, username, password):
    log.warning("No actor found for the existing KeyCloak user, {}. Creating one...".format(username))

    try:

        payload = info

        log.warning("Creating user actor mappings for user : {}"
                    .format(username))

        users = User.objects.filter(username=username)

        if len(users) > 0:
            user = users[0]
        else:
            log.warning("Creating Django user ...")
            user = User.objects.create_user(
                password=password,
                username=username,
                first_name=payload['given_name'] if 'given_name' in payload else '',
                last_name=payload['family_name'] if 'family_name' in payload else '',
                email=payload['email'] if 'email' in payload else ''
            )
            user.save()
            log.warning("Successfully created Django user")

        realm_roles = info['realm_roles'] if 'realm_roles' in info else []
        client_roles = info['client_roles'] if 'client_roles' in info else []

        market_actor_type = get_actor_type_from_realm_and_client_roles(realm_roles, client_roles)
        if market_actor_type is None:
            log.warning('Unable to create actor, unable to retrieve actor type from user roles')
            user.delete()
            return

        log.warning("Creating actor for user ... ")
        actor = MarketActor(
            companyName=payload['company_name'] if 'company_name' in payload else '',
            email=payload['email'] if 'email' in payload else '',
            phone=payload['phone'] if 'phone' in payload else '',
            representativeName=(payload['given_name'] if 'given_name' in payload else '')
                               + " "
                               + (payload['family_name'] if 'family_name' in payload else ''),
            vat=payload['vat'] if 'vat' in payload else '',
            marketActorTypeid=market_actor_type
        )
        actor.save()
        log.warning("Successfully created actor for user with type: {}".format(actor.marketActorTypeid.type))

        log.warning("Creating user actor mapping ...")
        # create relation
        rel = UserActorRel(
            actor_id=actor.id,
            user_id=user.id,
            status='valid'
        )

        rel.save()
        log.warning("Successfully created user actor mapping...")

        # IF MP, register MP as actor in all marketplaces
        if actor.marketActorTypeid.type in ["Generic Participant", "generic participant"]:
            log.warning("Registering actor in marketplaces. Creating Marketplace_has_MarketActor objects ...")
            for market in Marketplace.objects.all():
                m = Marketplace_has_MarketActor(
                    marketplace=market,
                    marketActor=actor
                )
                m.save()

            log.warning("Successfully registered market actor in marketplaces")

        # IF Operator, assign Operator as the operator of every market in this flavour
        # (only 1 market operator per market deployment)
        if actor.marketActorTypeid.type in ["Market Operator", "market operator"]:
            for market in Marketplace.objects.all():
                market.marketOperatorid = actor
                market.save()

            log.warning("Successfully registered market operator in marketplaces")


    except Exception as e:
        log.warning(e)
        log.warning("Could not create actor for user: {}".format(username))


def get_actor_type_from_realm_and_client_roles(realm_roles, client_roles):
    log.warning("Retrieving market actor type from user roles")
    log.warning("User realm roles: {}".format(realm_roles))
    log.warning("User client roles: {}".format(client_roles))

    market_actor_types = MarketActorType.objects.all()
    log.warning("Available actor types from database: {}".format(market_actor_types))

    if len(market_actor_types) == 0:
        log.warning("No market actor types available in the database, impossible to create user and actor mappings")

    try:
        try:
            if 'memo' in realm_roles:
                return MarketActorType.objects.get(type='memo')
        except Exception as e:
            log.warning("memo actor type non existent in database")

        try:
            if 'market-participant' in realm_roles:
                return MarketActorType.objects.get(type='generic participant')
        except Exception as e:
            log.warning("generic participant actor type non existent in database")

        try:
            if 'operator' in realm_roles:
                return MarketActorType.objects.get(type='market operator')
        except Exception as e:
            log.warning("market operator actor type non existent in database")

        else:
            if len(client_roles) is not 1:
                log.warning('User has no assigned client roles or more than one client roles')
                log.warning('Can not retrieve an actor type for it')
                return None
            else:
                client_role_name = client_roles[0]
                if client_role_name not in client_specific_roles:
                    log.warning("No actor type mapping to client role :{}, can not create actor".format(client_role_name))
                    return None

                actor_type_name = client_specific_roles.get(client_role_name)
                actor_type = MarketActorType.objects.get(type=actor_type_name)
                return actor_type
    except Exception as e:
        log.warning("Unable to retrieve market actor type")
        log.warning(e)
        return None
