from django.core.management import CommandError, BaseCommand, call_command
from django.core.management.commands import dumpdata
from ib.models import ActionStatus
from django.utils import timezone

class Command(BaseCommand):
    help = 'Dump database'

    def handle(self, *args, **options):
        try:
            status_objs = ActionStatus.objects.all().exists()
            if status_objs:
                self.stdout.write('DB will be dumped')
                call_command('dumpdata', exclude=['contenttypes', 'auth', 'corsheaders', 'admin', 'sessions', 'authtoken'], output='ib_dump_' + str(timezone.now().today().date()) + '.json')
            else:
                self.stdout.write('DB is empty')
        except Exception as e:
            self.stdout.write(e)