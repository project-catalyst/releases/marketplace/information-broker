from django.contrib.auth.management.commands import createsuperuser
from django.contrib.auth.models import User
from django.core.management import CommandError, BaseCommand, call_command

from ib.models import ActionStatus


class Command(BaseCommand):
    help = 'Fill database if not empty'

    def handle(self, *args, **options):
        try:
            status_objs = ActionStatus.objects.all().exists()
            if status_objs:
                self.stdout.write('The models are initialized')
            else:
                self.stdout.write('Initialize models')
                call_command('loaddata', 'auth.json')
                call_command('loaddata', 'ib.json')
        except Exception as e:
            self.stdout.write(e)
