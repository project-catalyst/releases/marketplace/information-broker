import logging

from ib.models import ReferencePrice, Marketplace_has_MarketActor, ActionStatus

log = logging.getLogger(__name__)

def check_actions(marketplace, market_session, market_actions):
    log.warning("Checking newly created market actions for validity: {}".format(market_actions))
    existActorInMarket = False

    # market session information
    market_session_start_time = market_session.sessionStartTime
    market_session_end_time = market_session.sessionEndTime
    market_session_delivery_start_time = market_session.deliveryStartTime
    market_session_delivery_end_time = market_session.deliveryEndTime
    market_session_form = market_session.formid
    is_market_session_active = True if market_session.sessionStatusid.status == "active" else False

    action_status_valid = ActionStatus.objects.get(status="valid")
    action_status_invalid = ActionStatus.objects.get(status="invalid")

    for ma in market_actions:
        log.warning("Performing validity check for action: {}".format(ma))

        ma_date = ma.date
        ma_start_delivery = ma.actionStartTime
        ma_end_delivery = ma.actionEndTime

        ma_form = ma.formid
        ma_price = float(ma.price)

        ma_actor = ma.marketActorid

        current_reference_prices = ReferencePrice.objects.filter(marketplaceId=marketplace, formId=ma_form,
                                                                validityEndTime__gt=ma_date,
                                                                validityStartTime__lt=ma_date)
        if len(current_reference_prices) == 0:
            log.warning("No reference price exists for the market of this action")
            current_reference_price = None
        else:
            current_reference_price = current_reference_prices[0]
            log.warning("Current reference price for form: {} is {} euro".format(ma_form.form, current_reference_price))

        ma_status = ma.statusid

        log.warning("Current status of action is : {} ".format(ma_status.status))

        if ma_status.status == "unchecked":
            if not existActorInMarket:
                marketplace_has_actor = Marketplace_has_MarketActor.objects.filter(marketplace=marketplace,
                                                                                   marketActor=ma_actor)
                if len(marketplace_has_actor) > 0:
                    existActorInMarket = True

            log.warning("Market action date: {}".format(ma_date))
            log.warning("Market action start delivery: {}".format(ma_start_delivery))
            log.warning("Market action end delivery: {}".format(ma_end_delivery))
            log.warning("Session start delivery: {}".format(market_session_delivery_start_time))
            log.warning("Session end delivery: {}".format(market_session_delivery_end_time))
            log.warning("Market action form: {}".format(ma_form.form))
            log.warning("Market session form: {}".format(market_session_form.form))
            log.warning("Is market session active: {}".format(is_market_session_active))
            log.warning("Current reference price: {}".format(current_reference_price.price if current_reference_price is not None else ""))
            log.warning("Market action price: {}".format(ma_price))
            log.warning("Exists actor in market: {}".format(existActorInMarket))

            if (market_session_delivery_start_time <= ma_start_delivery <= market_session_delivery_end_time
                    and market_session_delivery_start_time <= ma_end_delivery <= market_session_delivery_end_time
                    and market_session_start_time <= ma_date <= market_session_end_time
                    and market_session_form == ma_form
                    and (True if current_reference_price is None else ma_price <= current_reference_price.price)
                    and is_market_session_active
                    and existActorInMarket):
                ma.statusid = action_status_valid
                log.warning("After validity check, action is: {}".format(ma.statusid.status))
            else:
                ma.statusid = action_status_invalid
                log.warning("After validity check, action is: {}".format(ma.statusid.status))


            ma.save()
            log.warning("Checked action saved to database :{}".format(ma))