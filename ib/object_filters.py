import logging

from ib.models import *
from ib.permissions import get_actor_from_request

log = logging.getLogger(__name__)


class Filter(object):

    @staticmethod
    def check_actor_role(actor, types):
        """

        :param actor:
        :param types:
        :return:
        """
        return actor.marketActorTypeid.type in types

    @staticmethod
    def marketplaces(request, id=None):
        """
        Filters the marketplaces based on the user id
        :param request: The request to get the user from
        :param id: the id of the marketplace
        :return: The list of allowed marketplaces
        """
        ret = None
        actor = get_actor_from_request(request)

        if id is None:
            if Filter.check_actor_role(actor, ["market operator"]):
                ret = Marketplace.objects.all()
            elif Filter.check_actor_role(actor, ["mbm"]):
                ret = Marketplace.objects.all()
            elif Filter.check_actor_role(actor, ["mcm"]):
                ret = Marketplace.objects.all()
            elif Filter.check_actor_role(actor, ["memo"]):
                ret = Marketplace.objects.all()
            elif Filter.check_actor_role(actor, ["generic participant", "aggregator"]):
                ret = [x.marketplace for x in Marketplace_has_MarketActor.objects.filter(marketActor=actor)]
            elif Filter.check_actor_role(actor, ["dso"]):
                ret = Marketplace.objects.filter(dSOid=actor)
        else:
            # All other cases should be handled by the permissions stuff
            try:
                ret = Marketplace.objects.get(id=id)
            except Exception as e:
                log.warning("Marketplace {} does not exist: {}".format(id, e))
                return None
        return ret

    @staticmethod
    def loads(request, id=None):
        """
        Filters the loads based on the user id
        :param request: The request to get the user from
        :param id: the id of the load
        :return: The list of allowed loads
        """
        ret = None
        actor = get_actor_from_request(request)

        if id is None:
            return ret
        else:
            # All other cases should be handled by the permissions stuff
            try:
                ret = Load.objects.get(id=id)
            except Exception as e:
                log.warning("Load {} does not exist: {}".format(id, e))
                return None
        return ret

    @staticmethod
    def marketsessions(request, marketsession_id=None):
        marketplaces = Filter.marketplaces(request=request)
        if marketplaces is None:
            return None

        ret = []
        if marketsession_id is None:
            ret = MarketSession.objects.filter(marketplaceid__in=marketplaces)
            return ret
        else:
            try:
                ret = MarketSession.objects.get(id=marketsession_id)
            except Exception as e:
                log.warning(e)
                return None
            if ret.marketplaceid in marketplaces:
                return ret
            else:
                return None

    @staticmethod
    def market_actors_of_marketplaces(request, marketplace):
        """

        :param request:
        :param marketplaces:
        :return:
        """
        ret = None
        actor = get_actor_from_request(request)

        try:
            if Filter.check_actor_role(actor, ["market operator"]):
                ret = [x.marketActor for x in Marketplace_has_MarketActor.objects.filter(marketplace=marketplace)]
            elif Filter.check_actor_role(actor, ["generic participant", "aggregator"]):
                ret = [x.marketActor for x in Marketplace_has_MarketActor.objects.filter(marketplace=marketplace, marketActor=actor)]
            elif Filter.check_actor_role(actor, ["dso"]):
                # If the DSO is indeed the market DSO, they should only see their profile.
                # Initial access control guarantees that they should only see their marketplaces.
                ret = [actor]

        except Exception as e:
            log.warning("Marketplace {} does not exist: {}".format(id, e))
            return None
        return ret

    @staticmethod
    def market_actions_of_session(request, market_session):
        ret = None
        actor = get_actor_from_request(request)

        try:
            ret = MarketAction.objects.filter(marketSessionid=market_session)
            if Filter.check_actor_role(actor, ["market operator"]):
                pass
            elif Filter.check_actor_role(actor, ["memo"]):
                pass
            elif Filter.check_actor_role(actor, ["generic participant", "aggregator"]):
                ret = ret.filter(marketActorid=actor)
            elif Filter.check_actor_role(actor, ["dso"]):
                pass
            elif Filter.check_actor_role(actor, ["mbm"]):
                pass
            elif Filter.check_actor_role(actor, ["mcm"]):
                pass


        except Exception as e:
            log.warning("Marketplace {} does not exist: {}".format(id, e))
            return None
        return ret

    @staticmethod
    def market_actions_of_session_by_action_status(request, market_session, action_status):
        ret = None
        actor = get_actor_from_request(request)

        try:
            ret = MarketAction.objects.filter(marketSessionid=market_session, statusid__status=action_status)
            if Filter.check_actor_role(actor, ["market operator"]):
                pass
            elif Filter.check_actor_role(actor, ["memo"]):
                pass
            elif Filter.check_actor_role(actor, ["generic participant", "aggregator"]):
                ret = ret.filter(marketActorid=actor)
            elif Filter.check_actor_role(actor, ["dso"]):
                pass
            elif Filter.check_actor_role(actor, ["mbm"]):
                pass
            elif Filter.check_actor_role(actor, ["mcm"]):
                pass


        except Exception as e:
            log.warning("Marketplace {} does not exist: {}".format(id, e))
            return None
        return ret

    @staticmethod
    def marketaction(request, action_id=None):
        ret = []
        actor = get_actor_from_request(request)

        try:
            ret = MarketAction.objects.get(id=action_id)
            if Filter.check_actor_role(actor, ["market operator"]):
                pass
            elif Filter.check_actor_role(actor, ["memo"]):
                pass
            elif Filter.check_actor_role(actor, ["generic participant", "aggregator"]):
                ret = ret.filter(marketActorid=actor)
            elif Filter.check_actor_role(actor, ["dso"]):
                pass
            elif Filter.check_actor_role(actor, ["mbm"]):
                pass
            elif Filter.check_actor_role(actor, ["mcm"]):
                pass
        except Exception as e:
            log.warning("Market action {} does not exist: {}".format(action_id, e))
            return None
        return ret

    @staticmethod
    def marketactioncounteroffer(request, macounter_id=None):
        ret = []
        actor = get_actor_from_request(request)

        try:
            ret = MarketActionCounterOffer.objects.get(id=macounter_id)
            if Filter.check_actor_role(actor, ["market operator"]):
                pass
            elif Filter.check_actor_role(actor, ["memo"]):
                pass
            elif Filter.check_actor_role(actor, ["dso"]):
                pass
            elif Filter.check_actor_role(actor, ["mbm"]):
                pass
            elif Filter.check_actor_role(actor, ["mcm"]):
                pass
        except Exception as e:
            log.warning("Market action counteroffer {} does not exist: {}".format(macounter_id, e))
            return None
        return ret

    @staticmethod
    def invoices(request, marketplace_id=None, market_session_id=None):
        """

        :param request:
        :param marketplace_id:
        :param market_session_id:
        :return:
        """

        ret = None
        actor = get_actor_from_request(request)
        all_market_actions = []

        if marketplace_id is None and market_session_id is None:
            marketplaces = Filter.marketplaces(request=request)
            if marketplaces is None:
                return None
            for marketplace in marketplaces:
                for market_session in MarketSession.objects.filter(marketplaceid=marketplace):
                    all_market_actions.extend(Filter.market_actions_of_session(request=request, market_session=market_session))
        else:
            marketplace = Filter.marketplaces(request=request, id=marketplace_id)
            market_session = None
            if marketplace is None:
                return None
            try:
                market_session = MarketSession.objects.get(marketplaceid=marketplace_id, id=market_session_id)
            except Exception as e:
                log.warning("Could not find marketsession {} of marketplace {}".format(market_session_id, marketplace_id))
                return None
            all_market_actions.extend(Filter.market_actions_of_session(request=request, market_session=market_session))

        bids = Transaction.objects.filter(marketActionCounterOfferid__marketAction_Bid_id__in=all_market_actions)
        offers = Transaction.objects.filter(marketActionCounterOfferid__marketAction_Offer_id__in=all_market_actions)
        transactions = bids | offers
        ret = Invoice.objects.filter(transactionid__in=transactions)
        return ret

    @staticmethod
    def profile(request):
        """

        :param request:
        :param marketplace_id:
        :param market_session_id:
        :return:
        """

        ret = get_actor_from_request(request)
        return ret