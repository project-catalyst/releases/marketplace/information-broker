import logging
from django.contrib.auth.models import User

from drf_openapi.entities import VersionedSerializers
from rest_framework import serializers
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_201_CREATED, HTTP_204_NO_CONTENT, \
    HTTP_301_MOVED_PERMANENTLY, HTTP_401_UNAUTHORIZED, HTTP_403_FORBIDDEN, HTTP_404_NOT_FOUND, \
    HTTP_500_INTERNAL_SERVER_ERROR

from ib.models import ActionStatus, ActionType, Form, TimeFrame, MarketServiceType, MarketActorType, MarketActor, \
    Marketplace, SessionStatus, MarketSession, MarketAction, MarketActionCounterOffer, Transaction, Invoice, \
    Marketplace_has_MarketActor, ElectricityPrice, ReferencePrice, Rule, UserActorRel, SG_Consumption, SG_Generation, \
    Load, LoadValue

import math


class ActionStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = ActionStatus
        fields = ('id', 'status',)
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class ActionTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ActionType
        fields = ('id', 'type',)
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class FormSerializer(serializers.ModelSerializer):
    class Meta:
        model = Form
        fields = ('id', 'form',)
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class TimeFrameSerializer(serializers.ModelSerializer):
    class Meta:
        model = TimeFrame
        fields = ('id', 'type')
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class MarketServiceTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = MarketServiceType
        fields = ('id', 'type', 'timeFrameid')
        depth = 1
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class MarketActorTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = MarketActorType
        fields = ('id', 'type')
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class MarketActorSerializer(serializers.ModelSerializer):
    class Meta:
        model = MarketActor
        fields = ('id', 'companyName', 'email', 'marketActorTypeid', 'phone', 'representativeName', 'vat')
        depth = 1
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class MarketActorSerializer1(serializers.ModelSerializer):
    class Meta:
        model = MarketActor
        fields = ('id', 'companyName', 'email', 'marketActorTypeid', 'phone', 'representativeName', 'vat',
                  'username')
        # depth = 1
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }

    username = serializers.SerializerMethodField()

    def get_username(self, obj):

        correspondingUser = UserActorRel.objects.get(actor=obj).user

        if correspondingUser is None:
            return ' '
        else:
            return correspondingUser.username



class MarketPlaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Marketplace
        fields = ('id', 'marketOperatorid', 'marketServiceTypeid', 'dSOid',)
        depth = 3
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class MarketPlaceSerializer1(serializers.ModelSerializer):
    class Meta:
        model = Marketplace
        fields = ('id', 'marketOperatorid', 'marketServiceTypeid', 'dSOid',)
        depth = 3
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


# class MarketplaceSerializer(VersionedSerializers):
#     """
#     Changelog:
#
#     * **v1.0**: `title` is optional
#     * **v2.0**: `title` is required
#     """
#
#     VERSION_MAP = (
#         ('<2.0', MarketPlaceSerializer),
#     )


class SessionStatusSerializer(serializers.ModelSerializer):
    class Meta:
        depth = 0
        model = SessionStatus
        fields = ('id', 'status')
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class MarketSessionSerializer(serializers.ModelSerializer):
    sessionStatusid = SessionStatusSerializer(required=True)
    formid = FormSerializer(required=True)
    marketplaceid = MarketPlaceSerializer(required=True)

    class Meta:
        model = MarketSession
        fields = ('id', 'sessionStartTime', 'sessionEndTime', 'deliveryStartTime', 'deliveryEndTime', 'marketplaceid',
                  'sessionStatusid', 'formid', 'clearingPrice',)
        depth = 3
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class MarketSessionSerializer1(serializers.ModelSerializer):
    class Meta:
        model = MarketSession
        fields = ('id', 'sessionStartTime', 'sessionEndTime',
                  'deliveryStartTime', 'deliveryEndTime', 'marketplaceid', 'sessionStatusid', 'formid',
                  'clearingPrice',)
        depth = 3
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class MarketSessionSerializer2(serializers.ModelSerializer):
    class Meta:
        model = MarketSession
        fields = ('id', 'sessionStartTime', 'sessionEndTime',
                  'deliveryStartTime', 'deliveryEndTime', 'marketplaceid', 'sessionStatusid', 'formid',
                  'clearingPrice',)
        depth = 0
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class LoadSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=True)

    class Meta:
        model = Load
        fields = (
            'id', 'date',
        )

        # depth = 3
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class LoadSerializer1(serializers.ModelSerializer):
    class Meta:
        model = Load
        fields = (
            'id', 'date',
        )
        depth = 5
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


## class LoadValueSerializer

class LoadValueSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=True)

    class Meta:
        model = LoadValue

        fields = (
            'id', 'loadid', 'parameter', 'uom', 'value'
        )

        # depth = 3
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class LoadValueSerializer1(serializers.ModelSerializer):
    class Meta:
        model = LoadValue

        fields = (
            'id', 'loadid', 'parameter', 'uom', 'value'
        )

        depth = 5
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class MarketActionSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=True)

    class Meta:
        model = MarketAction

        fields = (
            'id', 'date', 'actionStartTime', 'actionEndTime', 'value', 'uom', 'price', 'deliveryPoint',
            'marketSessionid',
            'marketActorid', 'formid', 'actionTypeid', 'statusid', 'loadid', 'cpu', 'ram', 'disk',)
        depth = 5
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }

    cpu = serializers.SerializerMethodField()
    ram = serializers.SerializerMethodField()
    disk = serializers.SerializerMethodField()

    def get_cpu(self, obj):

        if obj.loadid is None:
            return 0.0

        else:
            load_val = LoadValue.objects.get(loadid=obj.loadid, parameter="CPU")

            if load_val is None:
                return 0.0
            else:
                return load_val.value

    def get_ram(self, obj):

        if obj.loadid is None:
            return 0.0

        else:
            load_val = LoadValue.objects.get(loadid=obj.loadid, parameter="RAM")

            if load_val is None:
                return 0.0
            else:
                return load_val.value

    def get_disk(self, obj):

        if obj.loadid is None:
            return 0.0

        else:
            load_val = LoadValue.objects.get(loadid=obj.loadid, parameter="Disk")

            if load_val is None:
                return 0.0
            else:
                return load_val.value


class MarketActionSerializerEdit(serializers.ModelSerializer):
    id = serializers.IntegerField(required=True)

    class Meta:
        model = MarketAction

        fields = (
            'id', 'date', 'actionStartTime', 'actionEndTime', 'value', 'uom', 'price', 'deliveryPoint',
            'marketSessionid',
            'marketActorid', 'formid', 'actionTypeid', 'statusid', 'loadid', 'cpu', 'ram', 'disk',)
        # depth = 5
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }

    cpu = serializers.DecimalField(max_digits=10, decimal_places=3)
    ram = serializers.DecimalField(max_digits=10, decimal_places=3)
    disk = serializers.DecimalField(max_digits=10, decimal_places=3)


class MarketActionClearingSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=True)

    class Meta:
        model = MarketAction

        fields = (
            'id', 'date', 'actionStartTime', 'actionEndTime', 'value', 'uom', 'price', 'deliveryPoint',
            'marketSessionid',
            'marketActorid', 'formid', 'actionTypeid', 'statusid', 'loadid', 'cpu', 'ram', 'disk',)
        depth = 0
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }

    cpu = serializers.SerializerMethodField()
    ram = serializers.SerializerMethodField()
    disk = serializers.SerializerMethodField()

    def get_cpu(self, obj):

        if obj.loadid is None:
            return 0

        else:
            load_val = LoadValue.objects.get(loadid=obj.loadid, parameter="CPU")

            if load_val is None:
                return 0
            else:
                return math.ceil(load_val.value)

    def get_ram(self, obj):

        if obj.loadid is None:
            return 0

        else:
            load_val = LoadValue.objects.get(loadid=obj.loadid, parameter="RAM")

            if load_val is None:
                return 0
            else:
                return math.ceil(load_val.value)

    def get_disk(self, obj):

        if obj.loadid is None:
            return 0

        else:
            load_val = LoadValue.objects.get(loadid=obj.loadid, parameter="Disk")

            if load_val is None:
                return 0
            else:
                return math.ceil(load_val.value)


class MarketActionSerializer1(serializers.ModelSerializer):
    class Meta:
        model = MarketAction
        fields = (
            'id', 'date', 'actionStartTime', 'actionEndTime', 'value', 'uom', 'price', 'deliveryPoint',
            'marketSessionid',
            'marketActorid', 'formid', 'actionTypeid', 'statusid', 'loadid',)
        depth = 5
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class MarketActionSerializer2(serializers.ModelSerializer):
    class Meta:
        model = MarketAction
        fields = (
            'id', 'date', 'actionStartTime', 'actionEndTime', 'value', 'uom', 'price', 'deliveryPoint',
            'marketSessionid',
            'marketActorid', 'formid', 'actionTypeid', 'statusid', 'loadid')
        depth = 0
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }

class PrioritisedMarketActionSerializer(serializers.Serializer):
    marketaction = MarketActionSerializer2(required=True)
    priority = serializers.IntegerField()


class PrioritisedMarketActionsClearingSerializer(serializers.Serializer):
    marketaction = MarketActionClearingSerializer(required=True)
    priority = serializers.IntegerField()


class MarketActionEditSingleSerializer(serializers.Serializer):
    date = serializers.DateTimeField()
    actionStartTime = serializers.DateTimeField()
    actionEndTime = serializers.DateTimeField()
    value = serializers.DecimalField(max_digits=10, decimal_places=3)
    uom = serializers.CharField()
    price = serializers.DecimalField(max_digits=10, decimal_places=3)
    deliveryPoint = serializers.CharField()
    marketSessionid = serializers.IntegerField()
    marketActorid = serializers.IntegerField()
    actionType = serializers.CharField()
    status = serializers.CharField()
    loadid = serializers.IntegerField()

    class Meta:
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class MarketActionCounterOfferSerializer(serializers.ModelSerializer):
    class Meta:
        model = MarketActionCounterOffer
        fields = ('id', 'marketAction_Bid_id', 'marketAction_Offer_id', 'exchangedValue',)
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class MarketActionCounterOfferSerializerClearingResult(serializers.Serializer):
    counteroffers = MarketActionCounterOfferSerializer(many=True, required=False)
    actions = serializers.ListField(child=serializers.IntegerField())


class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = ('id', 'dateTime', 'marketActionCounterOfferid',)
        depth = 1
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class TransactionSerializer1(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = ('id', 'dateTime', 'marketActionCounterOfferid',)
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class InvoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Invoice
        fields = ('id', 'date', 'penalty', 'amount', 'fixedFee', 'transactionid',)
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class Marketplace_has_MarketActorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Marketplace_has_MarketActor
        fields = ('id', 'marketplace', 'marketActor',)
        depth = 1
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class ElectricityPriceSerializer(serializers.ModelSerializer):
    class Meta:
        model = ElectricityPrice
        fields = ('id', 'price',)
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class ReferencePriceSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReferencePrice
        fields = ('id', 'price', 'validityStartTime', 'validityEndTime', 'formId', 'marketplaceId',)
        depth = 1
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class ReferencePriceSerializer2(serializers.ModelSerializer):
    class Meta:
        model = ReferencePrice
        fields = ('id', 'price', 'validityStartTime', 'validityEndTime', 'formId', 'marketplaceId',)
        depth = 0
        error_status_codes = {
            HTTP_400_BAD_REQUEST: 'Bad Request',
            HTTP_201_CREATED: 'Created',
            HTTP_204_NO_CONTENT: 'No content',
            HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
            HTTP_401_UNAUTHORIZED: 'Unauthorized',
            HTTP_403_FORBIDDEN: 'Forbidden',
            HTTP_404_NOT_FOUND: 'Not found',
            HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
        }


class RuleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rule
        fields = ('id', 'title', 'description', 'timestamp',)


class GeneralSerializer(serializers.ModelSerializer):
    class Meta:
        model = None


class UserActorRelSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserActorRel
        fields = ('user', 'actor', 'status',)


class UserActorStatusSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=200)
    actor = serializers.IntegerField()
    status = serializers.CharField(max_length=200)


class UserMarketSerializer(serializers.Serializer):
    type = serializers.CharField(max_length=200)
    form = serializers.CharField(max_length=200)
    url = serializers.CharField(max_length=200)
    id = serializers.IntegerField()
    component = serializers.CharField(max_length=200)
    actorId = serializers.IntegerField(required=False)

class AvailableMarketSerializer(serializers.Serializer):
    type = serializers.CharField(max_length=200)
    form = serializers.CharField(max_length=200)
    url = serializers.CharField(max_length=200)
    id = serializers.IntegerField()
    component = serializers.CharField(max_length=200)

class AvailableMarketSerializer(serializers.Serializer):
    type = serializers.CharField(max_length=200)
    form = serializers.CharField(max_length=200)
    url = serializers.CharField(max_length=200)
    id = serializers.IntegerField()
    component = serializers.CharField(max_length=200)


class ConstraintsSerializer(serializers.Serializer):
    constraintId = serializers.IntegerField()
    description = serializers.CharField(max_length=200)


class CorrelatedActionSerializer(serializers.Serializer):
    informationBrokerId = serializers.IntegerField()
    actionId = serializers.IntegerField()
    sessionId = serializers.IntegerField()

class CorrelationSerializer(serializers.Serializer):
    correlationId = serializers.IntegerField()
    constraintId = serializers.IntegerField()
    actions = CorrelatedActionSerializer(many=True)

class CorrelationsSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=200)
    correlations = CorrelationSerializer(many=True)

class SystemParametersSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField(max_length=200)
    value = serializers.CharField(max_length=200)


class CorrelatedMarketActionsSerializer(serializers.Serializer):
    #n = serializers.CharField(max_length=200)
    #GID = serializers.IntegerField()
    timeframe = serializers.CharField(max_length=200)
    timestamp = serializers.DateField()
    constraintId = serializers.IntegerField()
    actions = CorrelatedActionSerializer(many=True, required=False)
    depth = 1
    error_status_codes = {
    HTTP_400_BAD_REQUEST: 'Bad Request',
    HTTP_201_CREATED: 'Created',
    HTTP_204_NO_CONTENT: 'No content',
    HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
    HTTP_401_UNAUTHORIZED: 'Unauthorized',
    HTTP_403_FORBIDDEN: 'Forbidden',
    HTTP_404_NOT_FOUND: 'Not found',
    HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
    }


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'id', 'first_name', 'last_name', 'email')


class UserTokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'password')


class SG_Consumption_serializer(serializers.ModelSerializer):
    Customer_Number = serializers.CharField(max_length=30)
    Customer_Name = serializers.CharField(max_length=64)
    Date = serializers.CharField(max_length=10)
    Measuring_Period_Duration = serializers.CharField(max_length=5)
    Value_Identifier = serializers.CharField(max_length=10)
    Unique_cut_No = serializers.CharField(max_length=5)  # change the . to _
    Unique_CHA_No = serializers.CharField(max_length=64)  # change the . to _
    Device_ID = serializers.CharField(max_length=10)
    Measurements = serializers.ListField(child=serializers.CharField(max_length=16))
    Port_Number = serializers.CharField(max_length=5)
    Unit = serializers.CharField(max_length=10)
    Channel_Name = serializers.CharField(max_length=10)
    Timestamp = serializers.CharField(max_length=8)
    Transformer_Ratio = serializers.CharField(max_length=5)

    class Meta:
        model = SG_Consumption
        fields = (
            'Customer_Number', 'Customer_Name', 'Date', 'Measuring_Period_Duration', 'Value_Identifier',
            'Unique_cut_No',
            'Unique_CHA_No', 'Device_ID', 'Measurements', 'Port_Number', 'Unit', 'Channel_Name', 'Timestamp',
            'Transformer_Ratio')


class SG_ConsumptionChaSerializer(serializers.ModelSerializer):
    Unique_CHA_No = serializers.CharField(max_length=64)  # change the . to _

    class Meta:
        model = SG_Consumption
        fields = ('Unique_CHA_No',)


class SG_Generation_serializer(serializers.ModelSerializer):
    Customer_Number = serializers.CharField(max_length=30)
    Customer_Name = serializers.CharField(max_length=64)
    Date = serializers.CharField(max_length=10)
    Measuring_Period_Duration = serializers.CharField(max_length=5)
    Value_Identifier = serializers.CharField(max_length=10)
    Unique_cut_No = serializers.CharField(max_length=5)  # change the . to _
    Unique_CHA_No = serializers.CharField(max_length=64)  # change the . to _
    Device_ID = serializers.CharField(max_length=10)
    Measurements = serializers.ListField(child=serializers.CharField(max_length=16))
    Port_Number = serializers.CharField(max_length=5)
    Unit = serializers.CharField(max_length=10)
    Channel_Name = serializers.CharField(max_length=10)
    Timestamp = serializers.CharField(max_length=8)
    Transformer_Ratio = serializers.CharField(max_length=5)

    class Meta:
        model = SG_Generation
        fields = (
            'Customer_Number', 'Customer_Name', 'Date', 'Measuring_Period_Duration', 'Value_Identifier',
            'Unique_cut_No',
            'Unique_CHA_No', 'Device_ID', 'Measurements', 'Port_Number', 'Unit', 'Channel_Name', 'Timestamp',
            'Transformer_Ratio')


class SG_GenerationChaSerializer(serializers.ModelSerializer):
    Unique_CHA_No = serializers.CharField(max_length=64)

    class Meta:
        model = SG_Generation
        fields = ('Unique_CHA_No',)


class CredentialsSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()


class RefreshTokenSerializer(serializers.Serializer):
    refresh_token = serializers.CharField()


class MigrationDetailsSerializer(serializers.Serializer):
    marketaction = serializers.IntegerField()
    source = serializers.CharField(max_length=200)
    destination = serializers.CharField(max_length=200)
    delivery_start = serializers.DateTimeField()
    delivery_end = serializers.DateTimeField()
    transaction = serializers.IntegerField()
    depth = 1
    error_status_codes = {
        HTTP_400_BAD_REQUEST: 'Bad Request',
        HTTP_201_CREATED: 'Created',
        HTTP_204_NO_CONTENT: 'No content',
        HTTP_301_MOVED_PERMANENTLY: 'Moved permanently',
        HTTP_401_UNAUTHORIZED: 'Unauthorized',
        HTTP_403_FORBIDDEN: 'Forbidden',
        HTTP_404_NOT_FOUND: 'Not found',
        HTTP_500_INTERNAL_SERVER_ERROR: 'Internal server error'
    }