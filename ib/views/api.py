from datetime import datetime, timedelta
import json
from itertools import chain
import traceback

from django.db.models import Q
from django.http import Http404, JsonResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from drf_yasg.utils import swagger_auto_schema
from rest_framework import mixins, generics, status
from django.conf import settings
from rest_framework.response import Response
from rest_framework.views import APIView

from ib.object_filters import Filter
from ib.permissions import *
from rest_framework import permissions
from ib.serializer import *
from ib.swagger_responses import *
from ib.utils import *
from django.contrib.auth.models import User
from ib.models import TimeFrame
from ib.action_validity_check import action_validity_check
from ib.views.keycloak.scopes import  *
import pytz


class MarketPlaceList(mixins.ListModelMixin,
                      mixins.CreateModelMixin,
                      generics.GenericAPIView):
    serializer_class = MarketPlaceSerializer
    queryset = Marketplace.objects.all()

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, MCM_READ, MBM_READ, KNOWN_MARKET_ACTOR_READ, MEMO_READ],
        'POST': [MARKET_OPERATOR_WRITE, MCM_WRITE, MBM_WRITE, MEMO_WRITE],
    }

    @swagger_auto_schema(operation_summary="Get the list of marketplaces", responses=MarketPlaceList_GET)
    def get(self, request, format=None):
        """
        Retrieve List of MarketPlaces
        """
        mplaces = Filter.marketplaces(request=request)
        serializer = MarketPlaceSerializer(mplaces, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(responses=MarketPlaceList_POST, operation_summary="Marketplace creation",
                         request_body=MarketPlaceSerializer)
    def post(self, request, format=None):
        """
        Post a new MarketPlace
        """
        data = request.data
        serializer = MarketPlaceSerializer1(data=data)
        if serializer.is_valid():
            dso = get_object_or_404(MarketActor, pk=data['dSOid'])
            operator = get_object_or_404(MarketActor, pk=data['marketOperatorid'])
            mstype = get_object_or_404(MarketServiceType, pk=data['marketServiceTypeid'])
            newplace = Marketplace(dSOid=dso,
                                   marketOperatorid=operator,
                                   marketServiceTypeid=mstype)
            newplace.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MarketPlaceDetail(mixins.RetrieveModelMixin,
                        mixins.UpdateModelMixin,
                        mixins.DestroyModelMixin,
                        generics.GenericAPIView):
    queryset = Marketplace.objects.all()
    serializer_class = MarketPlaceSerializer

    keycloak_scopes = {
        'GET': [MARKET_PARTICIPANT_READ, DSO_READ, MARKET_OPERATOR_READ, MCM_READ, MBM_READ, MEMO_READ, KNOWN_MARKET_ACTOR_READ],
        'DELETE': [MARKET_OPERATOR_WRITE, MCM_WRITE, MBM_WRITE, MEMO_WRITE],
        'PUT': [MARKET_OPERATOR_WRITE, MCM_WRITE, MBM_WRITE, MEMO_WRITE]
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID], responses=MarketPlaceDetail_GET,
                         operation_summary="Get marketplace")
    def get(self, request, pk, format=None):
        """
        Get MarketPlace details
        """
        mplace = Filter.marketplaces(request=request, id=pk)
        if mplace is None:
            return Response(status=404, data={"details": "Marketplace not found"})
        serializer = MarketPlaceSerializer(mplace)
        return Response(serializer.data)

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID], responses=MarketPlaceDetail_DELETE,
                         operation_summary="Delete marketplace", request_body=MarketPlaceSerializer)
    def delete(self, request, pk, format=None):
        """
        Delete an existing MarketPlace
        """
        mplace = Filter.marketplaces(request=request, id=pk)
        if mplace is None:
            return Response(status=404)
        mplace.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID], responses=MarketPlaceDetail_PUT,
                         operation_summary="Update marketplace", request_body=MarketPlaceSerializer)
    def put(self, request, pk, format=None):
        """
        Update an existing MarketPlace
        """
        mplace = Filter.marketplaces(request=request, id=pk)
        if mplace is None:
            return Response(status=404)
        serializer = MarketPlaceSerializer(mplace, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MarketPlaceOfFormWithActiveSession(mixins.ListModelMixin,
                                         mixins.CreateModelMixin,
                                         generics.GenericAPIView):
    queryset = Marketplace.objects.all()
    serializer_class = MarketPlaceSerializer

    keycloak_scopes = {
        'GET': [KNOWN_MARKET_ACTOR_READ,  MARKET_OPERATOR_READ, MCM_READ, MBM_READ, MEMO_READ],
    }

    @swagger_auto_schema(manual_parameters=[FORM, TIMEFRAME],
                         operation_summary="Get the list of marketplaces of a given form with active sessions",
                         responses=MarketPlaceOfForm_GET)
    def get(self, request, *args, **kwargs):
        """
        Retrieve List of MarketPlaces of given form with active sessions
        """
        timeframes = dict(TimeFrame.type_choices).keys()
        forms = dict(Form.form_choices).keys()
        mplaces_of_form = []
        mplaces = Filter.marketplaces(request=request)
        active_sessions = []

        if 'timeframe' not in kwargs or 'form' not in kwargs:
            return Response(status=status.HTTP_404_NOT_FOUND)

        timeframe = kwargs['timeframe']
        form = kwargs['form']

        if form not in forms or timeframe not in timeframes:
            return Response(status=status.HTTP_404_NOT_FOUND)

        mps_timeframe = [mp for mp in mplaces if mp.marketServiceTypeid.timeFrameid.type == timeframe]

        for mp in mps_timeframe:
            active_sessions = MarketSession.objects.filter(marketplaceid=mp.id, formid=Form.objects.get(form=form),
                                                           sessionStatusid=SessionStatus.objects.get(status="active"))
            if len(active_sessions) > 0:
                mplaces_of_form.append(Marketplace.objects.get(id=mp.id))
        serializer = MarketPlaceSerializer(mplaces_of_form, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class MarketPlaceOfForm(mixins.ListModelMixin,
                        mixins.CreateModelMixin,
                        generics.GenericAPIView):
    queryset = Marketplace.objects.all()
    serializer_class = MarketPlaceSerializer

    keycloak_scopes = {
        'GET': [KNOWN_MARKET_ACTOR_READ,  MARKET_OPERATOR_READ, MCM_READ, MBM_READ, MEMO_READ],
    }

    @swagger_auto_schema(manual_parameters=[FORM, TIMEFRAME],
                         operation_summary="Get the list of marketplaces of a given form",
                         responses=MarketPlaceOfForm_GET)
    def get(self, request, *args, **kwargs):
        """
        Retrieve List of MarketPlaces of given form
        """
        timeframes = dict(TimeFrame.type_choices).keys()
        forms = dict(Form.form_choices).keys()
        mplaces_of_form = []
        mplaces = Filter.marketplaces(request=request)
        sessions = []

        if 'timeframe' not in kwargs or 'form' not in kwargs:
            return Response(status=status.HTTP_404_NOT_FOUND)

        timeframe = kwargs['timeframe']
        form = kwargs['form']

        if form not in forms or timeframe not in timeframes:
            return Response(status=status.HTTP_404_NOT_FOUND)

        mps_timeframe = [mp for mp in mplaces if mp.marketServiceTypeid.timeFrameid.type == timeframe]

        for mp in mps_timeframe:
            sessions = MarketSession.objects.filter(marketplaceid=mp.id, formid=Form.objects.get(form=form))
            if len(sessions) > 0:
                mplaces_of_form.append(Marketplace.objects.get(id=mp.id))
        serializer = MarketPlaceSerializer(mplaces_of_form, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class MarketSessions(mixins.ListModelMixin,
                     mixins.CreateModelMixin,
                     generics.GenericAPIView):
    queryset = MarketSession.objects.all()
    serializer_class = MarketSessionSerializer

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, MARKET_PARTICIPANT_READ, DSO_READ, MCM_READ, MBM_READ, MEMO_READ],
        'POST': [MARKET_OPERATOR_WRITE, MCM_WRITE, MBM_WRITE, MEMO_WRITE]
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID],
                         responses=MarketSessions_GET,
                         operation_summary="Get marketplace sessions")
    def get(self, request, pk, format=None):
        """
        Get market sessions of an existing marketplace
        """
        mplace = Filter.marketplaces(request=request, id=pk)
        if mplace is None:
            return Response(status=404)
        get_sessions = mplace.marketsession_set
        serializer = MarketSessionSerializer(get_sessions, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID],
                         responses=MarketSessions_POST,
                         operation_summary="Create marketsession for the requested marketplace",
                         request_body=MarketSessionSerializer)
    def post(self, request, pk, format=None):
        """
        Post a new market session for a specific marketplace
        """
        data = request.data
        serializer = MarketSessionSerializer(data=data)
        if serializer.is_valid():
            ses_status = get_object_or_404(SessionStatus, pk=data['sessionStatusid']['id'])
            thisform = get_object_or_404(Form, pk=data['formid']['id'])
            mplace = get_object_or_404(Marketplace, pk=pk)
            this_session = MarketSession(marketplaceid=mplace,
                                         sessionStatusid=ses_status,
                                         formid=thisform,
                                         sessionStartTime=data['sessionStartTime'],
                                         sessionEndTime=data['sessionEndTime'],
                                         deliveryStartTime=data['deliveryStartTime'],
                                         deliveryEndTime=data['deliveryEndTime'],
                                         clearingPrice=data['clearingPrice'])
            this_session.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ValidActionsOfAllActiveSessions(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, MARKET_PARTICIPANT_READ, DSO_READ, MCM_READ, MBM_READ, MEMO_READ],
    }

    @swagger_auto_schema(manual_parameters=[TIMEFRAME, ACTION_STATUS],
                         responses=MarketActions_GET,
                         operation_summary="Get all valid actions of the current active sessions")
    def get(self, request, format=None,   *args, **kwargs):
        """
        Get the list of all valid actions from every current active session
        """

        log.warning("Entering service: getAllMarketActions")
        timeframe = kwargs.get('timeframe')
        action_status = kwargs.get('status')
        log.warning("Route parameters identified are  timeframe: {} and action status: {}".format(timeframe, action_status))

        # TODO: get only mplaces in which the user is registered
        actor = get_actor_from_request(request)
        log.warning('Returning {} actions for actor: {}'.format(action_status, actor))

        mplaces_has_actor = Marketplace_has_MarketActor.objects.filter(marketActor=actor)

        mplaces = []
        for mpa in mplaces_has_actor:
            mplaces.append(mpa.marketplace)

        if len(mplaces) <= 0:
            log.warning("No marketplaces found for timeframe: {}".format(timeframe))
            return Response(status=404)

        active_sessions = MarketSession.objects.filter(marketplaceid__in=mplaces,
                                                    sessionStatusid__status='active')

        valid_actions = MarketAction.objects.filter(marketSessionid__in=active_sessions,
                                                 statusid__status=action_status)

        actions_serializer = MarketActionSerializer(valid_actions, many=True)


        log.warning("Actions to be returned: {}".format(actions_serializer.data))
        return Response(status=200, data=actions_serializer.data)

class ActiveMarketSessions(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, MARKET_PARTICIPANT_READ, DSO_READ, MCM_READ, MBM_READ, MEMO_READ],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID],
                         responses=MarketSessions_GET,
                         operation_summary="Get marketplace active sessions")
    def get(self, request, pk, format=None):
        """
        Get the active market sessions of a specific marketplace
        """
        mplace = Filter.marketplaces(request=request, id=pk)
        if mplace is None:
            return Response(status=404)
        active_sessions = mplace.marketsession_set.filter(sessionStatusid__status='active')
        serializer = MarketSessionSerializer(active_sessions, many=True)
        return Response(serializer.data)


class allUpdateMarketSessions(mixins.ListModelMixin,
                              mixins.UpdateModelMixin,
                              generics.GenericAPIView):
    serializer_class = MarketSessionSerializer

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ,  MCM_READ, MBM_READ, MEMO_READ],
        'PUT': [MARKET_OPERATOR_WRITE, MCM_WRITE, MBM_WRITE, MEMO_WRITE]
    }

    @swagger_auto_schema(manual_parameters=[SESSION_STATUS], responses=MarketSessions_GET,
                         operation_summary="Get marketplace cleared or completed sessions")
    def get(self, request, *args, **kwargs):
        """
        Set status as cleared to retrieve cleared sessions of a specific market
        Set status as completed to retrieve completed sessions of a specific market

        status: [active, closed, cleared, completed]
        """
        param = kwargs.get('status')
        if param in ['cleared', 'completed']:
            sessions = MarketSession.objects.filter(sessionStatusid__status=param)
            serializer = MarketSessionSerializer(sessions, many=True)
            return Response(serializer.data)
        else:
            return Response(status=404)

    @swagger_auto_schema(manual_parameters=[SESSION_STATUS], responses=allUpdateMarketSessions_PUT,
                         operation_summary="Set session status active or closed", request_body=MarketSessionSerializer)
    def put(self, request, *args, **kwargs):
        """
        Set status to update market session status accordingly
        'cleared', 'completed' work only for market sessions of the flexibility marketplace

        status: [active, closed, cleared, completed]
        """
        param = kwargs.get('status')
        log.warning("Enter service allUpdateMarketSessions...")
        log.warning("Status parameter: {}".format(param))
        to_status = {'active': 'inactive', 'closed': 'active', 'cleared': 'closed', 'completed': 'cleared'}
        time_param = {'active': 'sessionStartTime', 'closed': 'sessionEndTime', 'cleared': 'deliveryEndTime',
                      'completed': 'sessionEndTime'}
        now = pytz.utc.localize(datetime.now())
        _time_param = dict([(time_param[param] + "__lte", now)])
        ret = []
        if param in to_status:
            try:
                if param in {'cleared', 'completed'}:
                    gams = Marketplace.objects.filter(
                        marketServiceTypeid__in=MarketServiceType.objects.filter(type='ancillary-services'))
                    todaysessions = MarketSession.objects.filter(marketplaceid__in=gams,
                                                                 sessionStatusid__status=to_status[param],
                                                                 **_time_param)
                else:
                    todaysessions = MarketSession.objects.filter(sessionStatusid__status=to_status[param],
                                                                 **_time_param)
                for ses in todaysessions:
                    ses.sessionStatusid = SessionStatus.objects.get(status=param)
                    ses.save()
                    ret.append(ses)

                # return the list with the updated sessions
                serializer = MarketSessionSerializer(ret, many=True)
                return Response(serializer.data)

            except Exception as e:
                log.warning(e)
                return Response(status=404)
        else:
            return Response(status=404)


class MarketOperator(mixins.ListModelMixin,
                     mixins.CreateModelMixin,
                     generics.GenericAPIView):
    queryset = MarketActor.objects.all()
    serializer_class = MarketActorSerializer1

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, DSO_READ, MARKET_PARTICIPANT_READ],
        'POST': [MARKET_OPERATOR_WRITE]
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID],
                         responses=MarketOperator_GET,
                         operation_summary="List of registered Market Operators")
    def get(self, request, pk, format=None):
        """
        Through this endpoint information about the registered Market Operators is provided
        """
        mplace = Filter.marketplaces(request=request, id=pk)
        if mplace is None:
            return Response(status=404)
        serializer = MarketActorSerializer1(mplace.marketOperatorid)
        return Response(serializer.data)

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID],
                         responses=MarketOperator_POST,
                         operation_summary="Post information about Market Operators",
                         request_body=MarketActorSerializer1)
    def post(self, request, pk, *args, **kwargs):
        """
        Through this endpoint information about the registered Market Operators is posted
        """
        mplace = Filter.marketplaces(request=request, id=pk)
        data = request.data
        serializer = MarketActorSerializer1(data=data)
        if serializer.is_valid():
            actor = MarketActor(companyName=data['companyName'], email=data['email'],
                                marketActorTypeid=MarketActorType.objects.get(type='market operator'),
                                phone=data['phone'],
                                representativeName=data['representativeName'],
                                vat=data['vat'])
            actor.save()
            mplace.marketOperatorid = actor
            mplace.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GenericParticipantofMarketplace(mixins.ListModelMixin,
                                      mixins.CreateModelMixin,
                                      generics.GenericAPIView):
    queryset = MarketActor.objects.all()
    serializer_class = MarketActorSerializer1

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, DSO_READ],
        'POST': [MARKET_OPERATOR_WRITE, KNOWN_MARKET_ACTOR_WRITE]
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID],
                         responses=PARTICIPANTOFMARKET_GET,
                         operation_summary="List of registered Market Participants of marketplace")
    def get(self, request, pk):
        """
        Through this endpoint information about the registered Market Participants of a certain marketplace is provided
        """

        actors = Filter.market_actors_of_marketplaces(request=request, marketplace=pk)
        if actors is None:
            return Response(status=404)
        serializer = MarketActorSerializer1(actors, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID], responses=PARTICIPANTOFMARKET_POST,
                         operation_summary="Post information about Market Participants",
                         request_body=MarketActorSerializer1)
    def post(self, request, pk, *args, **kwargs):
        """
        Through this endpoint a Market Participant can be created and registered to the given marketplace
        """
        mplace = Filter.marketplaces(request=request, id=pk)
        data = request.data
        serializer = MarketActorSerializer1(data=data)
        if serializer.is_valid():
            actor = MarketActor(companyName=data['companyName'], email=data['email'],
                                marketActorTypeid=MarketActorType.objects.get(type='generic participant'),
                                phone=data['phone'],
                                representativeName=data['representativeName'],
                                vat=data['vat'])
            actor.save()
            new_participant = Marketplace_has_MarketActor(marketplace=mplace,
                                                          marketActor=actor)
            new_participant.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class getMarketActorById(mixins.ListModelMixin,
                         mixins.CreateModelMixin,
                         generics.GenericAPIView):
    queryset = MarketActor.objects.all()
    serializer_class = MarketActorSerializer1

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, DSO_READ, KNOWN_MARKET_ACTOR_READ, KNOWN_MARKET_ACTOR_WRITE],
    }

    @swagger_auto_schema(manual_parameters=[MARKETACTOR_ID],
                         responses=PARTICIPANTOFMARKET_GET,
                         operation_summary="Get specific market actor by id")
    def get(self, request, pk):
        """
        Through this endpoint information about one market actor is provided
        """

        actors = MarketActor.objects.get(pk=pk)
        if actors is None:
            return Response(status=404)
        serializer = MarketActorSerializer1(actors)

        data = serializer.data
        return Response(serializer.data)


class getMarketActorByUsername(mixins.ListModelMixin,
                         mixins.CreateModelMixin,
                         generics.GenericAPIView):
    queryset = MarketActor.objects.all()
    serializer_class = MarketActorSerializer1

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, DSO_READ, KNOWN_MARKET_ACTOR_READ, KNOWN_MARKET_ACTOR_WRITE],
    }

    @swagger_auto_schema(manual_parameters=[USERNAME],
                         responses=PARTICIPANTOFMARKET_GET,
                         operation_summary="Get specific market actor by username")
    def get(self, request, username):
        """
        Through this endpoint information about one market actor is provided
        """

        actor = UserActorRel.objects.get(user__username=username).actor

        if actor is None:
            return Response(status=404)
        serializer = MarketActorSerializer1(actor)

        data = serializer.data
        return Response(serializer.data)


class getUsernameByActorId(mixins.ListModelMixin,
                         mixins.CreateModelMixin,
                         generics.GenericAPIView):
    queryset = MarketActor.objects.all()
    serializer_class = MarketActorSerializer1
    permission_classes = (IsMarketOperator | IsKnownMarketActorWrite | IsMarketplaceDSOReadOnly | IsKnownMarketActorRead,)

    @swagger_auto_schema(manual_parameters=[MARKETACTOR_ID],
                         responses=PARTICIPANTOFMARKET_GET,
                         operation_summary="Get specific market actor by id")
    def get(self, request, marketActorId):
        """
        Through this endpoint username of one market actor is provided
        """

        actors = MarketActor.objects.get(pk=marketActorId)
        if actors is None:
            return Response(status=404)

        correspondingUser = UserActorRel.objects.get(actor=marketActorId).user

        if correspondingUser is None:
            return Response(status=404, data={'details' : 'No user found for corresponding market actor id'})

        return Response(status=200, data={'username':correspondingUser.username})


class GenericParticipant(mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView):

    keycloak_scopes = {
        'PUT': [MARKET_OPERATOR_WRITE, DSO_READ, KNOWN_MARKET_ACTOR_WRITE],
    }

    @swagger_auto_schema(responses=PARTICIPANT_PUT,
                         operation_summary="Edit information of Market Participant",
                         request_body=MarketActorSerializer1)
    def put(self, request, *args, **kwargs):
        """
        Through this endpoint information about the registered Market Participants can be edited
        """
        data = request.data
        serializer = MarketActorSerializer1(data=data)
        if serializer.is_valid():
            try:
                existing_actor = MarketActor.objects.get(id=data['id'])
            except Exception as e:
                log.warning(e)
                return Response(status=404, data={"details": str(e)})
            submitting_actor = Filter.profile(request=request)
            if existing_actor.id == submitting_actor.id:
                existing_actor.companyName = data['companyName']
                existing_actor.email = data['email']
                existing_actor.marketActorTypeid = MarketActorType.objects.get(type='generic participant')
                existing_actor.phone = data['phone']
                existing_actor.representativeName = data['representativeName']
                existing_actor.vat = data['vat']
                existing_actor.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(status=403, data={"details": "User can edit only own profile"})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GetDSO(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ],
        'POST': [MARKET_OPERATOR_WRITE],
    }

    @swagger_auto_schema(responses=GetDSO_GET, operation_summary="List of DSO")
    def get(self, request, *args, **kwargs):
        """
        Through this endpoint information about the registered DSO is provided
        """
        try:
            dso = MarketActor.objects.filter(marketActorTypeid__type="dso")
            serializer = MarketActorSerializer(dso, many=True)
            return Response(serializer.data)
        except Exception as e:
            log.warning("No DSO found: {}".format(e))
            return Response(status=404)

    @swagger_auto_schema(responses=MarketOperator_POST, operation_summary="Create DSO",
                         request_body=MarketActorSerializer1)
    def post(self, request, *args, **kwargs):
        """
        Through this endpoint information about the registered DSO is posted
        """
        data = request.data
        serializer = MarketActorSerializer1(data=data)
        if serializer.is_valid():
            if MarketActorType.objects.get(id=data['marketActorTypeid']) == MarketActorType.objects.get(type='dso'):
                new_dso = MarketActor(companyName=data['companyName'],
                                      email=data['email'],
                                      phone=data['phone'],
                                      representativeName=data['representativeName'],
                                      vat=data['vat'],
                                      marketActorTypeid=MarketActorType.objects.get(type='dso'))
                new_dso.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(status=400,
                                data={"details": "Invalid Input. Provided details do not refer to DSO user."})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class putClearedSessions(APIView):
    """
    This interface updates the status of a market session
    from "closed" to "cleared"
    """

    keycloak_scopes = {
        'PUT': [MARKET_OPERATOR_WRITE, MCM_WRITE, MBM_WRITE, MEMO_WRITE],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, SESSION_ID],
                         responses=putCompletedSessions_PUT,
                         operation_summary="Updates the status of a market session from closed to cleared",
                         request_body=MarketSessionSerializer)
    def put(self, request, *args, **kwargs):
        try:
            ses = Marketplace.objects.get(pk=kwargs.get('pk')).marketsession_set.get(id=kwargs.get('sid'))
        except Exception as e:
            return Response(status=404)
        if ses.sessionStatusid == SessionStatus.objects.get(status='closed'):
            ses.sessionStatusid = SessionStatus.objects.get(status='cleared')
            ses.save()
            serializer = MarketSessionSerializer(ses)
            return Response(serializer.data)
        else:
            return Response(status=400, data={"details": "Invalid input. Provided session was not closed."})


class putCompletedSessions(APIView):
    """
    This interface updates the
    status of a session from "cleared" to "completed".
    """

    keycloak_scopes = {
        'PUT': [MARKET_OPERATOR_WRITE, MCM_WRITE, MBM_WRITE, MEMO_WRITE],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, SESSION_ID],
                         responses=putCompletedSessions_PUT,
                         operation_summary="Updates the status of a session from cleared to completed",
                         request_body=MarketSessionSerializer)
    def put(self, request, *args, **kwargs):
        try:
            ses = Marketplace.objects.get(pk=kwargs.get('pk')).marketsession_set.get(id=kwargs.get('sid'))
        except Exception as e:
            return Response(status=404)
        if ses.sessionStatusid == SessionStatus.objects.get(status='cleared'):
            ses.sessionStatusid = SessionStatus.objects.get(status='completed')
            ses.save()
            serializer = MarketSessionSerializer(ses)
            return Response(serializer.data)
        else:
            return Response(status=400, data={"details": "Invalid input. Provided session was not cleared."})


class putEndedSessions(APIView):
    """
    this interface updates the status of market sessions from "active" to
    "closed" and posts sorted actions to MSM.
    """

    keycloak_scopes = {
        'PUT': [MARKET_OPERATOR_WRITE, MCM_WRITE, MBM_WRITE, MEMO_WRITE],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, SESSION_ID],
                         responses=putCompletedSessions_PUT,
                         operation_summary="Updates the status of a session from active to closed",
                         request_body=MarketSessionSerializer)
    def put(self, request, *args, **kwargs):
        sorted_actions = []
        try:
            ses = Marketplace.objects.get(pk=kwargs.get('pk')).marketsession_set.get(id=kwargs.get('sid'))
        except:
            raise Http404
        if ses.sessionStatusid == SessionStatus.objects.get(status='active'):
            ses.sessionStatusid = SessionStatus.objects.get(status='closed')
            ses.save()
            serializer = MarketSessionSerializer(ses)
            # if Marketplace.objects.get(pk=kwargs.get('pk')).marketServiceTypeid.type != 'ancillary-services':
            #     sorted_actions = ActionsOfMarketSession.get(ActionsOfMarketSession, request, 'app:actionsOfSession',
            #                                                 pk=kwargs.get('pk'), sid=kwargs.get('sid'),
            #                                                 action_type='actions', action_status='sorted')
            #     if sorted_actions.status_code != 200:
            #         return Response(status=404, data={"details": "Actions failed to be sorted"})
            #     sortedActionsSerializer = MarketActionSerializer2(data=list(sorted_actions.data), many=True)
            #
            #     url = settings.MCM_URL + "marketplace/{}/form/{}/marketsessions/{}/actions/sorted/".format(
            #         kwargs.get('pk'),
            #         ses.formid.form, ses.id)
            #     if sortedActionsSerializer.is_valid():
            #         msm_response = postActions(list(sorted_actions.data), url)
            #         if msm_response.status_code in {404, 500}:
            #             log.info("Error invoking MSM service.")
            #             return Response(status=404, data={"details": "Error invoking MSM service"})
            #     else:
            #         log.info("Invalid format of sorted actions.")
            #         Response(status=400, data={"details": "Invalid format of sorted actions."})
            return Response(status=200, data=serializer.data)
        else:
            return Response(status=400, data={"details": "Invalid input. Provided session was not active."})


class postListActionsToClearing(APIView):
    """
    this interface accepts a list of MarketActions from the MCM,
    it sends it to the ITLB for the processing, obtains a list of MarketActionsCounterOffers from the ITLB which it saves in the DB
    and after that sends the list of MarketActions to the MCM through the interface postListActionsToBilling
    """
    keycloak_scopes = {
        'POST': [MARKET_OPERATOR_WRITE, MCM_WRITE, MBM_WRITE, MEMO_WRITE],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, SESSION_ID, FORM],
                         responses=postListActionToClearing_POST,
                         operation_summary="Takes the list of MarketActions from the MCM and sends them to ITLB for processing",
                         request_body=PrioritisedMarketActionSerializer)
    def post(self, request, *args, **kwargs):
        """
        Post list of MarketActions to send to ITLB
        """

        log.info("Beginning service postListActionsToClearing...\n \n")

        # obtain URL params
        form = kwargs.get('form')
        mplace_id = kwargs.get('pk')
        session_id = kwargs.get('sid')

        output = []

        try:
            log.info("Validating route parameters marketplace id: {}, session id: {}, form: {}....".format(
                int(kwargs.get('pk')), int(kwargs['sid']), kwargs.get('form')))

            # check existing marketplace by id
            marketplace = Filter.marketplaces(request=request, id=int(kwargs.get('pk')))
            if marketplace is None:
                return Response(status=404, data={"details": "Marketplace not found."})

            # check existing session by id
            ses = MarketSession.objects.filter(marketplaceid=marketplace, id=int(kwargs['sid']))
            if ses is None:
                return Response(status=404, data={"details": "Market session not found."})

            # check existing form by name
            fid = kwargs.get('form')
            if fid not in [x.form for x in Form.objects.all()]:
                return Response(status=404, data={"details": "Form not found."})

            log.info("Route parameters are valid")

            # check if the MarketActions sent by the MCM have the correct format
            data = request.data
            log.info("Actions received by MCM for clearing: {}".format(json.dumps(data, cls=DjangoJSONEncoder)))

            serializer = PrioritisedMarketActionSerializer(data=data, many=True)

            if serializer.is_valid():

                for prioritised_market_action in data:
                    action_status = ActionStatus.objects.get(pk=prioritised_market_action["marketaction"]["statusid"])
                    # send the action for clearing only if it is a valid action
                    print(action_status)
                    if action_status.status == 'valid':
                        prioritised_ma_clearing = {}

                        ma = get_object_or_404(MarketAction, pk=prioritised_market_action["marketaction"]["id"])

                        prioritised_ma_clearing["marketaction"] = MarketActionClearingSerializer(ma).data
                        prioritised_ma_clearing["priority"] = prioritised_market_action["priority"]

                        print("Prioritised ma: {}".format(prioritised_ma_clearing))

                        ser = PrioritisedMarketActionsClearingSerializer(data=prioritised_ma_clearing, many=False)
                        v = ser.is_valid()
                        er = ser.errors

                        print("Prioritised ma valid: {}".format(v))
                        print("Prioritised ma errors: {}".format(er))

                        data = ser.validated_data
                        print("Prioritised ma data: {}".format(data))

                        output.append(prioritised_ma_clearing)

                # send actions to ITLB for clearing
                log.info("Actions before sending to ITLB: {} ".format(json.dumps(output, cls=DjangoJSONEncoder)))

                itlb_url = settings.ITLB_URL + 'v2/clearingRequest/'
                itlb_market_actions_counteroffers_response = postActionsWithTimeout(output, itlb_url, 300)

                log.info("ITLB clearing request status code: {} ".format(
                    itlb_market_actions_counteroffers_response.status_code))

                log.info("ITLB clearing request data response: {} ".format(
                    itlb_market_actions_counteroffers_response.data))


                if itlb_market_actions_counteroffers_response.status_code in {400, 404, 500}:
                    log.info("Error invoking ITLB clearing service status code: {} ".format(
                        itlb_market_actions_counteroffers_response.status_code))
                    return Response(status=404, data={
                        "details": "Error invoking ITLB clearing service, status code: {}".format(
                            itlb_market_actions_counteroffers_response.status_code)})
                else:
                    itlb_clearing_result = itlb_market_actions_counteroffers_response.data
                    #itlb_clearing_result = json.loads(itlb_clearing_result)

                    actions_and_counteroffers_ser = \
                        MarketActionCounterOfferSerializerClearingResult(data=itlb_clearing_result)

                    if (actions_and_counteroffers_ser.is_valid()):
                        log.warning("Received clearing response from ITLB is valid")
                        log.warning("Begin service to persist counteroffers in the DB")

                        # request_n = request.copy()
                        # request_n.data = json.dumps(itlb_clearing_result, cls=DjangoJSONEncoder)
                        # cleared_actions_response = MarketActionClearingCounterOffers.post(MarketActionClearingCounterOffers, request_n,
                        #

                        cleared_actions_response = persist_counteroffers_return_cleared_actions(data=json.dumps(itlb_clearing_result, cls=DjangoJSONEncoder))

                        if cleared_actions_response.status_code != 201:
                            return Response(status=404, data={
                                "details": "Unable to save counteroffers received from ITLB"})
                        else:
                            log.warning("Successfully persisted counteroffers db")
                            log.warning("Returning cleared actions to MCM")

                            cleared_actions_data = json.loads(cleared_actions_response.data)
                            cleared_actions_serializer = MarketActionSerializer2(data=cleared_actions_data, many=True)

                            if(cleared_actions_serializer.is_valid()):
                                log.warning("Actions cleared have correct format. Returning actions to MCM...")
                                output_list = []
                                for clearead_action in cleared_actions_data:
                                    ma = get_object_or_404(MarketAction, pk=clearead_action["id"])
                                    output_list.append(MarketActionSerializer2(ma).data)
                                return Response(output_list, status=200)
                            else:
                                return Response(status=404, data={
                                    "details": "Cleared actions have malformed format. Unable to return them to MCM!"})


                    else:
                        log.warning("Error invoking ITLB clearing service, invalid data format of the response")
                        return Response(status=404, data={
                            "details": "Error invoking ITLB clearing service, invalid data format of the response"})

        except Exception as e:
            print(traceback.format_exc())
            log.warning("An error occured while executing postListActionsToClearing for {}, {}: {} ".format(mplace_id,
                                                                                                            session_id,
                                                                                                            e))
            return Response(status=404)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



class getBids(APIView):
    """
    Through this interface, the bids for a specific market session are
    provided.
    """

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, MCM_READ, MBM_READ, MEMO_READ, MARKET_PARTICIPANT_READ, DSO_READ],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, SESSION_ID],
                         responses=getOffers_GET,
                         operation_summary="Get Bids for a specific session")
    def get(self, request, *args, **kwargs):
        try:
            marketplace = Filter.marketplaces(request=request, id=int(kwargs.get('pk')))
            if marketplace is None:
                return Response(status=404)
            ses = MarketSession.objects.filter(marketplaceid=marketplace, id=int(kwargs['sid']))
            if ses is None:
                return Response(status=404)
            sel_actions = Filter.market_actions_of_session(request=request, market_session=ses).filter(
                actionTypeid__type="bid")
        except Exception as e:
            log.warning("An error occured while fetching actions for {}, {}: {}".format(marketplace, ses, e))
            return Response(status=404)
        serializer = MarketActionSerializer(sel_actions, many=True)
        return Response(serializer.data)


class getOffers(APIView):
    """
    Through this interface, the offers for a specific market session are
    provided.
    """

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, MCM_READ, MBM_READ, MEMO_READ, MARKET_PARTICIPANT_READ, DSO_READ],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, SESSION_ID],
                         responses=getOffers_GET,
                         operation_summary="Get Offers for a specific session")
    def get(self, request, *args, **kwargs):
        try:
            marketplace = Filter.marketplaces(request=request, id=int(kwargs.get('pk')))
            if marketplace is None:
                return Response(status=404)
            ses = MarketSession.objects.filter(marketplaceid=marketplace, id=int(kwargs['sid']))
            if ses is None:
                return Response(status=404)
            sel_actions = Filter.market_actions_of_session(request=request, market_session=ses).filter(
                actionTypeid__type="offer")
        except Exception as e:
            log.warning("An error occured while fetching actions for {}, {}: {}".format(marketplace, ses, e))
            return Response(status=404)
        serializer = MarketActionSerializer(sel_actions, many=True)
        return Response(serializer.data)


class postLoad(mixins.ListModelMixin,
               mixins.CreateModelMixin,
               generics.GenericAPIView):
    queryset = Load.objects.all()
    serializer_class = LoadSerializer1

    keycloak_scopes = {
        'POST': [MARKET_OPERATOR_WRITE, MCM_WRITE, MBM_WRITE, MEMO_WRITE, MARKET_PARTICIPANT_WRITE, DSO_WRITE],
    }

    @swagger_auto_schema(manual_parameters=[],
                         responses=Load_POST,
                         operation_summary="Create a new Load",
                         request_body=LoadSerializer1)
    def post(self, request, *args, **kwargs):
        """
        Post a new load object
        """
        data = request.data
        serializer = LoadSerializer1(data=data)

        if serializer.is_valid():
            load = Load(date=data['date'])
            load.save()

            serializer_return = LoadSerializer(load, many=False)

            return Response(serializer_return.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class getLoadValuesByLoad(mixins.ListModelMixin,
                          mixins.CreateModelMixin,
                          generics.GenericAPIView):
    queryset = LoadValue.objects.all()
    serializer_class = LoadValueSerializer1

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, MCM_READ, MBM_READ, MEMO_READ, MARKET_PARTICIPANT_READ, DSO_READ],
    }

    @swagger_auto_schema(manual_parameters=[LOAD_ID],
                         responses=LoadValue_GET,
                         operation_summary="Get all LoadValues for a specific Load")
    def get(self, request, *args, **kwargs):
        """
        Get the list of load values for a specific load.
        """
        try:
            load = Filter.loads(request=request, id=int(kwargs.get('lid')))

            if load is None:
                return Response(status=404)

            load_values = LoadValue.objects.filter(loadid=load)

            if load_values is None:
                return Response(status=404)

        except Exception as e:
            log.warning("An error occured while fetching load values for {}: {}".format(load, e))
            return Response(status=404)
        serializer = LoadValueSerializer(load_values, many=True)

        return Response(serializer.data)


class postLoadValues(mixins.ListModelMixin,
                     mixins.CreateModelMixin,
                     generics.GenericAPIView):
    queryset = LoadValue.objects.all()
    serializer_class = LoadValueSerializer1

    keycloak_scopes = {
        'POST': [MARKET_OPERATOR_WRITE, MCM_WRITE, MBM_WRITE, MEMO_WRITE, MARKET_PARTICIPANT_WRITE, DSO_WRITE],
    }

    @swagger_auto_schema(manual_parameters=[],
                         responses=LoadValue_POST,
                         request_body=LoadValueSerializer1,
                         operation_summary="Post a new load value for a load")
    def post(self, request, *args, **kwargs):
        """
        Post a new Load Value object or list of Load Values objects
        """
        data = request.data

        is_many = isinstance(data, list)

        """
        Case post is a single item

        """
        if not is_many:

            serializer = LoadValueSerializer1(data=data)

            if serializer.is_valid():
                load = get_object_or_404(Load, pk=data['loadid'])

                loadValue = LoadValue(loadid=load,
                                      parameter=data['parameter'],
                                      uom=data['uom'],
                                      value=data['value'])
                loadValue.save()

                serializer_return = LoadValueSerializer(loadValue, many=False)

                return Response(serializer_return.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:

            loadValueList = []

            serializer = LoadValueSerializer1(data=data, many=True)

            if serializer.is_valid():

                for d in data:
                    load = get_object_or_404(Load, pk=d['loadid'])

                    loadValue = LoadValue(loadid=load,
                                          parameter=d['parameter'],
                                          uom=d['uom'],
                                          value=d['value'])

                    loadValue.save()

                    loadValueList.append(loadValue)

                serializer_return = LoadValueSerializer(loadValueList, many=True)
                return Response(serializer_return.data, status=status.HTTP_201_CREATED)

            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


###################################### Used for UPDATE / WITHDRAW in the AM interface ######################################
class getActionById(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, DSO_READ, MARKET_PARTICIPANT_READ, MEMO_READ, MCM_READ, MBM_READ],
    }

    @swagger_auto_schema(manual_parameters=[ACTION_ID], responses=getActionById_GET,
                         operation_summary="Get action by ID")
    def get(self, request, *args, **kwargs):
        try:
            action = MarketAction.objects.get(pk=kwargs.get('action_id'))
        except Exception as e:
            log.warning("Action not found. ", e)
            return Response(status=404, data={"details": str(e)})
        serializer = MarketActionSerializer(action)
        return Response(serializer.data)


class MarketActions(mixins.ListModelMixin,
                    mixins.CreateModelMixin,
                    generics.GenericAPIView):
    queryset = MarketAction.objects.all()
    serializer_class = MarketActionSerializer

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, DSO_READ, MARKET_PARTICIPANT_READ, MEMO_READ, MCM_READ, MBM_READ],
        'POST': [MARKET_OPERATOR_WRITE, DSO_WRITE, MARKET_PARTICIPANT_WRITE, MEMO_WRITE, MCM_WRITE, MBM_WRITE],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, SESSION_ID],
                         responses=MarketActions_GET,
                         operation_summary="Get all the actions (Bids & offers) for a specific market session")
    def get(self, request, *args, **kwargs):
        """
        Get the list of market actions for a specific marketplace
        """
        try:
            marketplace = Filter.marketplaces(request=request, id=int(kwargs.get('pk')))
            if marketplace is None:
                return Response(status=404)
            ses = MarketSession.objects.filter(marketplaceid=marketplace, id=int(kwargs['sid']))
            if ses is None:
                return Response(status=404)
            sel_actions = Filter.market_actions_of_session(request=request, market_session=ses)


        except Exception as e:
            log.warning("An error occured while fetching actions for {}, {}: {}".format(marketplace, ses, e))
            return Response(status=404)

        serializer = MarketActionSerializer(sel_actions, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, SESSION_ID], responses=MarketActions_POST,
                         operation_summary="Create a new action to a specific Marketplace",
                         request_body=MarketActionSerializer1)
    def post(self, request, *args, **kwargs):
        """
        Post a new Action object to a specific MarketPlace
        """
        log.warning("Start service for postintg MarketAction")
        possible_status = ['active', 'cleared', 'completed']
        forbidden_status = ['closed']

        data = request.data
        user = get_user_from_request(request)

        log.warning("MarketAction received for POST : {}".format(data))
        serializer = MarketActionSerializer1(data=data, many=True)
        output = []

        if serializer.is_valid():
            # Some extra checks
            log.warning("Verifying marketplace with id: {}".format(kwargs.get('pk')))
            marketplace = Filter.marketplaces(request=request, id=int(kwargs.get('pk')))
            market_session = None
            if marketplace is None:
                log.warning("Marketplace with id: {} does not exist for user {}".format(kwargs.get('pk'), user))
                return Response(status=404)
            try:
                log.warning("Verifying market session with id: {}".format(kwargs.get('sid')))
                market_session = MarketSession.objects.get(marketplaceid=marketplace, id=int(kwargs['sid']))
                if market_session is None:
                    log.warning("Market session with id: {} does not exist for user {}".format(kwargs.get('sid'), user))
                    return Response(status=404)
            except Exception as e:
                log.warning("Could not find market session {} for user {}".format(kwargs['sid'], user))
                return Response(status=404)

            market_session_status = market_session.sessionStatusid.status
            if market_session_status not in possible_status:
                log.warning("Market session status {} does not belong to the list of possible statuses {}"
                            .format(market_session_status, possible_status))
                return Response(status=404)

            if market_session_status in forbidden_status:
                log.warning("Market session status {} belongs to the list of forbidden statuses {}"
                            .format(market_session_status, forbidden_status))
                return Response(status=404)

            actions_to_be_validated = []

            for act in data:
                log.warning("Creating action from data: {}".format(act))
                log.warning("Retrieving action status with id : {}".format(act['statusid']))
                set_action_status = get_object_or_404(ActionStatus, pk=act['statusid'])
                log.warning("Retrieving form with id : {}".format(act['formid']))
                form = get_object_or_404(Form, pk=act['formid'])
                log.warning("Retrieving action type with id : {}".format(act['actionTypeid']))
                action_type = get_object_or_404(ActionType, pk=act['actionTypeid'])
                log.warning("Retrieving market actor with id : {}".format(act['marketActorid']))
                actor = get_object_or_404(MarketActor, pk=act['marketActorid'])
                log.warning("Retrieving market session with id : {}".format(act['marketSessionid']))
                market_session = get_object_or_404(MarketSession, pk=act['marketSessionid'])

                if 'loadid' in act and act['loadid'] is not None:
                    log.warning("Action belongs to IT Load marketplace. Retriving load with id: {}".format(act['loadid']))
                    load = get_object_or_404(Load, pk=act['loadid'])
                else:
                    load = None

                if 'deliveryPoint' in act and act['deliveryPoint'] is not None:
                    deliveryPoint = act['deliveryPoint']
                else:
                    deliveryPoint = None

                # bids posted by the DSO should keep the status set by the dso
                # while the other market actions should start with unchecked status
                # and be validated
                if actor.marketActorTypeid.type == "dso" and action_type.type == "bid":
                    action_status = set_action_status
                else:
                    action_status = ActionStatus.objects.get(status="unchecked")

                log.warning("Saving created action to database...")
                action = MarketAction(date=act['date'],
                                      actionStartTime=act['actionStartTime'],
                                      actionEndTime=act['actionEndTime'],
                                      value=act['value'],
                                      uom=act['uom'],
                                      price=act['price'],
                                      deliveryPoint=deliveryPoint,
                                      marketSessionid=market_session,
                                      marketActorid=actor,
                                      formid=form,
                                      actionTypeid=action_type,
                                      statusid=action_status,
                                      loadid=load)
                action.save()
                if action.id is None:
                    log.warning("Error in saving action to database")
                else:
                    log.warning("Succesfully saved action to database!")


                # dso bids do not need to be validated as they are automatically generated
                # by a market clearing algorithm
                if actor.marketActorTypeid.type == "dso" and action_type.type == "bid":
                    pass
                else:
                    actions_to_be_validated.append(MarketAction.objects.get(pk=action.id))

                output.append(MarketActionSerializer2(action).data)

            log.warning("Performing validity check for actions: {}".format(actions_to_be_validated))
            action_validity_check.check_actions(marketplace=marketplace, market_session=market_session, market_actions=actions_to_be_validated)

            validated_output = []

            for ma in output:
                validated_ma = MarketAction.objects.get(pk=ma['id'])
                validated_output.append(MarketActionSerializer2(validated_ma).data)

            return Response(validated_output, status=status.HTTP_201_CREATED)

        log.warning("Serializer errors: {}".format(serializer.errors))
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class getMarketActionsSimpleObjects(mixins.ListModelMixin,
                    mixins.CreateModelMixin,
                    generics.GenericAPIView):
    queryset = MarketAction.objects.all()
    serializer_class = MarketActionSerializer

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, DSO_READ, MARKET_PARTICIPANT_READ, MEMO_READ, MCM_READ, MBM_READ],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, SESSION_ID],
                         responses=MarketActions_GET,
                         operation_summary="Get all the actions (Bids & offers) for a specific market session")
    def get(self, request, *args, **kwargs):
        """
        Get the list of market actions for a specific marketplace
        """
        try:
            marketplace = Filter.marketplaces(request=request, id=int(kwargs.get('pk')))
            if marketplace is None:
                return Response(status=404)
            ses = MarketSession.objects.filter(marketplaceid=marketplace, id=int(kwargs['sid']))
            if ses is None:
                return Response(status=404)
            sel_actions = Filter.market_actions_of_session(request=request, market_session=ses)


        except Exception as e:
            log.warning("An error occured while fetching actions for {}, {}: {}".format(marketplace, ses, e))
            return Response(status=404)

        serializer = MarketActionSerializer2(sel_actions, many=True)
        return Response(serializer.data)


class MarketActionById(APIView):
    queryset = MarketAction.objects.all()
    serializer_class = MarketActionSerializer2

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, DSO_READ, MARKET_PARTICIPANT_READ, MEMO_READ, MCM_READ, MBM_READ],
    }

    @swagger_auto_schema(manual_parameters=[ACTION_ID],
                         responses=MarketAction_GET,
                         operation_summary="Get a specific action (Bid or offer) by ID")
    def get(self, request, *args, **kwargs):
        """
        Get a specific action (Bid or offer) by ID.
        """
        try:
            action = Filter.marketaction(request=request, action_id=int(kwargs.get('action_id')))
            if action is None:
                return Response(status=404)
        except Exception as e:
            log.warning("An error occured while fetching action {}: {}".format(kwargs.get('action_id'), e))
            return Response(status=404)

        serializer = MarketActionSerializer2(action)
        return Response(serializer.data, status=status.HTTP_200_OK)


class getTransactions(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, DSO_READ, MARKET_PARTICIPANT_READ, MEMO_READ, MCM_READ, MBM_READ],
        'POST': [MARKET_OPERATOR_WRITE, MEMO_WRITE, MCM_WRITE, MBM_WRITE],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, SESSION_ID],
                         responses=getTransactions_GET,
                         operation_summary="Get the list of transactions during a specific market session")
    def get(self, request, *args, **kwargs):
        """
        Get the list of transactions during a specific market session
        """
        user = get_user_from_request(request)

        try:
            marketplace = Filter.marketplaces(request=request, id=int(kwargs.get('pk')))
            if marketplace is None:
                return Response(status=404)
            market_session = MarketSession.objects.get(marketplaceid=marketplace, id=int(kwargs['sid']))
            if market_session is None or market_session.marketplaceid != marketplace:
                return Response(status=404)
        except Exception as e:
            log.warning(
                "Could not find market session {} for user {} or in marketplace {}".format(kwargs['sid'], user,
                                                                                           marketplace.id))
            return Response(status=404)

        ses_count_off = MarketActionCounterOffer.objects.select_related('marketAction_Bid_id__marketSessionid').filter(
            marketAction_Bid_id__marketSessionid=market_session)
        # ses_count_off = MarketActionCounterOffer.objects.filter(marketAction_Bid_id__marketSessionid=market_session)
        transactions = Transaction.objects.filter(marketActionCounterOfferid__in=ses_count_off)
        serializer = TransactionSerializer1(transactions, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, SESSION_ID], responses=getTransactions_POST,
                         operation_summary="Create transaction during a specific marketsession",
                         request_body=TransactionSerializer1)
    def post(self, request, *args, **kwargs):
        """Post a transaction during a specific market session"""
        user = get_user_from_request(request)

        data = request.data
        is_many = isinstance(data, list)
        output_list = []

        try:
            marketplace = Filter.marketplaces(request=request, id=int(kwargs.get('pk')))
            if marketplace is None:
                return Response(status=404)
            market_session = MarketSession.objects.get(marketplaceid=marketplace, id=int(kwargs['sid']))
            if market_session is None or market_session.marketplaceid != marketplace:
                return Response(status=404)
        except Exception as e:
            log.warning(
                "Could not find market session {} for user {} or in marketplace {}".format(kwargs['sid'], user,
                                                                                           marketplace.id))
            return Response(status=404)

        if not is_many:
            serializer = TransactionSerializer1(data=data)
            if serializer.is_valid():
                try:
                    counterOffer = MarketActionCounterOffer.objects.get(pk=data['marketActionCounterOfferid'])
                except Exception as e:
                    log.warning("Could not find counteroffer {}: {}".format(data['marketActionCounterOfferid'], e))
                    return Response(status=HTTP_404_NOT_FOUND)
                transact = Transaction.objects.create(dateTime=data['dateTime'],
                                                      marketActionCounterOfferid=counterOffer)
                output = TransactionSerializer1(transact)
                return Response(output.data, status=201)
            return Response(serializer.errors, status=400)
        else:
            serializer = TransactionSerializer1(data=data, many=True)
            if serializer.is_valid():
                for transaction in data:
                    try:
                        counterOffer = MarketActionCounterOffer.objects.get(
                            pk=transaction['marketActionCounterOfferid'])
                    except Exception as e:
                        log.warning(
                            "Could not find counteroffer {}: {}".format(transaction['marketActionCounterOfferid'], e))
                        return Response(status=HTTP_404_NOT_FOUND)
                    transact = Transaction.objects.create(dateTime=transaction['dateTime'],
                                                          marketActionCounterOfferid=counterOffer)
                    output_list.append(TransactionSerializer1(transact).data)
                return Response(output_list, status=201)
            return Response(status=400)
        return Response(serializer.errors, status=400)


class Invoices(mixins.ListModelMixin,
               mixins.CreateModelMixin,
               generics.GenericAPIView):
    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, KNOWN_MARKET_ACTOR_READ, MEMO_READ, MCM_READ, MBM_READ],
        'POST': [MARKET_OPERATOR_WRITE, MEMO_WRITE, MCM_WRITE, MBM_WRITE],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, SESSION_ID], responses=Invoices_GET,
                         operation_summary="Get all invoices")
    def get(self, request, *args, **kwargs):
        """
        Gets the invoices
        """
        try:
            if 'pk' in kwargs and 'sid' in kwargs:
                invoices = Filter.invoices(request=request, marketplace_id=int(kwargs['pk']),
                                           market_session_id=int(kwargs['sid']))
                if invoices is None:
                    return Response(status=200, data=[])

                serializer = InvoiceSerializer(invoices, many=True)
                return Response(status=200, data=serializer.data)
            else:
                invoices = Filter.invoices(request=request)
                if invoices is None:
                    return Response(status=200, data=[])

                serializer = InvoiceSerializer(invoices, many=True)
                return Response(status=200, data=serializer.data)
        except Exception as e:
            return Response(status=404)

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, SESSION_ID], responses=postInvoices_POST,
                         operation_summary="Create invoices", request_body=InvoiceSerializer)
    def post(self, request, *args, **kwargs):
        """Interface for MSM to post invoices to IB"""
        data = request.data
        user = get_user_from_request(request)
        output_list = []

        try:
            if 'pk' in kwargs and 'sid' in kwargs:
                mplace = Filter.marketplaces(request=request, id=int(kwargs['pk']))
                if mplace is None:
                    return Response(status=404)
                ses = MarketSession.objects.get(marketplaceid=mplace, id=int(kwargs['sid']))
                if ses is None or ses.marketplaceid != mplace:
                    return Response(status=404)
        except Exception as e:
            log.warning(
                "Could not find market session {} for user {} or in marketplace {}".format(kwargs['sid'], user,
                                                                                           mplace.id))
            return Response(status=404)

        serializer = InvoiceSerializer(data=data, many=True)
        if serializer.is_valid():
            try:
                for invoice in data:
                    inv = Invoice(date=invoice['date'],
                                  transactionid=Transaction.objects.get(pk=invoice['transactionid']),
                                  penalty=invoice['penalty'],
                                  fixedFee=invoice['fixedFee'],
                                  amount=invoice['amount'])
                    if 'pk' in kwargs and 'sid' in kwargs and Transaction.objects.get(id=invoice[
                        'transactionid']).marketActionCounterOfferid.marketAction_Bid_id.marketSessionid.id != int(
                        kwargs['sid']):
                        return Response(status=400,
                                        data={"details": "Provided invoice does not belong to the provided session."})
                    inv.save()
                    output_list.append(InvoiceSerializer(inv).data)
                return Response(output_list, status=status.HTTP_201_CREATED)
            except Exception as e:
                log.warning("Could not create invoice {}: {}".format(invoice, e))
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class FindMarketActorInMarketplace(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, MEMO_READ, MCM_READ, MBM_READ],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, ACTOR_ID],
                         responses=ISPARTICIPANTOFMARKET,
                         operation_summary="True if actor participates in the given marketplace")
    def get(self, request, *args, **kwargs):
        """This interface returns "true" if the Marketplace Participant posting the market action is registered in the
        specific marketplace, "false" otherwise"""

        try:
            mplace = Filter.marketplaces(request=request, id=kwargs.get('pk'))
            if mplace is None:
                return Response(status=404, data={"details": "Marketplace not found"})
            actor = MarketActor.objects.get(id=kwargs.get('actor'))
            find_actor_in_market = Marketplace_has_MarketActor.objects.filter(marketplace=mplace, marketActor=actor)
        except Exception as e:
            log.warning(e)
            return Response(status=404, data={"details": str(e)})
        if len(find_actor_in_market) > 0:
            return Response(json.dumps(True))
        return Response(json.dumps(False))


class AllActive(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, MARKET_PARTICIPANT_READ, DSO_READ],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, BIDS_PARAMETER],
                         responses=MarketActions_GET,
                         operation_summary="Get active offers or active bids or both of them")
    def get(self, request, *args, **kwargs):
        """
        Set type as offers to get the active offers of an active market session
        Set type as bids to get the active bids of an active market session
        Set type as actions to get the active actions (offers + bids) of an active market session

        type: [offers, bids, actions]
        """
        param = kwargs.get('type')
        action_types = {'offers': 'offer', 'bids': 'bid'}
        serializer = []
        active_sessions = Marketplace.objects.get(pk=kwargs.get('pk')).marketsession_set.filter(
            sessionStatusid__status='active')
        for ses in active_sessions:
            actionsofses = Filter.market_actions_of_session(request=request, market_session=ses)
            if param in action_types.keys():
                tmp = [action for action in actionsofses if
                       action.statusid.status == 'active' and action.actionTypeid.type == action_types[param]]
            elif param == 'actions':
                tmp = [action for action in actionsofses if action.statusid.status == 'active']
            else:
                return Response(status=404)
            for tmp_action in tmp:
                serializer.append(MarketActionSerializer(tmp_action).data)
        return Response(serializer)


class ActionsOfMarketSession(mixins.ListModelMixin,
                             mixins.UpdateModelMixin,
                             generics.GenericAPIView):
    queryset = MarketSession.objects.all()
    serializer_class = MarketActionSerializer

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, MARKET_PARTICIPANT_READ, DSO_READ, MEMO_READ, MCM_READ, MBM_READ],
        'PUT': [MARKET_OPERATOR_WRITE, DSO_WRITE, MEMO_WRITE, MCM_WRITE, MBM_WRITE],
        'POST': [MARKET_OPERATOR_WRITE, DSO_WRITE, MEMO_WRITE, MCM_WRITE, MBM_WRITE],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, SESSION_ID, ActionsOfMarketSession_ACTION_TYPE,
                                            ActionsOfMarketSession_ACTION_STATUS],
                         responses=ActionsOfMarketSession_GET,
                         operation_summary="Get active/sorted/delivered offers/bids/actions")
    def get(self, request, *args, **kwargs):
        """
        Get (active or sorted or delivered) actions/bids/offers

        action_type: [offers, bids, actions]
        action_status: [active, sorted, delivered]

        Also from this endpoint cleared actions can be posted.
        As 'cleared' actions [accepted, partially accepted, rejected] actions are defined
        """
        user = get_user_from_request(request)

        try:
            log.warning("Entering service ActionsOfMarketSessio.GET")
            log.warning("Verify existance of market session with id: {}".format(kwargs['sid']))
            market_session = Filter.marketsessions(request=request, marketsession_id=kwargs['sid'])
            if market_session is None:
                log.warning("Market session with id: {} doesn't exist for user: {}".
                            format(kwargs['sid'], user))
                return Response(status=403)

            param = kwargs.get('action_type')
            param2 = kwargs.get('action_status')
            log.warning("Action type: {}".format(param))
            log.warning("Action status: {}".format(param2))

            types = {'offers': 'offer', 'bids': 'bid'}
            if param in types.keys() and param2 == 'active':
                log.warning("Retrieving {} {}...".format(param2, param))
                active_types = market_session.marketaction_set.filter(
                    statusid__status=param2,
                    actionTypeid__type=
                    types[param])
                serializer = MarketActionSerializer(active_types, many=True)
                log.warning("Return: {}".format(serializer.data))
                return Response(serializer.data)
            elif param == 'actions':
                if param2 == 'active':
                    log.warning("Retrieving {} {}...".format(param2, param))
                    active_actions = Filter.market_actions_of_session(request=request,
                                                                      market_session=market_session).filter(
                        statusid__status=param2)
                    serializer = MarketActionSerializer(active_actions, many=True)
                    log.warning("Return: {}".format(serializer.data))
                    return Response(serializer.data)
                elif param2 == 'sorted':
                    log.warning("Retrieving {} {}...".format(param2, param))
                    sorted_offers = Filter.market_actions_of_session(request=request,
                                                                     market_session=market_session).filter(
                        actionTypeid__type=types['offers']).order_by('price')
                    sorted_bids = Filter.market_actions_of_session(request=request,
                                                                   market_session=market_session).filter(
                        actionTypeid__type=types['bids']).order_by('price')
                    result_list = list(chain(sorted_offers, sorted_bids))
                    serializer = MarketActionSerializer2(result_list, many=True)
                    log.warning("Return: {}".format(serializer.data))
                    return Response(serializer.data)
                elif param2 in {'delivered', 'valid'}:
                    log.warning("Retrieving {} {}...".format(param2, param))
                    res = Filter.market_actions_of_session(request=request, market_session=market_session).filter(
                        statusid__status=param2)
                    serializer = MarketActionSerializer2(res, many=True)
                    log.warning("Return: {}".format(serializer.data))
                    return Response(serializer.data)
                else:
                    return Response(status=404, data={"details": "No action status provided."})
            else:
                return Response(status=404, data={"details": "No action type provided."})
        except Exception as e:
            log.warning(e)
            return Response(status=404, data={"details": "Invalid."})

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, SESSION_ID, ActionsOfMarketSession_PUT_TYPE,
                                            ActionsOfMarketSession_PUT_STATUS],
                         responses=ActionsOfMarketSession_PUT,
                         operation_summary="Update requested action status to checked",
                         request_body=MarketActionSerializer)
    def put(self, request, *args, **kwargs):
        """
        Update actions of a specific market session to checked (valid or invalid)

        action_status: checked
        action_type: actions
        """
        log.warning("Entering service ActionsOfMarketSession: PUT")

        log.warning("Check existence of market session with id: {}".format(kwargs['sid']))
        market_session = Filter.marketsessions(request=request, marketsession_id=kwargs['sid'])
        if market_session is None:
            return Response(status=403)

        log.warning("Market Session with id: {} found".format(kwargs['sid']))

        data = json.loads(request.body)
        ret = []

        log.warning("Payload received: {}".format(data))
        for item in data:
            try:
                _mas = MarketActionSerializer2(data=item)
                _mas.is_valid(raise_exception=True)
                vd = _mas.validated_data
                existing_action = MarketAction.objects.get(id=item['id'])
                existing_action.statusid = vd['statusid']
                existing_action.save()
                log.warning("Saving updated action : {}".format(existing_action))
                ret.append(MarketActionSerializer2(existing_action).data)
            except Exception as e:
                log.error("An error occurred: ", e)
                return Response(status=400, data={"details": str(e)})
        return Response(ret)

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, SESSION_ID, ActionsOfMarketSession_POST_TYPE,
                                            ActionsOfMarketSession_POST_STATUS],
                         responses=ActionsOfMarketSession_POST,
                         operation_summary="Cleared ('accepted', 'partially_accepted', 'rejected) actions can be posted,",
                         request_body=MarketActionSerializer)
    def post(self, request, *args, **kwargs):
        """Through this interface cleared ('accepted', 'partially_accepted', 'rejected) actions can be posted,
        to be updated in the data storage
        <action_type>: 'clearing'
        <action_status>: 'actions

        """
        ses = get_object_or_404(MarketSession, pk=kwargs.get('sid'))
        param = kwargs.get('action_type')
        param2 = kwargs.get('action_status')
        possible_status = ['accepted', 'partially_accepted', 'rejected']

        ret = []
        market_sessions = None

        if param != 'clearing' or param2 != 'actions':
            log.info("Action_type {}, status {}".format(param, param2))
            return Response(status=405, data={"details": "Method not allowed on this url."})
        else:
            try:
                marketplace = Filter.marketplaces(request=request, id=kwargs.get('pk'))
                if marketplace is None:
                    return Response(status=404)
                market_sessions = MarketSession.objects.filter(marketplaceid__id=marketplace.id)
                if ses not in market_sessions:
                    return Response(status=404)
            except Exception as e:
                log.info(e)
                return Response(status=404)
            try:
                all_updated_objects = {}
                for item in request.data:
                    log.info(item)
                    _mas = MarketActionSerializer2(data=item)
                    try:
                        _mas.is_valid(raise_exception=True)
                    except Exception as e:
                        log.warning(e)
                        return Response(status=400,
                                        data={"details": "Invalid input: noticed an action of invalid format."})
                    try:
                        vd = _mas.validated_data
                        if vd['marketSessionid'].id != ses.id:
                            log.warning(
                                "Rejecting clearing of action {} since it does not belong to the provided market session.".format(
                                    _mas))
                            return Response(status=400, data={
                                "details": "Rejecting clearing of action {} since it does not belong to the provided market session.".format(
                                    _mas)})
                        action_id = item['id']
                        existing_action = get_object_or_404(MarketAction, pk=action_id)
                        if vd['statusid'].status not in possible_status:
                            log.warning(
                                "Rejecting clearing of action {} since its status is not inactive or the provided status is not supported.".format(
                                    _mas))
                            return Response(status=400, data={
                                "details": "Rejecting clearing of action {} since its status is not inactive or the provided status is not supported.".format(
                                    _mas)})
                        existing_action.statusid = ActionStatus.objects.get(id=vd['statusid'].id)
                        all_updated_objects[action_id] = {}
                        all_updated_objects[action_id] = existing_action
                        ret.append(existing_action)
                    except Exception as e:
                        log.error(e)
                        return Response(status=400, data={"details": "Invalid input"})
                for action_id in all_updated_objects:
                    all_updated_objects[action_id].save()
                return Response(MarketActionSerializer(ret, many=True).data)
            except Exception as e:
                log.warning(e)
                return Response(status=400)


class SelectedActionsOfGamSession(APIView):

    keycloak_scopes = {
        'PUT': [MARKET_OPERATOR_WRITE, DSO_WRITE, MEMO_WRITE, MCM_WRITE, MBM_WRITE],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, SESSION_ID, ACTION_ID],
                         responses=SelectedActionsOfGamSession_PUT,
                         operation_summary="Update an offer status to selected",
                         request_body=MarketActionSerializer1)
    def put(self, request, *args, **kwargs):
        """Through this interface, IB is notified to update valid flexibility offers as selected"""
        data = request.data

        if 'pk' not in kwargs and 'sid' not in kwargs and 'action_id' not in kwargs:
            return Response(status=status.HTTP_404_NOT_FOUND,
                            data={"details": "Marketplace or session ID not provided"})
        try:
            mplace = Filter.marketplaces(request=request, id=kwargs['pk'])
            ses = Filter.marketsessions(request=request, marketsession_id=kwargs['sid'])
            if ses.marketplaceid != mplace:
                return Response(status=status.HTTP_404_NOT_FOUND,
                                data={"details": "Session ID doe not match Marketplace ID."})
        except Exception as e:
            log.warning(e)
            return Response(status=status.HTTP_404_NOT_FOUND, data={"details": str(e)})

        try:
            action = Filter.marketaction(request=request, action_id=int(kwargs.get('action_id')))
        except Exception as e:
            log.warning(e)
            return Response(status=404)
        if action.statusid == ActionStatus.objects.get(status='valid'):
            action.statusid = ActionStatus.objects.get(status='selected')
            action.save()
            serializer = MarketActionSerializer1(action)
            return Response(serializer.data)
        else:
            return Response(status=400, data={"details": "Invalid input. Provided action was not valid."})


class electricityPrice(mixins.ListModelMixin,
                       mixins.CreateModelMixin,
                       generics.GenericAPIView):
    serializer_class = ElectricityPriceSerializer

    @swagger_auto_schema(responses=electricityPrice_POST, operation_summary="Set electricity price",
                         request_body=ElectricityPriceSerializer)
    def post(self, request, *args, **kwargs):
        """Through this interface, the price of electricity will be posted by a third party"""
        data = request.data
        serializer = ElectricityPriceSerializer(data=data)
        if serializer.is_valid():
            new_price = ElectricityPrice(price=data['price'])
            new_price.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(responses=electricityPrice_GET, operation_summary="Get electricity price")
    def get(self, request, *args, **kwargs):
        """Through this interface, the price of electricity provided"""
        try:
            get_price = ElectricityPrice.objects.latest('id')
            serializer = ElectricityPriceSerializer(get_price)
            return Response(serializer.data)
        except Exception as e:
            log.warning(e)
            return Response(status=404)


class findLastMarketSessionClearingPrice(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, DSO_READ, MARKET_PARTICIPANT_READ,MEMO_READ, MCM_READ, MBM_READ],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, FORM], responses=clearingPrice_GET,
                         operation_summary="Get clearing price of last market session of given marketplace by its form")
    def get(self, request, *args, **kwargs):
        """This interface returns as a
        string the clearing price of the last session related to the same marketplace and
        the same form of energy or energy service"""
        fid = kwargs.get('form')
        if fid not in [x.form for x in Form.objects.all()]:
            return Response(status=404, data={"details": "Marketplace form not found."})

        try:
            ses = list(
                Marketplace.objects.get(pk=kwargs.get('pk')).marketsession_set.filter(formid__form=kwargs.get('form'),
                                                                                      sessionStatusid__status='closed').order_by(
                    'sessionEndTime'))
        except:
            raise Response(status.HTTP_404_NOT_FOUND)
        if ses != []:
            last_ses = ses[::-1]
            return Response({"LastClearingPrice": last_ses[0].clearingPrice})
        else:
            return Response({})


class NotBilledOffers(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, MEMO_READ, MCM_READ, MBM_READ],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID],
                         responses=NOTBILLEDOFFERS_GET,
                         operation_summary="Get notbilled offers")
    def get(self, request, *args, **kwargs):
        """
        Through this interface offers which are not billed yet can be retrieved
        """
        serializer = []
        try:
            today_ses = Marketplace.objects.get(pk=kwargs.get('pk')).marketsession_set.filter(
                deliveryEndTime__day=pytz.utc.localize(datetime.now()).day,
                deliveryEndTime__year=pytz.utc.localize(datetime.now()).year,
                deliveryEndTime__month=pytz.utc.localize(datetime.now()).month, sessionStatusid__status='completed')
        except Exception as e:
            return Response(status.HTTP_404_NOT_FOUND)
        try:
            session_actions = Filter.market_actions_of_session(request=request, market_session=today_ses)
            actions = [action for action in session_actions if action.statusid.status in ['delivered', 'not_delivered',
                                                                                          'valid'] and action.actionTypeid.type == "offer"]
            for act in actions:
                serializer.append(MarketActionSerializer(act).data)
            return Response(serializer)
        except Exception as e1:
            return Response(status.HTTP_404_NOT_FOUND, data={"details": str(e1)})


class NotBilledOffersOfFlex(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, MEMO_READ, MCM_READ, MBM_READ],
    }

    @swagger_auto_schema(responses=NOTBILLEDOFFERSFLEX_GET,
                         operation_summary="Get notbilled offers")
    def get(self, request, *args, **kwargs):
        """
        Through this interface not billed offers of the Flexibility Marketplace can be retrieved
        """
        serializer = []
        try:
            gams = Marketplace.objects.filter(
                marketServiceTypeid__in=MarketServiceType.objects.filter(type='ancillary-services'))
            today_sessions = MarketSession.objects.filter(marketplaceid__in=gams,
                                                          deliveryEndTime__day=pytz.utc.localize(datetime.now()).day,
                                                          deliveryEndTime__year=pytz.utc.localize(datetime.now()).year,
                                                          deliveryEndTime__month=pytz.utc.localize(
                                                              datetime.now()).month,
                                                          sessionStatusid__status='completed')
        except Exception as e:
            return Response(status.HTTP_404_NOT_FOUND, data={"details": str(e)})
        try:
            for today_ses in today_sessions:
                session_actions = Filter.market_actions_of_session(request=request, market_session=today_ses)
                actions = [action for action in session_actions if
                           action.statusid.status in ['delivered', 'not_delivered',
                                                      'valid', 'selected'] and action.actionTypeid.type == "offer"]
                for act in actions:
                    serializer.append(MarketActionSerializer2(act).data)
            log.warning("Sending flexibility offers for billing: {}".format(serializer))
            return Response(serializer)
        except Exception as e1:
            return Response(status.HTTP_404_NOT_FOUND, data={"details": str(e1)})


class BilledActions(APIView):

    keycloak_scopes = {
        'PUT': [MARKET_OPERATOR_WRITE, MEMO_WRITE, MCM_WRITE, MBM_WRITE],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID],
                         responses=NOTBILLEDOFFERS_GET,
                         operation_summary="Update action status as billed", request_body=MarketActionSerializer)
    def put(self, request, *args, **kwargs):
        """Through this interface action status is updated to billed """
        possible_status = ['cleared', 'completed']
        market_sessions = None
        try:
            marketplace = Filter.marketplaces(request=request, id=kwargs.get('pk'))
            if marketplace is None:
                return Response(status=404, data={"details": "Marketplace not found."})
            market_sessions = MarketSession.objects.filter(
                deliveryEndTime__day=pytz.utc.localize(datetime.now()).day,
                deliveryEndTime__year=pytz.utc.localize(datetime.now()).year,
                deliveryEndTime__month=pytz.utc.localize(datetime.now()).month,
                sessionStatusid__status__in=possible_status, marketplaceid=marketplace)
        except Exception as e:
            log.info(e)
            return Response(status=404, data={
                "details": "There no cleared or completed market sessions today or the given marketplace does not exist: {}".format(
                    e)})

        try:
            all_updated_objects = {}
            for action in request.data:
                serializer = MarketActionSerializer(data=action)
                try:
                    serializer.is_valid()
                except Exception as e:
                    log.warning(e)
                    return Response(status=400, data={"details": "Invalid input: noticed an action of invalid format"})
                try:
                    if action['marketSessionid'] not in [session.id for session in market_sessions]:
                        return Response(status=400, data={
                            "details": "Given actions do not belong in sessions of the given marketplace"})
                    vd = serializer.validated_data
                    action_id = action['id']
                    all_updated_objects[action_id] = {}
                    all_updated_objects[action_id] = MarketAction.objects.get(id=action_id)
                except Exception as e:
                    log.warning(e)
                    return Response(status=400, data={"details": "Invalid input"})
            for action_id in all_updated_objects:
                all_updated_objects[action_id].statusid = ActionStatus.objects.get(status="billed")
                all_updated_objects[action_id].save()
            return Response(status=204)
        except Exception as e:
            log.warning(e)
            return Response(status=400)


class MarketActionCounterOffers(mixins.ListModelMixin,
                                mixins.CreateModelMixin,
                                generics.GenericAPIView):
    queryset = MarketActionCounterOffer.objects.all()
    serializer_class = MarketActionCounterOfferSerializer

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, MEMO_READ, MCM_READ, MBM_READ],
        'POST': [MARKET_OPERATOR_WRITE, MEMO_WRITE, MCM_WRITE, MBM_WRITE],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID],
                         responses=MarketActionCounterOffers_GET,
                         operation_summary="Get counteroffers")
    def get(self, request, *args, **kwargs):
        """Get counter-actions"""

        log.info("Started service to return counteroffers... \n \n")
        mplace = Filter.marketplaces(request=request, id=kwargs['pk'])

        log.info("Validating router parameters marketplace id: {}...  \n \n ".format(kwargs['pk']))
        if mplace is None:
            return Response(status=404, data={"details": "Marketplace not found"})

        log.info("Marketplace with id: {} valid  \n \n".format(kwargs['pk']))
        counterOffers = MarketActionCounterOffer.objects.filter(
            marketAction_Bid_id__marketSessionid_id__marketplaceid=mplace)

        serializer = MarketActionCounterOfferSerializer(counterOffers, many=True)

        log.info("Returning counteroffers: {}  \n \n".format(json.dumps(serializer.data, cls=DjangoJSONEncoder)))
        return Response(serializer.data)

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID], responses=MarketActionCounterOffers_POST,
                         operation_summary="Create counteroffers", request_body=MarketActionCounterOfferSerializer)
    def post(self, request, *args, **kwargs):
        """Post counter-actions"""

        log.warning("Starting service MarketActionCounterOffers")
        log.warning("Check existence of marketplace with id: {}".format(kwargs['pk']))
        mplace = Filter.marketplaces(request=request, id=kwargs['pk'])
        if mplace is None:
            return Response(status=404, data={"details": "Marketplace not found"})

        log.warning("Marketplace with id: {} found".format(kwargs['pk']))
        data = request.data

        log.warning("Payload received for MarketActionCounterOffers: {}".format(data))
        is_many = isinstance(data, list)
        if not is_many:
            serializer = MarketActionCounterOfferSerializer(data=data)
            if serializer.is_valid():
                try:
                    bid = MarketAction.objects.get(id=data['marketAction_Bid_id'])
                    offer = MarketAction.objects.get(id=data['marketAction_Offer_id'])
                except Exception as e:
                    log.warning(e)
                    return Response(status=status.HTTP_404_NOT_FOUND, data={"details": str(e)})
                counteroffer = MarketActionCounterOffer(
                    marketAction_Bid_id=bid,
                    marketAction_Offer_id=offer,
                    exchangedValue=data['exchangedValue'])
                counteroffer.save()
                output = MarketActionCounterOfferSerializer(counteroffer)
                return Response(output.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            output_list = []
            serializer = MarketActionCounterOfferSerializer(data=data, many=True)
            if serializer.is_valid():
                log.warning("Counteroffers payload successfully deserialized")
                for counter_action in data:
                    try:
                        bid = MarketAction.objects.get(id=counter_action['marketAction_Bid_id'])
                        offer = MarketAction.objects.get(id=counter_action['marketAction_Offer_id'])
                    except Exception as e:
                        log.warning(e)
                        return Response(status=status.HTTP_404_NOT_FOUND, data={"details": str(e)})
                    counteroffer = MarketActionCounterOffer(
                        marketAction_Bid_id=bid,
                        marketAction_Offer_id=offer,
                        exchangedValue=counter_action['exchangedValue'])

                    log.warning("Saving counteroffer to database: {}".format(counteroffer))
                    counteroffer.save()
                    output_list.append(MarketActionCounterOfferSerializer(counteroffer).data)
                return Response(output_list, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class MarketActionCounterOffersOfSession(mixins.ListModelMixin,
                                         mixins.CreateModelMixin,
                                         generics.GenericAPIView):
    queryset = MarketActionCounterOffer.objects.all()
    serializer_class = MarketActionCounterOfferSerializer

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, MEMO_READ, MCM_READ, MBM_READ],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID],
                         responses=MarketActionCounterOffers_GET,
                         operation_summary="Get counteroffers")
    def get(self, request, *args, **kwargs):
        """Get counter-actions"""

        log.info("Started service to return counteroffers... \n \n")

        mplace_id = kwargs['pk']
        session_id = kwargs['sid']

        mplace = Filter.marketplaces(request=request, id=mplace_id)
        session = Filter.marketsessions(request=request, marketsession_id=session_id)

        log.info(
            "Validating router parameters marketplace id: {}, session id: {}...  \n \n ".format(mplace_id, session_id))

        if mplace is None:
            log.info("Marketplace with id: {} NOT FOUND".format(mplace_id))
            return Response(status=404, data={"details": "Marketplace not found"})

        if session is None:
            log.info("Session with id: {} NOT FOUND".format(session_id))
            return Response(status=404, data={"details": "Session not found"})

        if session.marketplaceid != mplace:
            log.info("Session with id: {} does not belong to the marketplace with id: {}".format(mplace_id, session_id))
            return Response(status=404, data={"details": "Session {} does not belong to the specified marketplace {} ".format(session_id, mplace_id)})

        log.info("Marketplace with id: {} valid  \n  Session with id: {} valid  \n \n ".format(mplace_id, session_id))

        counterOffers = MarketActionCounterOffer.objects.filter(
            marketAction_Bid_id__marketSessionid_id__marketplaceid=mplace,
            marketAction_Bid_id__marketSessionid_id=session)

        serializer = MarketActionCounterOfferSerializer(counterOffers, many=True)

        log.info("Returning counteroffers: {}  \n \n".format(json.dumps(serializer.data, cls=DjangoJSONEncoder)))
        return Response(serializer.data)


# TODO: check if this works after setting up KeyCloak. does middleware apply to this?
class MarketActionClearingCounterOffers(mixins.ListModelMixin,
                                        mixins.CreateModelMixin,
                                        generics.GenericAPIView):
    queryset = MarketActionCounterOffer.objects.all()
    serializer_class = MarketActionCounterOfferSerializerClearingResult

    keycloak_scopes = {
        'POST': [MARKET_OPERATOR_WRITE, MEMO_WRITE, MCM_WRITE, MBM_WRITE],
    }

    @swagger_auto_schema(manual_parameters=[], responses=MarketActionCounterOffers_POST,
                         operation_summary="Create counteroffers",
                         request_body=MarketActionCounterOfferSerializerClearingResult)
    def post(self, request, *args, **kwargs):
        """Post counter-actions"""

        log.info("Beginning service marketactioncounteroffers/it-clearing POST... \n \n")

        data = request.data

        log.info("Received counteroffers from ITLB: {} ... \n \n".format(json.dumps(data, cls=DjangoJSONEncoder)))

        mplace_id = None
        form = None
        session_id = None

        output_list = []
        saved_counteroffers = []

        # Saved the received counteroffers to the DB and add the coresponding actions in a list to send the for billing to MCM
        serializer = MarketActionCounterOfferSerializerClearingResult(data=data)
        if serializer.is_valid():

            if 'counteroffers' in data:
                ma_counteroffers = data['counteroffers']
            elif 'couteroffers' in data:
                ma_counteroffers = data['couteroffers']
            else:
                ma_counteroffers = None

            action_ids = data['actions']

            if ma_counteroffers is not None:
                for counter_action in ma_counteroffers:
                    try:
                        bid = MarketAction.objects.get(id=counter_action['marketAction_Bid_id'])
                        offer = MarketAction.objects.get(id=counter_action['marketAction_Offer_id'])

                        if mplace_id is None:
                            mplace_id = bid.marketSessionid.marketplaceid.id

                        if form is None:
                            form = bid.formid.form

                        if session_id is None:
                            session_id = bid.marketSessionid.id

                        existing_bid = next((x for x in output_list if x['id'] == bid.id), None)
                        if existing_bid is None:
                            output_list.append(MarketActionSerializer2(bid).data)

                        existing_offer = next((x for x in output_list if x['id'] == offer.id), None)
                        if existing_offer is None:
                            output_list.append(MarketActionSerializer2(offer).data)

                    except Exception as e:
                        log.warning(e)
                        return Response(status=status.HTTP_404_NOT_FOUND, data={"details": str(e)})

                    counteroffer = MarketActionCounterOffer(
                        marketAction_Bid_id=bid,
                        marketAction_Offer_id=offer,
                        exchangedValue=counter_action['exchangedValue'])
                    counteroffer.save()

                    saved_counteroffers.append(MarketActionCounterOfferSerializer(counteroffer).data)

            # If no matchings occurred for some actions, still add them in the output list sent to MCM for billing
            for action_id in action_ids:
                try:
                    action = MarketAction.objects.get(id=action_id)

                    if mplace_id is None:
                        mplace_id = action.marketSessionid.marketplaceid.id

                    if form is None:
                        form = action.formid.form

                    if session_id is None:
                        session_id = action.marketSessionid.id

                    existing_action = next((x for x in output_list if x['id'] == action.id), None)
                    if existing_action is None:
                        output_list.append(MarketActionSerializer2(action).data)

                except Exception as e:
                    log.warning(e)
                    return Response(status=status.HTTP_404_NOT_FOUND, data={"details": str(e)})

            # Log the created counteroffers for debugging purposes
            log.info("SAVED counter offers to database: {} \n \n".format(
                json.dumps(saved_counteroffers, cls=DjangoJSONEncoder)))

            return Response(status=status.HTTP_201_CREATED, data=json.dumps(output_list, cls=DjangoJSONEncoder))

            # log.info("Before sending actions to MCM postListActionToBilling: {} \n \n ".format(
            #     json.dumps(output_list, cls=DjangoJSONEncoder)))
            #
            # mcm_url = settings.MCM_URL + "marketplace/{}/form/{}/marketsessions/{}/billing/actions/".format(mplace_id,
            #                                                                                                 form,
            #                                                                                                 session_id)
            # mcm_response = postActions(output_list, mcm_url)
            #
            # log.info("Receivd response from MCM postListActionsToBilling:{} \n \n".format(mcm_response.status_code))
            #
            # if mcm_response.status_code in {404, 500}:
            #     log.info("Error invoking postListActionsToBilling MCM service with status code:{} \n \n".format(
            #         mcm_response.status_code))
            #     return Response(status=404,
            #                     data={"details": "Error invoking postListActionsToBilling MCM service. \n \n"})
            # else:
            #     log.info("Successfully sent actions to MCM service postListActionsToBilling: {} \n \n".format(
            #         mcm_response.status_code))
            #     return Response(status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response(status=status.HTTP_400_BAD_REQUEST, data={"details": "UNEXPECTED ERROR"})

def persist_counteroffers_return_cleared_actions(data):
    log.info("Beginning service marketactioncounteroffers/it-clearing POST... \n \n")

    data = json.loads(data)
    log.info("Received counteroffers from ITLB: {} ... \n \n".format(json.dumps(data, cls=DjangoJSONEncoder)))

    mplace_id = None
    form = None
    session_id = None

    output_list = []
    saved_counteroffers = []

    # Saved the received counteroffers to the DB and add the coresponding actions in a list to send the for billing to MCM
    serializer = MarketActionCounterOfferSerializerClearingResult(data=data)
    if serializer.is_valid():

        if 'counteroffers' in data:
            ma_counteroffers = data['counteroffers']
        elif 'couteroffers' in data:
            ma_counteroffers = data['couteroffers']
        else:
            ma_counteroffers = None

        action_ids = data['actions']

        if ma_counteroffers is not None:
            for counter_action in ma_counteroffers:
                try:
                    bid = MarketAction.objects.get(id=counter_action['marketAction_Bid_id'])
                    offer = MarketAction.objects.get(id=counter_action['marketAction_Offer_id'])

                    if mplace_id is None:
                        mplace_id = bid.marketSessionid.marketplaceid.id

                    if form is None:
                        form = bid.formid.form

                    if session_id is None:
                        session_id = bid.marketSessionid.id

                    existing_bid = next((x for x in output_list if x['id'] == bid.id), None)
                    if existing_bid is None:
                        output_list.append(MarketActionSerializer2(bid).data)

                    existing_offer = next((x for x in output_list if x['id'] == offer.id), None)
                    if existing_offer is None:
                        output_list.append(MarketActionSerializer2(offer).data)

                except Exception as e:
                    log.warning(e)
                    return Response(status=status.HTTP_404_NOT_FOUND, data={"details": str(e)})

                counteroffer = MarketActionCounterOffer(
                    marketAction_Bid_id=bid,
                    marketAction_Offer_id=offer,
                    exchangedValue=counter_action['exchangedValue'])
                counteroffer.save()

                saved_counteroffers.append(MarketActionCounterOfferSerializer(counteroffer).data)

        # If no matchings occurred for some actions, still add them in the output list sent to MCM for billing
        for action_id in action_ids:
            try:
                action = MarketAction.objects.get(id=action_id)

                if mplace_id is None:
                    mplace_id = action.marketSessionid.marketplaceid.id

                if form is None:
                    form = action.formid.form

                if session_id is None:
                    session_id = action.marketSessionid.id

                existing_action = next((x for x in output_list if x['id'] == action.id), None)
                if existing_action is None:
                    output_list.append(MarketActionSerializer2(action).data)

            except Exception as e:
                log.warning(e)
                return Response(status=status.HTTP_404_NOT_FOUND, data={"details": str(e)})

        # Log the created counteroffers for debugging purposes
        log.info("SAVED counter offers to database: {} \n \n".format(
            json.dumps(saved_counteroffers, cls=DjangoJSONEncoder)))

        return Response(status=status.HTTP_201_CREATED, data=json.dumps(output_list, cls=DjangoJSONEncoder))

        # log.info("Before sending actions to MCM postListActionToBilling: {} \n \n ".format(
        #     json.dumps(output_list, cls=DjangoJSONEncoder)))
        #
        # mcm_url = settings.MCM_URL + "marketplace/{}/form/{}/marketsessions/{}/billing/actions/".format(mplace_id,
        #                                                                                                 form,
        #                                                                                                 session_id)
        # mcm_response = postActions(output_list, mcm_url)
        #
        # log.info("Receivd response from MCM postListActionsToBilling:{} \n \n".format(mcm_response.status_code))
        #
        # if mcm_response.status_code in {404, 500}:
        #     log.info("Error invoking postListActionsToBilling MCM service with status code:{} \n \n".format(
        #         mcm_response.status_code))
        #     return Response(status=404,
        #                     data={"details": "Error invoking postListActionsToBilling MCM service. \n \n"})
        # else:
        #     log.info("Successfully sent actions to MCM service postListActionsToBilling: {} \n \n".format(
        #         mcm_response.status_code))
        #     return Response(status=status.HTTP_201_CREATED)

    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    return Response(status=status.HTTP_400_BAD_REQUEST, data={"details": "UNEXPECTED ERROR"})


class deleteMarketActionCounterOffers(mixins.ListModelMixin,
                                      mixins.CreateModelMixin,
                                      generics.GenericAPIView):
    queryset = MarketActionCounterOffer.objects.all()
    serializer_class = MarketActionCounterOfferSerializer

    keycloak_scopes = {
        'DELETE': [MARKET_OPERATOR_WRITE, MEMO_WRITE, MCM_WRITE, MBM_WRITE],
    }

    @swagger_auto_schema(manual_parameters=[], responses=deleteMarketActionCounterOffers_DELETE,
                         operation_summary="Delete counteroffers", request_body=MarketActionCounterOfferSerializer)
    def delete(self, request, *args, **kwargs):
        """Post counter-actions"""

        data = request.data

        serializer = MarketActionCounterOfferSerializer(data=data, many=True)

        if serializer.is_valid():
            for counter_action in data:
                try:
                    marketactioncounteroffer = MarketActionCounterOffer.objects.get(id=counter_action['id'])
                    marketactioncounteroffer.delete()
                except Exception as e:
                    log.warning(e)
                    return Response(status=status.HTTP_404_NOT_FOUND, data={"details": str(e)})

            return Response(status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MarketActionCounterOfferById(APIView):
    queryset = MarketActionCounterOffer.objects.all()
    serializer_class = MarketActionCounterOfferSerializer

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, MEMO_READ, MCM_READ, MBM_READ],
    }

    @swagger_auto_schema(manual_parameters=[COUNTER_ID],
                         responses=MarketActionCounterOffer_GET,
                         operation_summary="Get counteroffer by ID")
    def get(self, request, *args, **kwargs):
        """Get counter-action by ID"""
        try:
            counterOffer = Filter.marketactioncounteroffer(request=request, macounter_id=int(kwargs.get('counter_id')))
        except Exception as e:
            log.warning(e)
            return Response(status=status.HTTP_404_NOT_FOUND, data={"details": str(e)})
        serializer = MarketActionCounterOfferSerializer(counterOffer)
        return Response(serializer.data)


class MarketSessionById(APIView):
    queryset = MarketSession.objects.all()
    serializer_class = MarketSessionSerializer2

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, MEMO_READ, MCM_READ, MBM_READ, DSO_READ, MARKET_PARTICIPANT_READ],
    }

    @swagger_auto_schema(manual_parameters=[SESSION_ID],
                         responses=MarketSession_GET,
                         operation_summary="Get a single market session")
    def get(self, request, *args, **kwargs):
        """
        Through this interface, a single session can be retrieved
        """
        market_session = Filter.marketsessions(request=request, marketsession_id=int(kwargs.get('sid')))
        if market_session is None:
            return Response(status=404)
        serializer = MarketSessionSerializer1(market_session)
        return Response(serializer.data, status=status.HTTP_200_OK)

class MarketSessionBriefById(APIView):
    queryset = MarketSession.objects.all()
    serializer_class = MarketSessionSerializer2
    permission_classes = (IsMarketOperator | IsMEMO | IsMCM | IsMBM | IsMarketplaceDSOReadOnly | IsMarketplaceParticipantReadOnly,)

    @swagger_auto_schema(manual_parameters=[SESSION_ID],
                         responses=MarketSession_GET,
                         operation_summary="Get a single market session with summary")
    def get(self, request, *args, **kwargs):
        """
        Through this interface, a single session can be retrieved
        with a depth 0 serializer, meaning no further details for foreign key
        objects
        """
        market_session = Filter.marketsessions(request=request, marketsession_id=int(kwargs.get('sid')))
        if market_session is None:
            return Response(status=404)
        serializer = MarketSessionSerializer2(market_session)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ModifyMarketSession(APIView):
    queryset = MarketSession.objects.all()
    serializer_class = MarketSessionSerializer

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, MEMO_READ, MCM_READ, MBM_READ, DSO_READ, MARKET_PARTICIPANT_READ],
        'PUT': [MARKET_OPERATOR_WRITE, MEMO_WRITE, MCM_WRITE, MBM_WRITE],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, SESSION_ID],
                         responses=ModifyMarketSession_GET,
                         operation_summary="Get a single market session")
    def get(self, request, *args, **kwargs):
        """
        Through this interface, a single session of a marketplace can be retrieved
        """
        market_session = Filter.marketsessions(request=request, marketsession_id=int(kwargs.get('sid')))
        if market_session is None or market_session.marketplaceid.id != int(kwargs.get('pk')):
            return Response(status=404)
        serializer = MarketSessionSerializer(market_session)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, SESSION_ID],
                         responses=ModifyMarketSession_PUT,
                         operation_summary="Market session modified", request_body=MarketSessionSerializer)
    def put(self, request, *args, **kwargs):
        """Through this interface, entities may modify an existing market session."""

        data = request.data

        if 'pk' not in kwargs and 'sid' not in kwargs:
            return Response(status=status.HTTP_404_NOT_FOUND,
                            data={"details": "Marketplace or session ID not provided"})
        try:
            mplace = Filter.marketplaces(request=request, id=kwargs['pk'])
            ses = Filter.marketsessions(request=request, marketsession_id=kwargs['sid'])
            if not ses or ses.marketplaceid != mplace:
                return Response(status=status.HTTP_404_NOT_FOUND,
                                data={"details": "Session ID does not match Marketplace ID."})
        except Exception as e:
            log.warning(e)
            return Response(status=status.HTTP_404_NOT_FOUND, data={"details": str(e)})

        try:
            serializer = MarketSessionSerializer2(data=data)
            try:
                serializer.is_valid(raise_exception=True)
            except Exception as e:
                log.warning(e)
                return Response(status=400,
                                data={"details": "Invalid input: noticed a market session of invalid format."})
            new_ses = serializer.data
            ses.sessionStartTime = new_ses['sessionStartTime']
            ses.sessionEndTime = new_ses['sessionEndTime']
            ses.deliveryStartTime = new_ses['deliveryStartTime']
            ses.deliveryEndTime = new_ses['deliveryEndTime']
            ses.sessionStatusid = SessionStatus.objects.get(id=new_ses['sessionStatusid'])
            ses.formid = Form.objects.get(id=new_ses['formid'])
            ses.clearingPrice = new_ses['clearingPrice']
            ses.save()
        except Exception as e:
            log.warning(e)
            return Response(status=400, data={"details": "Invalid input"})
        market_actions = Filter.market_actions_of_session(request=request, market_session=ses).filter(
            statusid__status__in=["valid"])
        if market_actions is not None:
            for market_action in market_actions:
                market_action.statusid = ActionStatus.objects.get(status="inactive")
                market_action.save()
        return Response(status=204)


class sessionStatus(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, DSO_READ, KNOWN_MARKET_ACTOR_READ],
    }

    @swagger_auto_schema(responses=getSessionStatus_GET,
                         operation_summary="Session Status cases are provided")
    def get(self, requests, *args, **kwargs):
        """Through this interface the list of market session status cases is provided"""
        stat = SessionStatus.objects.all()
        if stat is None:
            return Response(status=status.HTTP_404_NOT_FOUND)
        serializer = SessionStatusSerializer(stat, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class actionStatus(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, KNOWN_MARKET_ACTOR_READ, MEMO_READ],
    }

    @swagger_auto_schema(responses=getActionStatus_GET,
                         operation_summary="Action Status cases are provided")
    def get(self, requests, *args, **kwargs):
        """Through this interface Action Status cases are provided"""
        stat = ActionStatus.objects.all()
        if stat is None:
            return Response(status=status.HTTP_404_NOT_FOUND)
        serializer = ActionStatusSerializer(stat, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class actionStatusById(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, KNOWN_MARKET_ACTOR_READ, MEMO_READ],
    }

    @swagger_auto_schema(manual_parameters=[ACTION_STATUS_ID], responses=getActionStatusId_GET,
                         operation_summary="Action Status retrieved by ID")
    def get(self, request, *args, **kwargs):
        """Through this interface the action status by ID can be retrieved"""
        try:
            status = ActionStatus.objects.get(pk=kwargs.get('action_status_id'))
        except Exception as e:
            log.warning("Form not found. ", e)
            return Response(status=404, data={"details": str(e)})
        serializer = ActionStatusSerializer(status)
        return Response(serializer.data)


class actionType(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, MEMO_READ, MCM_READ, MBM_READ, KNOWN_MARKET_ACTOR_READ],
    }

    @swagger_auto_schema(responses=actionType_GET,
                         operation_summary="Action Status cases are provided")
    def get(self, requests, *args, **kwargs):
        """Through this interface the list of action status cases is provided"""
        atype = ActionType.objects.all()
        if atype is None:
            return Response(status=status.HTTP_404_NOT_FOUND)
        serializer = ActionTypeSerializer(atype, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class editSingleAction(APIView):

    keycloak_scopes = {
        'PUT': [MARKET_OPERATOR_WRITE, DSO_WRITE, KNOWN_MARKET_ACTOR_WRITE],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, SESSION_ID, ACTION_ID], responses=editSingleAction_PUT,
                         operation_summary="Modify specific action", request_body=MarketActionSerializerEdit)
    def put(self, request, *args, **kwargs):
        """Through this interface a market action can be modified"""
        data = request.data
        output = []
        actions_to_be_validated = []

        if 'pk' not in kwargs and 'sid' not in kwargs and 'action_id' not in kwargs:
            return Response(status=status.HTTP_404_NOT_FOUND,
                            data={"details": "Marketplace or session ID not provided"})
        try:
            mplace = Filter.marketplaces(request=request, id=kwargs['pk'])
            ses = Filter.marketsessions(request=request, marketsession_id=kwargs['sid'])
            if ses.marketplaceid != mplace:
                return Response(status=status.HTTP_404_NOT_FOUND,
                                data={"details": "Session ID doe not match Marketplace ID."})
        except Exception as e:
            log.warning(e)
            return Response(status=status.HTTP_404_NOT_FOUND, data={"details": str(e)})

        new_status = ActionStatus.objects.get(id=data['statusid'])

        serializer = MarketActionSerializerEdit(data=data)

        if serializer.is_valid():
            try:
                act = MarketAction.objects.get(id=kwargs['action_id'])
            except Exception as e1:
                log.warning(e1)
                return Response(status=status.HTTP_404_NOT_FOUND, data={"details": str(e1)})

            existing_status = ActionStatus.objects.get(id=data['statusid'])
            act.actionStartTime = data['actionStartTime']
            act.actionEndTime = data['actionEndTime']
            act.value = data['value']
            act.oum = data['uom']
            act.price = data['price']
            act.deliveryPoint = data['deliveryPoint']

            if data['loadid'] is not None:
                act.loadid = Load.objects.get(id=data['loadid'])
            else:
                act.loadid = None

            # case MarketAction is an IT Load Market Action, update the associated Load Values
            if act.loadid is not None:

                cpu_load_value = LoadValue.objects.get(loadid=act.loadid, parameter="CPU")
                ram_load_value = LoadValue.objects.get(loadid=act.loadid, parameter="RAM")
                disk_load_value = LoadValue.objects.get(loadid=act.loadid, parameter="Disk")

                if cpu_load_value is not None:
                    cpu_load_value.value = data['cpu']
                    cpu_load_value.save()

                if ram_load_value is not None:
                    ram_load_value.value = data['ram']
                    ram_load_value.save()

                if disk_load_value is not None:
                    disk_load_value.value = data['disk']
                    disk_load_value.save()

            act.actionTypeid = ActionType.objects.get(id=data['actionTypeid'])
            act.statusid = ActionStatus.objects.get(id=data['statusid'])
            act.save()
            if existing_status.status in ['unchecked', 'valid', 'invalid'] and new_status.status not in ['selected']:
                act.statusid = ActionStatus.objects.get(status="unchecked")
                act.save()

                saved_action = MarketAction.objects.get(pk=act.id)
                actions_to_be_validated.append(saved_action)

                # send action to validity check
                # choose action form DB, as all the fields would have been converted to correct type date is DateTime not Str
                action_validity_check.check_actions(marketplace=mplace, market_session=ses,
                                                    market_actions=actions_to_be_validated)

            return Response(serializer.data, status=200)
        else:
            log.warning("Serializer errors: {}".format(serializer.errors))
            return Response(serializer.errors, status=400)
        return Response(status=400)


class getReferencePrice(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, DSO_WRITE, MARKET_PARTICIPANT_READ, MEMO_READ, MCM_READ, MBM_READ],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, FORM], responses=getReferencePrice_GET,
                         operation_summary="Get the reference price for the target market ID")
    def get(self, request, *args, **kwargs):
        """Retrieve the reference price for the target marketplace"""

        marketservicetypes = dict(MarketServiceType.type_choices).keys()
        energyforms = ['electric_energy', 'thermal_energy', 'it_load']
        ancillaryforms = ['congestion_management', 'reactive_power_compensation', 'spinning_reserve', 'scheduling']

        try:
            if 'date' in kwargs:
                date = kwargs.get('date')
            else:
                date = pytz.utc.localize(datetime.now())

            form = Form.objects.get(form=kwargs['form'])
            mplace = Filter.marketplaces(request=request, id=kwargs['pk'])

            if (mplace.marketServiceTypeid.type == "energy" and form.form == "electric_energy") or (
                    mplace.marketServiceTypeid.type == "ancillary-services" and form.form in ancillaryforms) or (
                    mplace.marketServiceTypeid.type == "thermal_energy" and form.form == "thermal_energy") or (
                    mplace.marketServiceTypeid.type == "it_load" and form.form == "it_load"):

                prices = ReferencePrice.objects.filter(marketplaceId=mplace, formId=form,
                                                       validityEndTime__gt=date,
                                                       validityStartTime__lt=date)
                get_price = prices.latest('validityEndTime')
            else:
                return Response(status=400, data={"details": "Mismatch in input parameters"})
        except Exception as e:
            log.warning(e)
            return Response(status=404, data={"details": str(e)})
        serializer = ReferencePriceSerializer(get_price)
        return Response(serializer.data)


class getReferencePriceValue(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, DSO_WRITE, MARKET_PARTICIPANT_READ, MEMO_READ, MCM_READ, MBM_READ],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, FORM],
                         operation_summary="Get the reference price for the target market ID")
    def get(self, request, *args, **kwargs):
        """Retrieve the reference price for the target marketplace"""

        marketservicetypes = dict(MarketServiceType.type_choices).keys()
        energyforms = ['electric_energy', 'thermal_energy', 'it_load']
        ancillaryforms = ['congestion_management', 'reactive_power_compensation', 'spinning_reserve', 'scheduling']

        try:
            if 'date' in kwargs:
                date = kwargs.get('date')
            else:
                date = pytz.utc.localize(datetime.now())

            form = Form.objects.get(form=kwargs['form'])
            mplace = Filter.marketplaces(request=request, id=kwargs['pk'])

            if (mplace.marketServiceTypeid.type == "energy" and form.form == "electric_energy") or (
                    mplace.marketServiceTypeid.type == "ancillary-services" and form.form in ancillaryforms) or (
                    mplace.marketServiceTypeid.type == "thermal_energy" and form.form == "thermal_energy") or (
                    mplace.marketServiceTypeid.type == "it_load" and form.form == "it_load"):

                prices = ReferencePrice.objects.filter(marketplaceId=mplace, formId=form,
                                                       validityEndTime__gt=date,
                                                       validityStartTime__lt=date)
                get_price = prices.latest('validityEndTime')
            else:
                return Response(status=400, data={"details": "Mismatch in input parameters"})
        except Exception as e:
            log.warning(e)
            return Response(status=404, data={"details": str(e)})

        return Response(get_price.price)


class postReferencePrice(APIView):

    keycloak_scopes = {
        'POST': [MARKET_OPERATOR_WRITE, MEMO_READ, MCM_READ, MBM_READ],
    }

    @swagger_auto_schema(responses=postReferencePrice, operation_summary="Create reference prices",
                         request_body=ReferencePriceSerializer)
    def post(self, request, *args, **kwargs):
        """Post reference prices"""
        data = request.data
        serializer = ReferencePriceSerializer(data=data, many=True)

        try:
            if serializer.is_valid():
                for priceitem in data:
                    referenceprice = ReferencePrice(price=priceitem['price'],
                                                    validityStartTime=priceitem['validityStartTime'],
                                                    validityEndTime=priceitem['validityEndTime'],
                                                    formId=Form.objects.get(pk=priceitem['formId']),
                                                    marketplaceId=Marketplace.objects.get(
                                                        pk=priceitem['marketplaceId']))
                    referenceprice.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            log.warning("Could not create ReferencePrice {}: {}", serializer.data, e)
            return Response(status=404, data={"details": str(e)})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class getReferencePricesPreviousDay(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, MEMO_READ, MCM_READ, MBM_READ, MARKET_PARTICIPANT_READ, DSO_READ],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, FORM], responses=getReferencePrice_GET,
                         operation_summary="Get the reference price for the target market ID in the last 24 hours")
    def get(self, request, *args, **kwargs):
        """Retrieve the reference prices for the target marketplace in the last 24 hours"""

        marketservicetypes = dict(MarketServiceType.type_choices).keys()
        energyforms = ['electric_energy', 'thermal_energy', 'it_load']
        ancillaryforms = ['congestion_management', 'reactive_power_compensation', 'spinning_reserve', 'scheduling']

        try:
            if 'date' in kwargs:
                date = int(kwargs.get('date'))
                current_time = datetime.fromtimestamp(date / 1000.0)
                current_time_previous_day = current_time - timedelta(hours=24)
            else:
                date = pytz.utc.localize(datetime.now())
                current_time = date
                current_time_previous_day = current_time - timedelta(hours=24)

            form = Form.objects.get(form=kwargs['form'])
            mplace = Filter.marketplaces(request=request, id=kwargs['pk'])

            if (mplace.marketServiceTypeid.type == "energy" and form.form == "electric_energy") or (
                    mplace.marketServiceTypeid.type == "ancillary-services" and form.form in ancillaryforms) or (
                    mplace.marketServiceTypeid.type == "thermal_energy" and form.form == "thermal_energy") or (
                    mplace.marketServiceTypeid.type == "it_load" and form.form == "it_load"):

                ref_prices = ReferencePrice.objects.filter(~Q(validityEndTime__lt=current_time_previous_day),
                                                           ~Q(validityStartTime__gt=current_time),
                                                           formId=form,
                                                           marketplaceId=mplace,
                                                           ).order_by('validityStartTime', 'validityEndTime')
            else:
                return Response(status=400, data={"details": "Mismatch in input parameters"})
        except Exception as e:
            log.warning(e)
            return Response(status=404, data={"details": str(e)})
        serializer = ReferencePriceSerializer2(ref_prices, many=True)
        return Response(serializer.data)


class getClearingPricesPreviousDay(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, MEMO_READ, MCM_READ, MBM_READ, MARKET_PARTICIPANT_READ, DSO_READ],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, FORM], responses=getReferencePrice_GET,
                         operation_summary="Get the clearing prices for the target market ID in the last 24 hours")
    def get(self, request, *args, **kwargs):
        """Retrieve the clearing prices for the target marketplace in the last 24 hours"""

        marketservicetypes = dict(MarketServiceType.type_choices).keys()
        energyforms = ['electric_energy', 'thermal_energy', 'it_load']
        ancillaryforms = ['congestion_management', 'reactive_power_compensation', 'spinning_reserve', 'scheduling']

        try:
            if 'date' in kwargs:
                date = int(kwargs.get('date'))
                current_time = datetime.fromtimestamp(date / 1000.0)
                current_time_previous_day = current_time - timedelta(hours=24)
            else:
                date = pytz.utc.localize(datetime.now())
                current_time = date
                current_time_previous_day = current_time - timedelta(hours=24)

            form = Form.objects.get(form=kwargs['form'])
            mplace = Filter.marketplaces(request=request, id=kwargs['pk'])

            if (mplace.marketServiceTypeid.type == "energy" and form.form == "electric_energy") or (
                    mplace.marketServiceTypeid.type == "ancillary-services" and form.form in ancillaryforms) or (
                    mplace.marketServiceTypeid.type == "thermal_energy" and form.form == "thermal_energy") or (
                    mplace.marketServiceTypeid.type == "it_load" and form.form == "it_load"):

                market_sessions = MarketSession.objects.filter(~Q(deliveryEndTime__lt=current_time_previous_day),
                                                               ~Q(deliveryStartTime__gt=current_time),
                                                               formid=form,
                                                               marketplaceid=mplace,
                                                               ).order_by('deliveryStartTime', 'deliveryEndTime')

                clearing_price_objects = []

                for mses in market_sessions:
                    if mses.clearingPrice is not None:
                        clearing_price = {}
                        clearing_price['price'] = mses.clearingPrice
                        clearing_price['startTime'] = mses.deliveryStartTime
                        clearing_price['endTime'] = mses.deliveryEndTime
                        clearing_price['formId'] = form.id
                        clearing_price['marketplaceId'] = mplace.id
                        clearing_price['sessionId'] = mses.id
                        clearing_price_objects.append(clearing_price)

            else:
                return Response(status=400, data={"details": "Mismatch in input parameters"})
        except Exception as e:
            log.warning(e)
            return Response(status=404, data={"details": str(e)})
        return Response(clearing_price_objects, status=200)

class getRules(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, DSO_READ, KNOWN_MARKET_ACTOR_READ],
        'POST': [MARKET_OPERATOR_WRITE],
        'DELETE': [MARKET_OPERATOR_WRITE],
        'PUT': [MARKET_OPERATOR_WRITE],
    }

    @swagger_auto_schema(responses=getRules_GET, operation_summary="List all the available rules")
    def get(self, request, *args, **kwargs):
        """ Retrieve rules """
        try:
            data = Rule.objects.all()
            serializer = RuleSerializer(data, many=True)
            return Response(serializer.data)
        except Exception as e:
            log.warning(e)
            return Response(status=404)

    @swagger_auto_schema(responses=getRules_POST, operation_summary="List all the available rules",
                         request_body=RuleSerializer)
    def post(self, request, *args, **kwargs):
        """ Add new rules """
        data = dict(request.data)
        data['timestamp'] = pytz.utc.localize(datetime.now())
        serializer = RuleSerializer(data=data)
        if serializer.is_valid():
            obj = Rule.objects.create(title=data['title'], description=data['description'], timestamp=data['timestamp'])
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(responses=getRules_POST, operation_summary="Delete rule",
                         request_body=RuleSerializer)
    def delete(self, request, *args, **kwargs):
        """ Delete a rule """
        payload = request.data
        rule = Rule.objects.get(pk=payload['id']).delete()
        return Response(status=204)

    @swagger_auto_schema(responses=getRules_POST, operation_summary="Edit rule",
                         request_body=RuleSerializer)
    def put(self, request):
        """Update a rule"""

        payload = request.data

        Rule.objects.filter(pk=payload['id']).update(
            title=payload['title'],
            description=payload['description']
        )
        return Response(status=204)


class viewRule(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, DSO_READ, KNOWN_MARKET_ACTOR_READ],
        'DELETE': [MARKET_OPERATOR_WRITE],
    }

    @swagger_auto_schema(manual_parameters=[RULE_ID], responses=RULE_GET, operation_summary="Get rule")
    def get(self, request, *args, **kwargs):
        try:
            rule = get_object_or_404(Rule, pk=kwargs.get('pk'))
            serializer = RuleSerializer(rule)
            return Response(serializer.data)
        except Exception as e:
            log.warning(e)
            return Response(status=404)

    @swagger_auto_schema(manual_parameters=[RULE_ID], responses=RULE_DELETE, operation_summary="Delete rule",
                         request_body=RuleSerializer)
    def delete(self, requests, *args, **kwargs):
        try:
            rule = get_object_or_404(Rule, pk=kwargs.get('pk'))
            rule.delete()
            return Response(status=204)
        except Exception as e:
            log.warning(e)
            return Response(status=404)


class getProfileData(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, KNOWN_MARKET_ACTOR_READ],

    }

    @swagger_auto_schema(manual_parameters=[USERNAME], responses=PARTICIPANT_GET,
                         operation_summary="Get profile for a given username")
    def get(self, request, *args, **kwargs):
        name = kwargs.get('username')
        actor = None
        user = None
        dso = None
        rel = UserActorRel.objects.filter(user__username=name, status="valid")
        if len(rel) == 0:
            return Response(status=404)
        elif len(rel) == 1:
            actor = MarketActor.objects.get(id=rel[0].actor.id)
            user = User.objects.get(pk=rel[0].user_id)
            marketParts = Marketplace_has_MarketActor.objects.filter(marketActor_id=actor.id)
            if len(marketParts):
                marketPart = marketParts[0]
                market = Marketplace.objects.filter(pk=marketPart.marketplace.id)
                if len(market):
                    dso = MarketActor.objects.get(pk=market[0].dSOid_id)
        if actor:
            tmp = MarketActorSerializer(actor).data
        else:
            tmp = None
        if dso:
            tmp1 = MarketActorSerializer(dso).data
        else:
            tmp1 = None
        return Response({
            'actor': tmp,
            'market_actors': MarketActorTypeSerializer(MarketActorType.objects.all(), many=True).data,
            'user': {'username': user.username, 'id': user.id, 'first_name': user.first_name,
                     'last_name': user.last_name, 'email': user.email},
            'dso': tmp1
        })


class UpdateUserProfile(APIView):

    keycloak_scopes = {
        'PUT': [MARKET_OPERATOR_WRITE, KNOWN_MARKET_ACTOR_WRITE],
    }

    @swagger_auto_schema(responses=PARTICIPANT_PUT, operation_summary="Update profile for a given username",
                         request_body=RuleSerializer)
    def put(self, request, *args, **kwargs):
        """ Update user profile """
        try:
            payload = request.data
            rel = UserActorRel.objects.get(user__username=kwargs.get("username"))
            actor = MarketActor.objects.get(pk=rel.actor_id)
            user = User.objects.get(pk=rel.user_id)
            User.objects.filter(id=user.id).update(
                first_name=payload['first_name'],
                last_name=payload['last_name'],
                email=payload['email']
            )
            MarketActor.objects.filter(id=actor.id).update(
                companyName=payload['company_name'],
                email=payload['email'],
                phone=payload['phone'],
                representativeName=payload['first_name'] + " " + payload['last_name'],
                vat=payload['vat']
            )

            return JsonResponse({"state": True})

        except Exception as ex:
            from traceback import print_exc
            print_exc()
            return JsonResponse({"state": False, "reason": ex.args})


class AdminDetail(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ],
    }

    @swagger_auto_schema(responses=MarketOperator_GET, operation_summary="Get details of admin")
    def get(self, request, *args, **kwargs):
        """Displays relationships between users and marketplace actors"""
        actors = MarketActor.objects.all()
        users_actors = UserActorRel.objects.all()
        users = User.objects.all()
        pending_requests = UserActorRel.objects.filter(status="invalid").count()
        rules = Rule.objects.all()

        timeframes = TimeFrame.objects.all()
        forms = Form.objects.all()
        dsoList = MarketActor.objects.filter(
            pk__in=[rel.actor.id for rel in UserActorRel.objects.filter(status="valid")],
            marketActorTypeid__type__iexact="DSO")
        sessionStatus = SessionStatus.objects.all()
        context = {
            'actors': MarketActorSerializer1(actors, many=True).data,
            'users': UserSerializer(users, many=True).data,
            'relations': UserActorRelSerializer(users_actors, many=True).data,
            'pending_requests': pending_requests,
            'title': "Admin",
            'rules': RuleSerializer(rules, many=True).data,
            'timeframes': TimeFrameSerializer(timeframes, many=True).data,
            'forms': FormSerializer(forms, many=True).data,
            'dsoList': MarketActorSerializer(dsoList, many=True).data,
            "sessionStatus": SessionStatusSerializer(sessionStatus, many=True).data,
        }
        return JsonResponse(context)


class RelationDetails(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ],
    }

    @swagger_auto_schema(responses=MarketOperator_GET, operation_summary="Get relation details of actors.")
    def get(self, request, *args, **kwargs):
        users_actors = UserActorRel.objects.all()
        actors = []
        uIds = []
        for item in users_actors:
            actors.append(item.actor)
            uIds.append(item.user)
        context = {
            'actors': MarketActorSerializer(actors, many=True).data,
            'users': UserSerializer(uIds, many=True).data,
            'relations': UserActorRelSerializer(users_actors, many=True).data
        }
        return JsonResponse(context)


class SessionView(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ],
    }

    @swagger_auto_schema(responses=MarketSessions_GET, operation_summary="List of all market sessions")
    def get(self, request, *args, **kwargs):
        """Retrieve the list of all market sessions"""
        try:
            sessions = MarketSession.objects.all()
            serializer = MarketSessionSerializer(sessions, many=True)
            return Response(serializer.data)
        except Exception as ex:
            if settings.DEBUG:
                from traceback import print_exc
                print_exc()
            return JsonResponse({"message": ex.message, "sessions": []}, status=400)


class AccessManagerDec(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, KNOWN_MARKET_ACTOR_READ],
    }

    @swagger_auto_schema(manual_parameters=[USERNAME], responses=AccessManagerDec_GET,
                         operation_summary="Get User actor relation")
    def get(self, request, *args, **kwargs):
        try:
            rel = UserActorRel.objects.get(user__username=kwargs.get("username"))
            dso_count = MarketActor.objects.filter(pk=rel.actor_id,
                                                   marketActorTypeid__type="dso").count()
        except:
            raise Http404
        context = {'rel': UserActorRelSerializer(rel).data, 'dso_count': dso_count}
        return JsonResponse(context)


class returnSpecificActor(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, KNOWN_MARKET_ACTOR_READ],
    }

    @swagger_auto_schema(responses=returnSpecificActor_GET, operation_summary="Get specific actor")
    def get(self, request, *args, **kwargs):
        rel = UserActorRel.objects.get(user__username=kwargs.get('username'))
        actor = rel.actor
        serializer = MarketActorSerializer(actor)
        return (Response(serializer.data))


class displaySpecificInvoices(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, KNOWN_MARKET_ACTOR_READ],
    }

    @swagger_auto_schema(manual_parameters=[PATHINVOICES, ACTOR_ID, USERNAME], responses=User_Invoices_GET,
                         operation_summary="Get energy invoices of specific actor")
    def get(self, request, *args, **kwargs):
        """Retrieve invoices of specific actor"""
        try:
            pathInvoices = kwargs['pathInvoices']
            actor = MarketActor.objects.get(pk=kwargs['actor'])
            username = kwargs['username']
            user = UserActorRel.objects.get(user__username=username)
        except Exception as e:
            log.warning(e)
            return Response(status=404, data={"details": str(e)})

        invoice_info = []

        try:
            if pathInvoices == "user-invoices":
                formInvoices = ['electric_energy']
            elif pathInvoices == "heat-invoices":
                formInvoices = ['thermal_energy']
            elif pathInvoices == "it_load-invoices":
                formInvoices = ['it_load']
            elif pathInvoices == "balancing-invoices":
                formInvoices = ['congestion_management', 'reactive_power_compensation', 'spinning_reserve',
                                'scheduling']

            if actor.marketActorTypeid.type == "market operator":
                candidate_actions = MarketAction.objects.filter(
                    Q(formid__form__in=formInvoices))
            else:
                candidate_actions = MarketAction.objects.filter(
                    Q(marketActorid=actor) & Q(formid__form__in=formInvoices))
        except Exception as e:
            log.warning(e)
            return Response(status=404, data={"details": str(e)})

        for candidate_action in candidate_actions:
            try:
                candidate_macos = MarketActionCounterOffer.objects.filter(
                    Q(marketAction_Bid_id=candidate_action) | Q(marketAction_Offer_id=candidate_action))
            except Exception as e:
                log.warning(e)
                return Response(status=404)
            for candidate_maco in candidate_macos:
                try:
                    candidate_transactions = Transaction.objects.filter(marketActionCounterOfferid=candidate_maco)
                except Exception as e:
                    log.warning(e)
                    return Response(status=404, data={"details": str(e)})
                for candidate_transaction in candidate_transactions:
                    invoices = Invoice.objects.filter(transactionid=candidate_transaction)
                    if len(invoices) > 1:
                        raise Exception('Multiple Invoices for one transaction')
                    if len(invoices) < 1:
                        continue
                    invoice = {}
                    invoice['participant'] = candidate_action.marketActorid.representativeName
                    invoice['action_id'] = candidate_action.id
                    invoice['actionStartTime'] = setUTCFormatter(candidate_action.actionStartTime)
                    invoice['actionEndTime'] = setUTCFormatter(candidate_action.actionEndTime)
                    if pathInvoices == "balancing-invoices":
                        invoice['price'] = candidate_action.price
                    elif pathInvoices == "it_load-invoices":
                        invoice['price'] = invoices[0].amount
                    else:
                        invoice['price'] = candidate_action.marketSessionid.clearingPrice
                    # invoice['exchanged_value']  = str(candidate_maco.exchangedvalue) + " " + candidate_action.uom

                    # in case of the flexibility market
                    # the amount of the invoice is directly the amount from the corresponding database
                    # object, as it was set by the clearing algorithm
                    if pathInvoices == "balancing-invoices":
                        invoice['exchanged_value'] = invoices[0].amount
                    else:
                        invoice['exchanged_value'] = round(candidate_maco.exchangedValue * invoice['price'], 2)

                    invoice['marketSession'] = candidate_action.marketSessionid.id
                    invoice['type'] = candidate_action.actionTypeid.type
                    invoice['id'] = invoices[0].id
                    invoice['date'] = setUTCFormatter(invoices[0].date)
                    invoice['fee'] = "-" if invoices[0].fixedFee == None else invoices[0].fixedFee
                    invoice['penalty'] = "-" if invoices[0].penalty == None else invoices[0].penalty
                    invoice['total'] = invoice['exchanged_value'] + invoice['fee'] - invoice['penalty']
                    invoice['form'] = candidate_action.formid.form
                    invoice_info.append(invoice)
        return Response({'invoices': invoice_info}, status=200)


class displaySpecificInvoicesDetails(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, KNOWN_MARKET_ACTOR_READ],
    }

    @swagger_auto_schema(manual_parameters=[PATHINVOICES, ACTOR_ID, USERNAME], responses=User_Invoices_GET,
                         operation_summary="Get energy invoices of specific actor")
    def get(self, request, *args, **kwargs):
        """Retrieve invoices of specific actor"""
        try:
            pathInvoices = kwargs['pathInvoices']
            actor = MarketActor.objects.get(pk=kwargs['actor'])
            username = kwargs['username']
            user = UserActorRel.objects.get(user__username=username)
        except Exception as e:
            log.warning(e)
            return Response(status=404, data={"details": str(e)})

        invoice_info = []

        try:
            if pathInvoices == "user-invoices":
                formInvoices = ['electric_energy']
            elif pathInvoices == "heat-invoices":
                formInvoices = ['thermal_energy']
            elif pathInvoices == "it_load-invoices":
                formInvoices = ['it_load']
            elif pathInvoices == "balancing-invoices":
                formInvoices = ['congestion_management', 'reactive_power_compensation', 'spinning_reserve',
                                'scheduling']

            if actor.marketActorTypeid.type == "market operator":
                candidate_actions = MarketAction.objects.filter(
                    Q(formid__form__in=formInvoices))
            else:
                candidate_actions = MarketAction.objects.filter(
                    Q(marketActorid=actor) & Q(formid__form__in=formInvoices))
        except Exception as e:
            log.warning(e)
            return Response(status=404, data={"details": str(e)})

        for candidate_action in candidate_actions:
            try:
                candidate_macos = MarketActionCounterOffer.objects.filter(
                    Q(marketAction_Bid_id=candidate_action) | Q(marketAction_Offer_id=candidate_action))
            except Exception as e:
                log.warning(e)
                return Response(status=404)
            for candidate_maco in candidate_macos:
                try:
                    candidate_transactions = Transaction.objects.filter(marketActionCounterOfferid=candidate_maco)
                except Exception as e:
                    log.warning(e)
                    return Response(status=404, data={"details": str(e)})
                for candidate_transaction in candidate_transactions:
                    invoices = Invoice.objects.filter(transactionid=candidate_transaction)
                    if len(invoices) > 1:
                        raise Exception('Multiple Invoices for one transaction')
                    if len(invoices) < 1:
                        continue
                    invoice = {}
                    invoice['market_actor_id'] = candidate_action.marketActorid.id
                    invoice['participant'] = candidate_action.marketActorid.representativeName
                    invoice['action_id'] = candidate_action.id
                    invoice['actionStartTime'] = setUTCFormatter(candidate_action.actionStartTime)
                    invoice['actionEndTime'] = setUTCFormatter(candidate_action.actionEndTime)
                    if pathInvoices == "balancing-invoices":
                        invoice['price'] = candidate_action.price
                    elif pathInvoices == "it_load-invoices":
                        invoice['price'] = invoices[0].amount
                    else:
                        invoice['price'] = candidate_action.marketSessionid.clearingPrice
                    # invoice['exchanged_value']  = str(candidate_maco.exchangedvalue) + " " + candidate_action.uom
                    # in case of the flexibility market
                    # the amount of the invoice is directly the amount from the corresponding database
                    # object, as it was set by the clearing algorithm
                    if pathInvoices == "balancing-invoices":
                        invoice['exchanged_value'] = invoices[0].amount
                    else:
                        invoice['exchanged_value'] = round(candidate_maco.exchangedValue * invoice['price'], 2)

                    invoice['marketSession'] = candidate_action.marketSessionid.id
                    invoice['type'] = candidate_action.actionTypeid.type
                    invoice['id'] = invoices[0].id
                    invoice['date'] = setUTCFormatter(invoices[0].date)
                    invoice['fee'] = "-" if invoices[0].fixedFee == None else invoices[0].fixedFee
                    invoice['penalty'] = "-" if invoices[0].penalty == None else invoices[0].penalty
                    invoice['total'] = invoice['exchanged_value'] + invoice['fee'] - invoice['penalty']
                    invoice['form'] = candidate_action.formid.form
                    invoice_info.append(invoice)
        return Response({'invoices': invoice_info}, status=200)


class generalHistory(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, KNOWN_MARKET_ACTOR_READ],
    }

    @swagger_auto_schema(manual_parameters=[PATHHISTORY, USERNAME], responses=getActionById_GET,
                         operation_summary="Get specific actions of specific actor")
    def get(self, request, *args, **kwargs):
        pathHistory = kwargs['pathHistory']
        username = kwargs['username']
        try:
            rel = UserActorRel.objects.get(user__username=username, status="valid")
        except Exception as e:
            log.warning(e)
            return Response(status=404, data={"details": str(e)})
        actor = None
        actions = []
        try:
            actor = MarketActor.objects.get(id=rel.actor.id)
        except Exception as e1:
            log.warning(e1)
            return Response(status=404, data={"details": str(e1)})
        if actor is None:
            actions = []
        else:

            if pathHistory == "energy-history":
                energyForms = ["electric_energy"]
            elif pathHistory == "heat-history":
                energyForms = ["thermal_energy"]
            elif pathHistory == "ancillary-history":
                energyForms = ['congestion_management', 'reactive_power_compensation', 'spinning_reserve', 'scheduling']
            elif pathHistory == "it_load-history":
                energyForms = ['it_load']

            if actor.marketActorTypeid.type == "market operator":
                actions = MarketAction.objects.filter(formid__form__in=energyForms)
            else:
                actions = MarketAction.objects.filter(marketActorid__id=actor.id, formid__form__in=energyForms)

        clearing_info = []
        if actions or len(actions) > 0:
            for action in actions:
                if action.statusid.status == "accepted" or action.statusid.status == "partially_accepted" or action.statusid.status == "delivered":
                    info = {}
                    info['id'] = action.id

                    # For it-load actions the clearing price equals the "amount" field from the Invoice table
                    if pathHistory == "it_load-history":
                        try:
                            candidate_macos = MarketActionCounterOffer.objects.filter(
                                Q(marketAction_Bid_id=action) | Q(marketAction_Offer_id=action))
                        except Exception as e:
                            log.warning(e)
                            return Response(status=404)
                        for candidate_maco in candidate_macos:
                            try:
                                candidate_transactions = Transaction.objects.filter(
                                    marketActionCounterOfferid=candidate_maco)
                            except Exception as e:
                                log.warning(e)
                                return Response(status=404, data={"details": str(e)})
                            for candidate_transaction in candidate_transactions:
                                invoices = Invoice.objects.filter(transactionid=candidate_transaction)
                                if len(invoices) > 1:
                                    raise Exception('Multiple Invoices for one transaction')
                                if len(invoices) < 1:
                                    continue
                                info['clearing_price'] = invoices[0].amount

                    else:
                        info['clearing_price'] = action.marketSessionid.clearingPrice

                    if action.actionTypeid.type == "bid":
                        counterActions = MarketActionCounterOffer.objects.filter(marketAction_Bid_id=action.id)
                    else:
                        counterActions = MarketActionCounterOffer.objects.filter(marketAction_Offer_id=action.id)
                    info['value'] = 0
                    if len(counterActions) > 0:
                        for ca in counterActions:
                            info['value'] = info['value'] + ca.exchangedValue
                    clearing_info.append(info)
        context = {
            'actions': MarketActionSerializer(actions, many=True).data,
            'actor': MarketActorSerializer(actor).data,
            'clearing_info': clearing_info,
            'title': 'History',
            'year': pytz.utc.localize(datetime.now()).year
        }
        return JsonResponse(context)


class createKeycloakUser(APIView):

    def post(self, request, *args, **kwargs):

        try:
            log.warning("Entering service createKeycloakUser")
            payload = request.data

            keycloak = Client()

            actor_type = MarketActorType.objects.get(pk=payload['market_actor_id'])
            if(actor_type == None):
                return Response(status=404, data={'details': 'No market actor type with the required id: {} found'.format(payload['market_actor_id'])})

            log.warning("Creating keycloak account for user: {}, actor typ: {}".format(payload['username'],
                                                                                       actor_type.type))
            ret = keycloak.create_user(payload['username'],
                                       payload['password'],
                                       payload['first_name'],
                                       payload['last_name'],
                                       actor_type.type)

            if ret is None:
                log.warning("Error while creating account for user: {}, actor typ: {}".format(payload['username'],
                                                                                       actor_type.type))
                log.warning("A user with this username already exists!")
                return Response(status=500, data={'details':'A user with this username already exists!'})
            else:
                log.warning("Account successfully created. Keycloak user id is: {}".format(ret))

        except:
            return Response(status=500, data={'details':'Failed to create user'})
        return Response(json.dumps(1))


class createUserActorAndAssignClientSpecificRoles(APIView):

    def post(self, request, *args, **kwargs):
        try:
            payload = request.data

            log.warning("Entering service createUserActorAndAssignClientSpecificRoles")
            log.warning("Creating user actor mappings and assigning client specific roles for user : {}"
                        .format(payload['username']))

            existing_user = User.objects.filter(username=payload['username'])

            if len(existing_user) > 0:
                log.warning("A Django user with username: {} already exists. Aborting...".format(payload['username']))
                return Response(json.dumps(1))

            log.warning("Creating Django user ...")
            user = User.objects.create_user(
                password=payload['password'],
                username=payload['username'],
                first_name=payload['first_name'],
                last_name=payload['last_name'],
                email=payload['email']
            )
            user.save()
            log.warning("Successfully created Django user")

            log.warning("Creating actor for user ... ")
            actor = MarketActor(
                companyName=payload['company_name'],
                email=payload['email'],
                phone=payload['phone'],
                representativeName=payload['first_name'] + " " + payload['last_name'],
                vat=payload['vat'],
                marketActorTypeid=MarketActorType.objects.get(pk=payload['market_actor_id'])
            )
            actor.save()
            log.warning("Successfully created actor for user with type: {}".format(actor.marketActorTypeid.type))

            log.warning("Creating user actor mapping ...")
            # create relation
            rel = UserActorRel(
                actor_id=actor.id,
                user_id=user.id,
                status='valid'
            )

            rel.save()
            log.warning("Successfully created user actor mapping...")


            log.warning("Registering actor in marketplaces. Creating Marketplace_has_MarketActor objects ...")
            # IF MP, register MP as actor in all marketplaces
            if actor.marketActorTypeid.type in ["Generic Participant", "generic participant"]:
                for market in Marketplace.objects.all():
                    m = Marketplace_has_MarketActor(
                        marketplace=market,
                        marketActor=actor
                    )
                    m.save()

            # IF Operator, assign Operator as the operator of every market in this flavour
            # (only 1 market operator per market deployment)
            if actor.marketActorTypeid.type in ["Market Operator", "market operator"]:
                for market in Marketplace.objects.all():
                    market.marketOperatorid = actor
                    market.save()

            log.warning("Successfully registered market actor in marketplaces")
            keycloak = Client()

            log.warning("Assigning keycloak client specific roles for user: {}".format(user.username))
            ret = keycloak.assign_client_roles_to_market_actor(user.username,
                                                         actor.marketActorTypeid.type)

            if ret == 0:
                log.warning("No client specific roles to add for user: {} for user type: {}".format(user.username,
                                                                                                    actor.marketActorTypeid.type))


        except Exception as e:
            log.warn(e)
            return Response(json.dumps(0))
        return Response(json.dumps(1))


class signupUser(APIView):

    def post(self, request, *args, **kwargs):
        try:
            payload = request.data

            existing_user = User.objects.filter(username=payload['username'])

            if len(existing_user) > 0:
                return Response(json.dumps(0))

            user = User.objects.create_user(
                password=payload['password'],
                username=payload['username'],
                first_name=payload['first_name'],
                last_name=payload['last_name'],
                email=payload['email']
            )
            user.save()
            actor = MarketActor(
                companyName=payload['company_name'],
                email=payload['email'],
                phone=payload['phone'],
                representativeName=payload['first_name'] + " " + payload['last_name'],
                vat=payload['vat'],
                marketActorTypeid=MarketActorType.objects.get(pk=payload['market_actor_id'])
            )
            actor.save()

            # create relation
            rel = UserActorRel(
                actor_id=actor.id,
                user_id=user.id,
                status='valid'
            )
            rel.save()
            # IF DSO, associate dso with marketplace types
            if actor.marketActorTypeid.type in ["DSO", "dso"]:
                operators = [op.id for op in MarketActor.objects.filter(marketActorTypeid__type="market operator")]
                if len(operators) > 0:
                    for mst in MarketServiceType.objects.all():
                        rec = Marketplace(
                            marketServiceTypeid=MarketServiceType.objects.get(pk=mst.id),
                            marketOperatorid=MarketActor.objects.get(pk=operators[0]),
                            dSOid=MarketActor.objects.get(pk=actor.id)
                        )
                        rec.save()

            # IF MP, register MP as actor in DSO marketplaces
            if actor.marketActorTypeid.type in ["Market Participant", "market participant"]:
                for market in Marketplace.objects.filter(dSOid__exact=payload['dso_id']):
                    m = Marketplace_has_MarketActor(
                        marketplace=Marketplace.objects.get(pk=market.id),
                        marketActor=MarketActor.objects.get(pk=actor.id)
                    )
                    m.save()
        except:
            return Response(json.dumps(0))
        return Response(json.dumps(1))


class signupForm(APIView):
    """
    This endpoint provides the supported market actor types and the list of DSOs
    for filling the sign up form of the Access Manager accordingly
    """

    def get(self, request, *args, **kwargs):
        try:

            log.warning("enter service signupForm...")
            #activeUsers = [rel.actor.id for rel in UserActorRel.objects.filter(status="valid")]
            #dsoList = MarketActor.objects.filter(pk__in=activeUsers, marketActorTypeid__type__iexact="dso")
            dsoList = []
            actortypes = MarketActorType.objects.all()
            log.warning("Successfully retrieved market actor types: {}".format(actortypes))
            actortypeserializer = MarketActorTypeSerializer(actortypes, many=True)
            context = {'dso_list': MarketActorSerializer(dsoList, many=True).data,
                       'market_actors': actortypeserializer.data}
            log.warning("responding to service signupForm {}".format(context))
            return Response(context, status=200)
        except Exception as e:
            log.warning(e)
            return Response(status=404, data={"details": str(e)})
        return Response(status=404)


class selectMarketplace(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, KNOWN_MARKET_ACTOR_READ, MEMO_READ],
    }

    @swagger_auto_schema(manual_parameters=[SERVICE_TYPE, TIMEFRAME], responses=MarketPlaceList_GET,
                         operation_summary="Get marketplace by market service type and timeframe")
    def get(self, request, *args, **kwargs):
        type = kwargs.get('servicetype')
        timeframe = kwargs.get('timeframe')
        flex_timeframes = ['real_time', 'intra_day', 'day_ahead']
        # mplaces = []

        if type in {"ancillary-services", "ancillary_services", "congestion_management", "reactive_power_compensation",
                    "spinning_reserve", "scheduling"}:
            type = "ancillary-services"
            if timeframe not in flex_timeframes:
                return Response(status=404, data={
                    "details": "Provided timeframe not supported for the flexibility marketplace."})

        elif type == 'electric_energy':
            type = 'energy'
            if timeframe not in [x.type for x in TimeFrame.objects.all()]:
                return Response(status=404,
                                data={"details": "Provided timeframe not supported for energy marketplaces."})

        elif type == 'thermal_energy':
            if timeframe not in [x.type for x in TimeFrame.objects.all()]:
                return Response(status=404, data={"details": "Provided timeframe not supported for heat marketplaces."})

        elif type == 'it_load':
            if timeframe not in [x.type for x in TimeFrame.objects.all()]:
                return Response(status=404,
                                data={"details": "Provided timeframe not supported for IT Load marketplaces."})

        else:
            return Response(status=404, data={"details": "Provided type not supported"})

        mymarket = Filter.marketplaces(request=request)

        ids = [x.id for x in mymarket if x in Marketplace.objects.filter(marketServiceTypeid__type=type,
                                                                         marketServiceTypeid__timeFrameid__type=timeframe)]
        return Response(ids, status=200)


class selectMarketplaceByUsername(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, KNOWN_MARKET_ACTOR_READ, MEMO_READ],
    }

    @swagger_auto_schema(manual_parameters=[SERVICE_TYPE, TIMEFRAME, USERNAME], responses=MarketPlaceList_GET,
                         operation_summary="Get marketplace by market service type and timeframe of specific username")
    def get(self, request, *args, **kwargs):
        type = kwargs.get('servicetype')
        timeframe = kwargs.get('timeframe')
        username = kwargs.get('username')
        flex_timeframes = ['real_time', 'intra_day', 'day_ahead']

        if type == 'ancillary_services':
            type = 'ancillary-services'

        if type == 'ancillary-services' and timeframe not in flex_timeframes:
            return Response(status=404,
                            data={"details": "Provided timeframe not supported for ancillary services marketplace."})
        elif type == 'energy' and (timeframe not in [x.type for x in TimeFrame.objects.all()]):
            return Response(status=404, data={"details": "Provided timeframe not supported for energy marketplaces."})
        elif type == 'thermal_energy' and timeframe not in [x.type for x in TimeFrame.objects.all()]:
            return Response(status=404, data={"details": "Provided timeframe not supported for heat marketplaces."})
        elif type == 'it_load' and timeframe not in [x.type for x in TimeFrame.objects.all()]:
            return Response(status=404, data={"details": "Provided timeframe not supported for IT Load marketplaces."})

        rel = UserActorRel.objects.get(user__username=username)

        actor = get_actor_from_request(request)

        dso_count = MarketActor.objects.filter(pk=rel.actor_id,
                                               marketActorTypeid__type="dso").count()


        if actor.marketActorTypeid.type == "market operator":
            mplace = Marketplace.objects.filter(marketServiceTypeid__type=type,
                                                marketServiceTypeid__timeFrameid__type=timeframe)
            ids = [m.id for m in mplace]
        elif dso_count:
            mplace = Marketplace.objects.filter(marketServiceTypeid__type=type, dSOid__id=rel.actor.id,
                                                marketServiceTypeid__timeFrameid__type=timeframe)
            ids = [m.id for m in mplace]
        else:
            memberInMarketplaces = [i.marketplace.id for i in
                                    Marketplace_has_MarketActor.objects.filter(marketActor=rel.actor)]

            mplace = Marketplace.objects.filter(marketServiceTypeid__type=type, pk__in=memberInMarketplaces,
                                                marketServiceTypeid__timeFrameid__type=timeframe)
            ids = [m.id for m in mplace]
        return Response(ids, status=200)


class FormList(APIView):

    @swagger_auto_schema(responses=Form_GET, operation_summary="List of available forms")
    def get(self, request, *args, **kwargs):
        forms = Form.objects.all()
        serializer = FormSerializer(forms, many=True)
        return Response(serializer.data)


class FormById(APIView):

    @swagger_auto_schema(manual_parameters=[FORM_ID], responses=getFormById_GET,
                         operation_summary="Get action by ID")
    def get(self, request, *args, **kwargs):
        try:
            form = Form.objects.get(pk=kwargs.get('form_id'))
        except Exception as e:
            log.warning("Form not found. ", e)
            return Response(status=404, data={"details": str(e)})
        serializer = FormSerializer(form)
        return Response(serializer.data)


class TimeframeList(APIView):

    @swagger_auto_schema(responses=TimeframeList_GET, operation_summary="List of available timeframes")
    def get(self, request, *args, **kwargs):
        times = TimeFrame.objects.all()
        serializer = TimeFrameSerializer(times, many=True)
        return Response(serializer.data)


class actionByUser(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, KNOWN_MARKET_ACTOR_READ],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, SESSION_ID, USERNAME, IS_DSO],
                         responses=actionByUser_RESPONSE,
                         operation_summary="Get action by username, or all actions of marketsession, if requesting user is dso or operator.")
    def get(self, request, *args, **kwargs):
        try:

            log.warning("Entering service actionByUser...")
            username = kwargs.get('username')
            is_dso = kwargs.get('is_dso')
            log.warning("Retrieve market actions for username: {}".format(username))
            log.warning("Check existence of marketplace with id: {} for user: {}".format(kwargs.get('pk'), username))
            marketplace = Filter.marketplaces(request=request, id=kwargs.get('pk'))
            if marketplace is None:
                log.warning("Marketplace with id: {} not existing for user {}".format(kwargs.get('pk'), username))
                return Response(status=403)
            log.warning("Marketplace with id: {} found!".format(kwargs.get('pk')))
            log.warning("Check existence of marketsession with id: {}".format(kwargs['sid']))
            ses = Filter.marketsessions(request=request, marketsession_id=kwargs['sid'])
            if ses is None:
                log.warning("Market session with id: {} not existing for user: {}".format(kwargs['sid'], username))
                return Response(status=403)

            elif ses.marketplaceid != marketplace:
                log.warning("Market session with id: {} does not belong to market: {}".format(kwargs['sid'], kwargs.get('pk')))
                return Response(status=404, data={
                    "details": "Market session {} does not belong to marketplace {}.".format(ses.id, marketplace.id)})

            log.warning("Session with id: {} found!".format(kwargs.get('sid')))
            actions = Filter.market_actions_of_session(request=request, market_session=ses)
            serializer = MarketActionSerializer(actions, many=True)
            log.warning("Returning market actions for user {} : {}".format(username, serializer.data))
            return Response(serializer.data)
        except Exception as e:
            log.warning(e)
            return Response(status=404, data={"details": str(e)})


class actionByUserAndActionStatus(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, KNOWN_MARKET_ACTOR_READ],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, SESSION_ID, USERNAME, IS_DSO, ACTION_STATUS],
                         responses=actionByUser_RESPONSE,
                         operation_summary="Get action by username, or all actions of marketsession, if requesting user is dso or operator. Filter by action status")
    def get(self, request, *args, **kwargs):
        try:
            username = kwargs.get('username')
            is_dso = kwargs.get('is_dso')
            action_status = kwargs.get('action_status')
            marketplace = Filter.marketplaces(request=request, id=kwargs.get('pk'))
            if marketplace is None:
                return Response(status=403)
            ses = Filter.marketsessions(request=request, marketsession_id=kwargs['sid'])
            if ses is None:
                return Response(status=403)
            elif ses.marketplaceid != marketplace:
                return Response(status=404, data={
                    "details": "Market session {} does not belong to marketplace {}.".format(ses.id, marketplace.id)})

            actions = Filter.market_actions_of_session_by_action_status(request=request, market_session=ses, action_status=action_status)
            serializer = MarketActionSerializer(actions, many=True)
            return Response(serializer.data)
        except Exception as e:
            log.warning(e)
            return Response(status=404, data={"details": str(e)})


class selectMarketSessionByForm(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, MARKET_PARTICIPANT_READ, DSO_READ],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, FORM, SESSION_STATUS], responses=MarketSessions_GET,
                         operation_summary="Get market session of given marketplace by its form")
    def get(self, request, *args, **kwargs):
        if 'status' in kwargs:
            ses_status = kwargs['status']
        else:
            Response(status=404, data={"details": "Status not provided"})
        mplace = Filter.marketplaces(request=request, id=kwargs.get('pk'))
        form = Form.objects.get(form=kwargs.get('form'))
        mysessions = Filter.marketsessions(request=request)
        sessions = [ses for ses in mysessions if ses in MarketSession.objects.filter(marketplaceid=mplace, formid=form,
                                                                                     sessionStatusid__status=ses_status)]
        serializer = MarketSessionSerializer(sessions, many=True)
        return Response(serializer.data)


class pendingRequests(APIView):

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, MARKET_PARTICIPANT_READ, DSO_READ],
    }

    @swagger_auto_schema(responses=GET_pendingRequests, operation_summary="Get pending requests")
    def get(self, request, *args, **kwargs):
        pending_requests = UserActorRel.objects.filter(status="invalid").count()
        return Response(pending_requests)


class AssignActorToMarketplace(APIView):
    """This interface assigns a given actor to a given marketplace"""

    keycloak_scopes = {
        'POST': [MARKET_OPERATOR_WRITE, MARKET_PARTICIPANT_WRITE],
        'DELETE': [MARKET_OPERATOR_WRITE, MARKET_PARTICIPANT_WRITE],
    }

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, ACTOR_ID], responses=ACTOROFMARKETPLACE_RESPONSES,
                         operation_summary="Assign specific actor to specific marketplace")
    def post(self, requests, *args, **kwargs):
        actor_id = kwargs.get("actor")
        mplace_id = kwargs.get("pk")
        markeplace = get_object_or_404(Marketplace, pk=mplace_id)
        marketactor = get_object_or_404(MarketActor, pk=actor_id)
        Marketplace_has_MarketActor.objects.create(
            marketplace=markeplace,
            marketActor=marketactor
        )
        return Response(status=status.HTTP_201_CREATED)

    @swagger_auto_schema(manual_parameters=[MARKETPLACE_ID, ACTOR_ID], responses=ACTOROFMARKETPLACE_DELETE,
                         operation_summary="Delete specific actor from specific marketplace")
    def delete(self, requests, *args, **kwargs):
        actor_id = kwargs.get("actor")
        mplace_id = kwargs.get("pk")
        mapping = Marketplace_has_MarketActor.objects.filter(
            marketplace_id=mplace_id,
            marketActor_id=actor_id
        )
        if mapping.exists():
            mapping.delete()
            return Response(status.HTTP_204_NO_CONTENT)
        else:
            return Response(status.HTTP_404_NOT_FOUND)


class MarketActorMplaces(APIView):
    """This interface returns the information about the marketplaces in which a Market Participant is registered"""

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, KNOWN_MARKET_ACTOR_READ],

    }

    @swagger_auto_schema(manual_parameters=[ACTOR_ID], responses=MarketActorMplaces_GET,
                         operation_summary="Marketplaces in which given market actor participates")
    def get(self, request, *args, **kwargs):
        try:
            actor = MarketActor.objects.get(id=kwargs['actor'])
        except Exception as e:
            log.warning(e)
            return Response(status=404, data={"details": str(e)})
        mappings = Marketplace_has_MarketActor.objects.filter(marketActor=actor)
        ownmarkets = Filter.marketplaces(request=request)

        mplace_serializer = MarketPlaceSerializer(ownmarkets, many=True)

        mplace_ids = [mapping.marketplace_id for mapping in mappings if mapping.marketplace in ownmarkets]
        return Response(status=200, data=mplace_serializer.data)


class StatusParticipant(APIView):
    """
    This interface updates a Market Participant's status, according to given input
    data = {"username": <username>, "actor": <actor_id>, "status": <valid | invalid | unknown>};
    """


    keycloak_scopes = {
        'PUT': [MARKET_OPERATOR_WRITE],
    }

    @swagger_auto_schema(responses=PARTICIPANT_STATUS_PUT, operation_summary="Updates a Participant's status",
                         request_body=UserActorStatusSerializer)
    def put(self, request, *args, **kwargs):
        data = request.data
        serializer = UserActorStatusSerializer(data=data)
        if serializer.is_valid():
            try:
                uar = UserActorRel.objects.get(user__username=data["username"], actor_id=data["actor"])
                uar.status = data["status"]
                uar.save()
                return Response(status=204, data={"details": PARTICIPANT_STATUS_PUT.get(status.HTTP_204_NO_CONTENT)})
            except Exception as e:
                log.warning(e)
                return Response(status=404, data={"details": str(e)})
        else:
            return Response(serializer.errors, status=400)
        return Response(status=400)

class requestAID(APIView):
    """
    This interface retrieves the actor id given the user information payload
    """

    keycloak_scopes = {
        'POST': [MARKET_OPERATOR_WRITE, MEMO_WRITE, MBM_WRITE, MCM_WRITE],
    }

    @swagger_auto_schema(responses=ACTOR_ID, operation_summary="Retrieves actor id given the user information payload")
    def post(self, request, *args, **kwargs):

        log.warning('Entering service requestAID')
        payload = request.data
        log.warning("Received user payload: {}".format(payload))

        username = payload['username']
        if username is None:
            log.warning('The field username is mandatory on the payload')
            return Response(status=404, data={'details': 'The field username is mandatory on the payload'})

        actor = UserActorRel.objects.get(user__username=username).actor
        if actor is None:
            log.warning("No actor found for username: {}".format(username))
            return Response(status=404, data={'details': "No actor found for username: {}".format(username)})

        return Response(status=200, data={'actorId': actor.id})


class MigrationDetails(mixins.ListModelMixin,
                     mixins.CreateModelMixin,
                     generics.GenericAPIView):
    queryset = MarketActionCounterOffer.objects.all()
    serializer_class = MigrationDetailsSerializer

    keycloak_scopes = {
        'GET': [MARKET_OPERATOR_READ, MARKET_PARTICIPANT_READ, DSO_READ],
    }

    @swagger_auto_schema(manual_parameters=[ACTION_ID],
                         responses=MIGRATIONDETAILS_GET,
                         operation_summary="Get migration details")
    def get(self, request, *args, **kwargs):
        """
        Get migration details of an existing IT Load Market Action
        """
        try:
            action = kwargs['action_id']
            counteractions = MarketActionCounterOffer.objects.filter(Q(marketAction_Bid_id__id=action) | Q(marketAction_Offer_id__id=action))
        except Exception as e:
            log.warning(e)
            return Response(status=status.HTTP_404_NOT_FOUND, data={"details": str(e)})

        if len(counteractions) > 1:
            log.warning("Multiple counteroffers found for action: {}".format(action))
            return Response(status=status.HTTP_404_NOT_FOUND, data={'details': "Multiple counteroffers found for action: {}".format(action)})
        elif len(counteractions) > 0:
            bid = counteractions.first().marketAction_Bid_id
            bid_start = bid.actionStartTime
            bid_end = bid.actionEndTime
            offer = counteractions.first().marketAction_Offer_id
            offer_start = offer.actionStartTime
            offer_end = offer.actionEndTime
            del_start = min(bid_start, offer_start)
            del_end = max(bid_end, offer_end)
            source = UserActorRel.objects.get(actor=offer.marketActorid).user.username
            destination = UserActorRel.objects.get(actor=bid.marketActorid).user.username
            transaction = Transaction.objects.get(marketActionCounterOfferid=counteractions.first())
            migrationdetails = MigrationDetails(marketaction=action, source=source, destination=destination, delivery_start=del_start, delivery_end=del_end, transaction=transaction.id)
            serializer = MigrationDetailsSerializer(migrationdetails)
            log.info(serializer)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_404_NOT_FOUND, data={'details': "No counteractions found for action: {}".format(action)})
