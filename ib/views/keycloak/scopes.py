
# Realm specific scopes (designated for every client)

KNOWN_MARKET_ACTOR_READ = 'known-market-actor'
KNOWN_MARKET_ACTOR_WRITE = 'known-market-actor'

MEMO_READ = 'memo'
MEMO_WRITE = 'memo'

MARKET_OPERATOR_READ = 'operator'
MARKET_OPERATOR_WRITE = 'operator'

MARKET_PARTICIPANT_READ = 'market-participant'
MARKET_PARTICIPANT_WRITE = 'market-participant'

# Client specific scopes (designated for a specific client)
DSO_READ = 'dso-read'
DSO_WRITE = 'dso-write'

MCM_READ = 'mcm-read'
MCM_WRITE = 'mcm-write'

MBM_READ = 'mbm-read'
MBM_WRITE = 'mbm-write'

AGGREGATOR_READ = 'aggregator-read'
AGGREGATOR_WRITE = 'aggregator-write'



