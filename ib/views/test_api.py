from datetime import datetime
import json
from itertools import chain

from django.db.models import Q
from django.http import Http404, JsonResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from drf_yasg.utils import swagger_auto_schema
from rest_framework import mixins, generics, status
from django.conf import settings
from rest_framework.response import Response
from rest_framework.views import APIView

from ib.object_filters import Filter
from ib.permissions import *
from rest_framework import permissions
from ib.serializer import *
from ib.swagger_responses import *
from ib.utils import *
from django.contrib.auth.models import User
from ib.models import TimeFrame
import pytz


class getUserMarketMEMO(APIView):

    def get(self, request, *args, **kwargs):
        """Retrieve marketplaces for a specific user"""

        username = kwargs.get('username')

        log.info("Return registered markets for user: {}".format(username))


        ua_rel = UserActorRel.objects.filter(user__username=username)
        if len(ua_rel) > 0:
            actor_id = ua_rel[0].actor.id
        else:
            actor_id = 1

        user_markets = []

        user_market_electricity = {}
        user_market_electricity['type'] = 'energy'
        user_market_electricity['form'] = 'electric_energy'
        user_market_electricity['url'] = 'http://localhost:8000'
        user_market_electricity['id'] = 1
        user_market_electricity['component'] = 'IB'
        user_market_electricity['actorId'] = actor_id

        user_market_heat = {}
        user_market_heat['type'] = 'energy'
        user_market_heat['form'] = 'thermal_energy'
        user_market_heat['url'] = 'http://localhost:8002'
        user_market_heat['id'] = 2
        user_market_heat['component'] = 'IB'
        user_market_heat['actorId'] = actor_id

        user_market_it_load = {}
        user_market_it_load['type'] = 'it_load'
        user_market_it_load['form'] = 'it_load'
        user_market_it_load['url'] = 'http://localhost:8003'
        user_market_it_load['id'] = 3
        user_market_it_load['component'] = 'IB'
        user_market_it_load['actorId'] = actor_id

        user_market_flexibility = {}
        user_market_flexibility['type'] = 'ancillary_services'
        user_market_flexibility['form'] = 'ancillary_services'
        user_market_flexibility['url'] = 'http://localhost:8004'
        user_market_flexibility['id'] = 4
        user_market_flexibility['component'] = 'IB'
        user_market_flexibility['actorId'] = actor_id

        user_markets.append(user_market_heat)
        user_markets.append(user_market_electricity)
        user_markets.append(user_market_it_load)
        user_markets.append(user_market_flexibility)

        return Response(status=200, data=UserMarketSerializer(user_markets, many=True).data)


class itlbClearingRequest(APIView):
    permission_classes = ()

    def post(self, request, *args, **kwargs):
        """Retrieve marketplaces for a specific user"""
        data = request.data
        prioritised_ma_serializer = PrioritisedMarketActionsClearingSerializer(data=data, many=True)

        if (prioritised_ma_serializer.is_valid()):
            co_s = MarketActionCounterOffer.objects.all()
            counteroffers = MarketActionCounterOfferSerializer(co_s, many=True).data
            actions = [5, 6, 7, 8]

            counteroffers_and_actions_response = {}
            counteroffers_and_actions_response["counteroffers"] = counteroffers
            counteroffers_and_actions_response["actions"] = actions

            response_data = json.dumps(counteroffers_and_actions_response, cls=DjangoJSONEncoder)
            return Response(status=201, data=response_data)
        else:
            return Response(status=400, data=prioritised_ma_serializer.errors)


class getMarketsMEMO(APIView):
    permission_classes = ()


    def get(self, request, *args, **kwargs):
        """Retrieve all marketplaces"""

        log.info("Retrieving all marketplaces")

        user_markets = []

        user_market_electricity = {}
        user_market_electricity['type'] = 'energy'
        user_market_electricity['form'] = 'electric_energy'
        user_market_electricity['url'] = 'http://localhost:8000'
        user_market_electricity['id'] = 1
        user_market_electricity['component'] = 'IB'

        user_market_heat = {}
        user_market_heat['type'] = 'thermal_energy'
        user_market_heat['form'] = 'thermal_energy'
        user_market_heat['url'] = 'http://localhost:8002'
        user_market_heat['id'] = 2
        user_market_heat['component'] = 'IB'

        user_market_it_load = {}
        user_market_it_load['type'] = 'it_load'
        user_market_it_load['form'] = 'it_load'
        user_market_it_load['url'] = 'http://localhost:8003'
        user_market_it_load['id'] = 3
        user_market_it_load['component'] = 'IB'

        user_market_flexibility = {}
        user_market_flexibility['type'] = 'ancillary_services'
        user_market_flexibility['form'] = 'ancillary_services'
        user_market_flexibility['url'] = 'http://localhost:8004'
        user_market_flexibility['id'] = 4
        user_market_flexibility['component'] = 'IB'

        user_markets.append(user_market_heat)
        user_markets.append(user_market_electricity)
        user_markets.append(user_market_it_load)
        user_markets.append(user_market_flexibility)

        data = AvailableMarketSerializer(user_markets, many=True).data

        return Response(status=200, data=data)



class postMarketRegistrationRequestMEMO(APIView):
    def post(self, request, *args, **kwargs):
        payload = request.data

        log.warning("Receive payload for registration request MEMO: {}"
                    .format(payload))
        return Response(status=200)


class UserEnhancedAPIView(APIView):
    @property
    def user(self):
        return get_user_from_request(self.request)


class UserEnhancedGenericAPIView(generics.GenericAPIView):
    @property
    def user(self):
        return get_user_from_request(self.request)


class dsoTestKeyCloak(UserEnhancedAPIView):

    keycloak_scopes = {
        'GET': ['dso-read', 'market-operator-read', 'market-participant-read', 'mbm-read'],
        'POST': ['dso-write', 'market-operator-write', 'market-participant-write', 'mbm-write']
    }


    def get(self, request, *args, **kwargs):
        """Get test for keycloak dso scope"""

        user = self.user
        actor = get_actor_from_request(request)

        log.warn("Entering service GET:getTestKeyCloak")

        marketplaces = Filter.marketplaces(request)
        serializer = MarketPlaceSerializer(marketplaces, many=True)

        return Response(status=200, data=serializer.data)


    def post(self, request, *args, **kwargs):
        """Post test for keycloak dso scope"""

        log.warn("Entering service POST:getTestKeyCloak")
        data = request.data
        user = self.user
        actor = get_actor_from_request(request)

        marketplaces = Filter.marketplaces(request)
        serializer = MarketPlaceSerializer(marketplaces, many=True)

        return Response(status=200, data=serializer.data)


class getConstraintsMEMO(APIView):
    permission_classes = ()


    def get(self, request, *args, **kwargs):
        """Retrieve invoices of specific actor"""
        print("intra in get constraints")
        correlations = []
        correlation_one = {}
        correlation_one['constraintId'] = 1
        correlation_one['description'] = "All or nothing";

        correlation_two = {}
        correlation_two['constraintId'] = 2
        correlation_two['description'] = "Only one";

        correlations.append(correlation_one)
        correlations.append(correlation_two)

        return Response(status=200, data=ConstraintsSerializer(correlations, many=True).data)


class correlatedMarketActions(APIView):
    permission_classes = ()

    def post(self, request, *args, **kwargs):
        """Post reference prices"""

        data = request.data
        print("post correlations")
        print(data)
        serializer = CorrelatedMarketActionsSerializer(data=data)

        if (serializer.is_valid()):
            return Response("", status=status.HTTP_201_CREATED)
        return Response("", status=status.HTTP_204_NO_CONTENT)

    def get(self, request, *args, **kwargs):
        try:
            timeframe = kwargs['timeframe']
            username = kwargs['username']
            start = kwargs['start']
            end = kwargs['end']
        except Exception as e:
            return Response(status=404, data={'details': str(e)})

        print("get correlations")
        print(timeframe)
        print(username)
        print(start)
        print(end)

        correlations_obj = {}

        correlations_obj['username'] = username;

        correlations = []
        correlation_one = {}
        correlation_one['correlationId'] = 1
        correlation_one["constraintId"] = 1;

        actions = []
        actions.append({'informationBrokerId': 2,
                        'actionId': 20,
                        'sessionId': 17});
        actions.append({'informationBrokerId': 1,
                        'actionId': 21,
                        'sessionId': 17});
        correlation_one['actions'] = actions;

        correlations.append(correlation_one)

        correlation_one = {}
        correlation_one['correlationId'] = 2
        correlation_one['constraintId'] = 2
        actions = []
        actions.append({'informationBrokerId': 1,
                        'actionId': 22,
                        'sessionId': 18})
        actions.append({'informationBrokerId': 3,
                        'actionId': 23,
                        'sessionId': 19})
        correlation_one['actions'] = actions
        correlations.append(correlation_one)
        correlations_obj['correlations'] = correlations
        return Response(status=status.HTTP_200_OK, data=CorrelationsSerializer(correlations_obj, many=False).data)

    def put(self, request, *args, **kwargs):
        """Post reference prices"""
        print("put")
        data = request.data
        print(data)
        return Response("", status=status.HTTP_202_ACCEPTED)


    def delete(self, request, *args, **kwargs):
        """Post reference prices"""
        print("delete")
        data = request.data
        print(data)
        return Response("", status=status.HTTP_202_ACCEPTED)


class correlatedMarketActionsHistory(APIView):
    permission_classes = ()

    def get(self, request, *args, **kwargs):
        try:
            timeframe = kwargs['timeframe']
            username = kwargs['username']
        except Exception as e:
            return Response(status=404, data={'details': str(e)})

        print("get correlations")
        print(timeframe)
        print(username)

        correlations_obj = {}

        correlations_obj['username'] = username;

        correlations = []
        correlation_one = {}
        correlation_one['correlationId'] = 1
        correlation_one["constraintId"] = 1;

        actions = []
        actions.append({'informationBrokerId': 2,
                        'actionId': 20,
                        'sessionId': 17});
        actions.append({'informationBrokerId': 1,
                        'actionId': 21,
                        'sessionId': 17});
        correlation_one['actions'] = actions;

        correlations.append(correlation_one)

        correlation_one = {}
        correlation_one['correlationId'] = 2
        correlation_one['constraintId'] = 2
        actions = []
        actions.append({'informationBrokerId': 1,
                        'actionId': 22,
                        'sessionId': 18})
        actions.append({'informationBrokerId': 3,
                        'actionId': 23,
                        'sessionId': 19})
        correlation_one['actions'] = actions
        correlations.append(correlation_one)
        correlations_obj['correlations'] = correlations
        return Response(status=status.HTTP_200_OK, data=CorrelationsSerializer(correlations_obj, many=False).data)


class systemParameterConfig(APIView):
    permission_classes = ()

    def get(self, request, *args, **kwargs):
        arr = []
        obj1 = {'id': 1, 'name': "fixedFee", 'value': "500.00"}
        obj2 = {'id': 2, 'name': "penalty", 'value': "100.00"}
        obj3 = {'id': 3, 'name': "FlexSessionBillingTime", 'value': "23.00"}
        obj4 = {'id': 4, 'name': "FlexSessionCompleteTime", 'value': "23.00"}
        arr.append(obj1)
        arr.append(obj2)
        arr.append(obj3)
        arr.append(obj4)
        return Response(status=status.HTTP_200_OK, data=SystemParametersSerializer(arr, many=True).data)

    def put(self, request, *args, **kwargs):
        print("put")
        data = request.data
        print(data)
        return Response("", status=status.HTTP_202_ACCEPTED)
