# Dockerfile
FROM python:3.6-alpine

ENV PROJECT_ROOT=/root
ENV PIPENV_VENV_IN_PROJECT=1

WORKDIR ${PROJECT_ROOT}

RUN pip install pipenv

RUN set -ex \
    && apk add --no-cache --virtual .build-deps make gcc libc-dev \ 
    linux-headers pcre-dev musl-dev postgresql-dev

COPY Pipfile .
COPY Pipfile.lock .

RUN LIBRARY_PATH=/lib:/usr/lib /bin/sh -c "pipenv install" \
    && runDeps="$( \
            scanelf --needed --nobanner --recursive .venv \
                    | awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
                    | sort -u \
                    | xargs -r apk info --installed \
                    | sort -u \
    )" \
    && apk del .build-deps \
    && apk add --no-cache $runDeps

# The auth.json is completely irrelevant now
COPY auth.json .

# The ib.json should in my opinion be completely refactored
COPY ib.json .

COPY manage.py .
COPY entrypoint.sh .
COPY VERSION .

COPY auth auth
COPY information_broker information_broker
COPY ib ib

RUN pipenv run python manage.py collectstatic --settings=information_broker.settings.dev --no-input
ENV GUNICORN_CMD_ARGS="--bind 0.0.0.0:80 --workers 4 --error-logfile - --access-logfile -"
RUN rm -rf /root/.cache

# Annotate Port
EXPOSE 80

# ENTRYPOINT ["sh", "entrypoint.sh"]
